<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class M_publik extends CI_Model {
const __tablekomoditi = 'tbl_komoditi';

    function __construct() {
        parent::__construct();
    }

    public function selekKomoditi() {
        $this->db->select('tbl_koordinat.jenis_komoditas as jenis_komoditas');
        $this->db->group_by("jenis_komoditas");
        $query = $this->db->get('tbl_koordinat');
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

    public function total_all() {
        $this->db->from('tbl_koordinat');
        $this->db->group_by("kode_sertifikasi");
         $data = $this->db->get();
        return $data->num_rows();
       
    }

     public function total_where($komoditas) {
        $this->db->from('tbl_koordinat');
        $this->db->group_by("kode_sertifikasi");
        $this->db->where('tbl_koordinat.jenis_komoditas',$komoditas);
         $data = $this->db->get();
        return $data->num_rows();
       
    }

    public function allData()
   {

    $status='Selesai Approve';
   	$this->db->select(' tbl_koordinat.longitude as longitude,
   						tbl_koordinat.latitude as latitude,	
                        tbl_koordinat.created_date as created_date,
   						tbl_koordinat.kode_sertifikasi as kode_sertifikasi,
   						tbl_koordinat.jenis_komoditas as jenis_komoditas,
   						tbl_koordinat.jenis_usaha as jenis_usaha,
                            	
                        tbl_sertifikasi.nama_pemohon As nama_pemohon,
                        tbl_sertifikasi.email As email,
                        tbl_sertifikasi.nama_usaha As nama_usaha,
                        tbl_sertifikasi.status As status,
                        tbl_sertifikasi.no_telp As no_telp');

        $this->db->join('tbl_sertifikasi', 'tbl_koordinat.kode_sertifikasi = tbl_sertifikasi.kode', 'inner');
        $this->db->where('tbl_sertifikasi.status =', $status);
        // $this->db->where('tbl_vehicle_rent.created_date <=', $tanggal_akhir);

        // $this->db->where("(master_data.tanggal >='$tanggal_awal' AND master_data.tanggal<='$tanggal_akhir')");
        $query = $this->db->get('tbl_koordinat');
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

      public function Choice($komoditas) {
        $status='Selesai Approve';
         	$this->db->select(' tbl_koordinat.longitude as longitude,
   						tbl_koordinat.latitude as latitude,	
   						tbl_koordinat.kode_sertifikasi as kode_sertifikasi,
                        tbl_koordinat.created_date as created_date,
                        tbl_sertifikasi.nama_pemohon As nama_pemohon,
                        tbl_sertifikasi.email As email,
                        tbl_sertifikasi.jenis_usaha as jenis_usaha,
                        tbl_sertifikasi.status As status,
                        tbl_sertifikasi.jenis_komoditas As jenis_komoditas,
                        tbl_sertifikasi.nama_usaha As nama_usaha,
                        tbl_sertifikasi.no_telp As no_telp');

        $this->db->join('tbl_sertifikasi', 'tbl_koordinat.kode_sertifikasi = tbl_sertifikasi.kode', 'inner');
        $this->db->where('tbl_sertifikasi.jenis_komoditas',$komoditas);
        $this->db->where('tbl_sertifikasi.status =', $status);
        $query = $this->db->get('tbl_koordinat');
        if ($query->num_rows() > 0) {
            return $query->result();
    }

}


}