<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Web extends MX_Controller{

	function __construct()
	{
        parent::__construct();
    }

	public function index()
	{
		$this->load->model('M_publik');
        $data = array (
        	'komoditas' => $this->M_publik->selekKomoditi(),
	        'sessid'	=> $this->session->userdata('id'),
            'total'  =>  $this->M_publik->total_all(),
	        'title'		=> "Prima PSBP",
	        'style' 	=> '<style type="text/css">body {background-color: #FFFFFF;}.ui.menu .item img.logo {margin-right: 1em;}.main.container {margin-top: 7em;}.wireframe {margin-top: 2em;}.ui.footer.segment{}</style>'
	    );

        $this->load->view('front/global/header', $data);
        $this->load->view('front/global/nav_menu', $data);
		$this->load->view('Home', $data);
		$this->load->view('front/global/footer', $data);
       
	}

	public function showMapJatim()
	{
		$this->load->model('M_publik');
        $res = $this->M_publik->allData();
        $res2 = $this->M_publik->total_all();

         $data=array();
           foreach ($res as $row) {
                $data2=array();
                $data2['lng']=(float)$row->longitude;
                $data2['lat']=(float)$row->latitude;
                $data2['kode']=$row->kode_sertifikasi;
                $data2['jenis_komoditas']=$row->jenis_komoditas;
                $data2['created_date']=date('d-m-Y',strtotime($row->created_date));
                $data2['jenis_usaha']=$row->jenis_usaha;
                $data2['nama_pemohon']=$row->nama_pemohon;
                $data2['nama_usaha']=$row->nama_usaha;
                $data2['email']=$row->email;
                $data2['no_telp']=$row->no_telp;
                $data2['total']=$this->M_publik->total_all();
                $data[] = $data2;
            }            
            echo json_encode($data);

            if ($res < 0){
            	echo json_encode("data tidak ditemukan atau data tidak ada");
            }

	}

	public function Cari()
	{
		$this->load->model('M_publik');

		$komoditas = $this->input->post('komoditas');

         $res = $this->M_publik->Choice($komoditas);
            if ($komoditas!="") {
                $res = $this->M_publik->Choice($komoditas);
            }else{
                 $res = $this->M_publik->allData();
            }

         $data=array();
           foreach ($res as $row) {
                $data2=array();
                $data2['lng']=(float)$row->longitude;
                $data2['lat']=(float)$row->latitude;
                $data2['kode']=$row->kode_sertifikasi;
                $data2['jenis_komoditas']=$row->jenis_komoditas;
                $data2['created_date']=date('d-m-Y',strtotime($row->created_date));
                $data2['jenis_usaha']=$row->jenis_usaha;
                $data2['nama_pemohon']=$row->nama_pemohon;
                $data2['nama_usaha']=$row->nama_usaha;
                $data2['email']=$row->email;
                $data2['no_telp']=$row->no_telp;
                $data2['total']=$this->M_publik->total_where($komoditas);
                $data[] = $data2;
            }            
            echo json_encode($data);
	}


}
