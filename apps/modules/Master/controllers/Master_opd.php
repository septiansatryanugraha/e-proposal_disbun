<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_opd extends AUTH_Controller {

	const __tableName = 'tbl_user';
	const __tableName2 = 'tbl_login';
	const __tableId   = 'id_user';
	const __tableIdname  = 'name';
	const __folder    = 'v_opd/';
	const __kode_menu = 'master-opd';
	const __title     = 'Master User ';
	const __model     = 'Mdl_opd' ;

	


	public function __construct()
	{
		parent::__construct();
		$this->load->model(self::__model);
		$this->load->model('M_sidebar');
	}
	
	public function loadkonten($page, $data) {
		
		$data['userdata'] 	= $this->userdata;
		$ajax = ($this->input->post('status_link') == "ajax" ? true : false);
		if (!$ajax) { 
			$this->load->view('Dashboard/layouts/header', $data);
		}
		$this->load->view($page, $data);
		if (!$ajax) $this->load->view('Dashboard/layouts/footer', $data);
	}

	public function index()
	{
		$accessAdd = $this->M_sidebar->access('add',self::__kode_menu);
        $data['accessAdd']  = $accessAdd->menuview;
		$data['userdata'] 	= $this->userdata; 
		$data['page'] 		= self::__title;
		$data['judul'] 		= self::__title;
		
		$this->loadkonten(''.self::__folder.'home',$data);
	}

	public function ajax_list()
	{
		$tanggalAwal = $this->input->post('tanggal_awal');
        $tanggalAkhir = $this->input->post('tanggal_akhir');

        $filter = array(
            'tanggal_awal' => $tanggalAwal,
            'tanggal_akhir' => $tanggalAkhir,
        );
        
		$accessEdit = $this->M_sidebar->access('edit',self::__kode_menu);
        $accessDel = $this->M_sidebar->access('del',self::__kode_menu);
		$list = $this->Mdl_opd->getData(1,$filter);

		$data = array();
		$no = $_POST['start'];
		foreach ($list as $brand) {

			$status = '<small class="label pull-center bg-yellow">Non aktif</small">';
            if ($brand->status == 'Aktif') {
                $status = '<small class="label pull-center bg-green">Aktif</small">';
            }

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $brand->email;
			$row[] = $brand->nama_lengkap;
			$row[] = $brand->alamat;
			$row[] = $status;
			//add html for action
		     $buttonEdit = '';
            if ($accessEdit->menuview > 0) {
                $buttonEdit = anchor('edit-master-opd/' . $brand->id_user, ' <span tooltip="Edit Data"><span class="fa fa-edit" ></span>', ' class="btn btn-sm btn-primary klik ajaxify" ');
            }
            $buttonDel = '';
            if ($accessDel->menuview > 0) {
                $buttonDel = '<button class="btn btn-sm btn-danger hapus-master-opd" data-id=' . "'" . $brand->nama . "'" . '><span tooltip="Hapus Data"><i class="glyphicon glyphicon-trash"></i></button>';
            }

			$row[] = $buttonEdit . '  ' . $buttonDel;
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	
	public function Add() {
		
		/*ini harus ada boss */
		$data['userdata'] = $this->userdata;
		$access = $this->M_sidebar->access('add',self::__kode_menu);
		if ($access->menuview == 0){
			$data['page'] 		= self::__title;
			$data['judul'] 		= self::__title;
			$this->loadkonten('Dashboard/layouts/no_akses',$data);
		 }
		
		/*ini harus ada boss */
		else{
		$data['page'] 		= self::__title;
		$data['judul'] 		= self::__title;
		$data['dinas'] 	    = $this->Mdl_opd->selekDinas();
	
		$this->loadkonten(''.self::__folder.'tambah',$data);
	}
}

	 public function prosesAdd() {

    	$username = $this->userdata->nama;
        $date = date('Y-m-d H:i:s');
   
        $data = array(
				'nama_lengkap'	=> $this->input->post('nama_lengkap'),
				'alamat'	    => $this->input->post('alamat'),
				'email'         => $this->input->post('email'),
				'nama'          => $this->input->post('nama_lengkap'),
		        'password' 		=> base64_encode($this->input->post("password")),
		        'ip_address'	=> $this->input->ip_address(),
		        'user_agent'	=> $this->input->user_agent(),
		        'created_by'    => $username ,
		        'created_date'  => $date,
                'updated_date'  => $date
			);

			$data2=array(
			'nama' 		             => $this->input->post('nama_lengkap'),
			'email'                  => $this->input->post('email'),
			'status' 		         => 'Non aktif',	
			'tipe'                   => 'opd',		
			'password' 		   		 => base64_encode($this->input->post("password")),
			 );	


		$checkEmail = $this->Mdl_opd->checkEmail($data);

		if(!$checkEmail){
		 $result = $this->db->insert(self::__tableName, $data);
		 $result = $this->db->insert(self::__tableName2, $data2);

		if ($this->db->trans_status() === FALSE) {
              $out['status'] = 'gagal';   
           }
			
		if ($result > 0) {
		$this->db->trans_commit();
		$out['status'] = 'berhasil';
		} else {
		$this->db->trans_rollback();
		$out['status'] = 'gagal';
		}
		} else{

		$out['status'] = 'peringatan';
		}

		echo json_encode($out);
	}

	

	
	public function Edit($id) {
		
		/*ini harus ada boss */
		$data['userdata'] = $this->userdata;
		$access = $this->M_sidebar->access('edit',self::__kode_menu);
		if ($access->menuview == 0){
			$data['page'] 		= self::__title;
			$data['judul'] 		= self::__title;
			$this->loadkonten('Dashboard/layouts/no_akses',$data);
		 }
		 /*ini harus ada boss */
		 else{
		
		$where=array(self::__tableId => $id);
		$data['brand']  = $this->Mdl_opd->selectById($id);
		$data['datastatus']  = $this->Mdl_opd->selek_status();

		$data['page'] 		= self::__title;
		$data['judul'] 		= self::__title;
	    $this->loadkonten(''.self::__folder.'update',$data);
	}
	}


	public function prosesUpdate() {
		
    	$username = $this->userdata->nama;
        $date = date('Y-m-d H:i:s');

        $where = trim($this->input->post(self::__tableId));
        $where2 = trim($this->input->post('nama'));

         $data = array(
				'nama_lengkap'	=> $this->input->post('nama_lengkap'),
				'alamat'	    => $this->input->post('alamat'),
				'nama'          => $this->input->post('nama'),
				'latitude'	    => $this->input->post('latitude'),
				'longitude'	    => $this->input->post('longitude'),
				'status'		=> $this->input->post('status'),
		        // 'email'			=> $this->input->post('_email'),
		        'password' 		=> base64_encode($this->input->post("password")),
		        'ip_address'	=> $this->input->ip_address(),
		        'user_agent'	=> $this->input->user_agent(),
		        'created_by'    => $username,
                'updated_date'  => $date
			);

			$data2=array(
			'nama' 		             => $this->input->post('nama'),
			// 'email' 		   		 => $this->input->post('_email'),
			'status'		         => $this->input->post('status'),	
			'tipe'                   => 'opd',		
			'password' 		   		 => base64_encode($this->input->post("password")),
			 );	

		 $result = $this->db->update(self::__tableName, $data, array(self::__tableId => $where));
		 $result = $this->db->update(self::__tableName2, $data2, array('nama' => $where2));

		if ($this->db->trans_status() === FALSE) {
              $out['status'] = 'gagal';   
            }
			
		if ($result > 0) {
		$this->db->trans_commit();
		$out['status'] = 'berhasil';
		} else {
		$this->db->trans_rollback();
		$out['status'] = 'gagal';
		}
		

		echo json_encode($out);
	}


	// // public function hapus() {

	// // $this->load->helper("file");
	// // $token = $this->input->post('nama');
	// // $gambar=$this->db->get_where(self::__tableName,array('nama' =>$token));
	// // if($gambar->num_rows()>0){
	// // $hasil=$gambar->row();
	// // $judul=$hasil->link_file;
	// // //hapus unlink foto
	// // if(file_exists($file=$judul)){
	// // unlink($file);
	// // }
	// // $this->db->delete(self::__tableName,array(self::__tableId =>$token));
	// // }
	// // echo "{}";
	
	// // }


	public function hapus() {

	$this->load->helper("file");
	$token = $this->input->post('nama');
	$this->db->delete(self::__tableName,array('nama'=>$token));
	$this->db->delete(self::__tableName2,array('nama' =>$token));
	echo "{}";
	
	}



	// // public function checkPhoneExist($isAjax = 1, $phone, $idEmploye = NULL) {
 // //        $isExist = false;
 // //        if ($isAjax > 0) {
 // //            $phone = trim($this->input->post("phone"));
 // //            $idEmploye = trim($this->input->post("id_employe"));
 // //        }
 // //        if (strlen($phone) > 0) {
 // //            $q = "SELECT * FROM tbl_employe WHERE phone = '{$phone}'";
 // //            if (strlen($idEmploye) > 0) {
 // //                $q .= " AND id_employe <> '{$idEmploye}'";
 // //            }
 // //            $result = $this->db->query($q)->row_array();
 // //            if ($result != NULL) {
 // //                $isExist = true;
 // //            }
 // //        }
 // //        $arr = array('is_exist' => $isExist);

 // //        if ($isAjax > 0) {
 // //            $out['status'] = 'berhasil';
 // //        } else {
 // //            return $isExist;
 // //        }
 // //    }

    
  

	
	
}
