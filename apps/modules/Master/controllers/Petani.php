<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Petani extends AUTH_Controller {

    const __tableName = 'tbl_petani';
    const __tableId = 'id_petani';
    const __folder = 'v_petani/';
    const __kode_menu = 'master-petani';
    const __title = 'Kelompok Tani';
    const __model = 'M_petani';

    public function __construct() {
        parent::__construct();
        $this->load->model(self::__model);
        $this->load->model('M_sidebar');
    }

    public function loadkonten($page, $data) {

        $data['userdata'] = $this->userdata;
        $ajax = ($this->input->post('status_link') == "ajax" ? true : false);
        if (!$ajax) {
            $this->load->view('Dashboard/layouts/header', $data);
        }
        $this->load->view($page, $data);
        if (!$ajax)
            $this->load->view('Dashboard/layouts/footer', $data);
    }

    function ambil_data() {

        $modul = $this->input->post('modul');

        if ($modul == "kabupaten") {
            $id = $this->input->post('kode');
            echo $this->M_petani->select_kabupaten($id);
        }

        if ($modul == "kecamatan") {
            $id = $this->input->post('kode');
            echo $this->M_petani->select_kecamatan($id);
        } else if ($modul == "desa") {
            $id = $this->input->post('kecamatan');
            echo $this->M_petani->select_desa($id);
        }
    }

    public function index() {
        $accessAdd = $this->M_sidebar->access('add', self::__kode_menu);
        $data['accessAdd'] = $accessAdd->menuview;
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = self::__title;

        $this->loadkonten('' . self::__folder . 'home', $data);
    }

    public function ajax_list() {
        $status = $this->input->post('status');
        $tanggalAwal = $this->input->post('tanggal_awal');
        $tanggalAkhir = $this->input->post('tanggal_akhir');

        $filter = array(
            'status' => $status,
            'tanggal_awal' => $tanggalAwal,
            'tanggal_akhir' => $tanggalAkhir,
        );

        $accessEdit = $this->M_sidebar->access('edit', self::__kode_menu);
        $accessDel = $this->M_sidebar->access('del', self::__kode_menu);
        $list = $this->M_petani->getData(1, $filter);

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $brand) {

            $status = '<small class="label pull-center bg-yellow">Non aktif</small">';
            if ($brand->status == 'Aktif') {
                $status = '<small class="label pull-center bg-green">Aktif</small">';
            }

            $tipe_petani = '<small class="label pull-center bg-yellow">Baru</small">';
            if ($brand->tipe_petani == 'Lama') {
                $tipe_petani = '<small class="label pull-center bg-blue">Data Lama</small">';
            }
            $status_proposal = '<small class="label pull-center bg-yellow">Bisa Melakukan Pengajuan</small">';
            $getKelompokDpa = $this->getKelompokDpa($brand->id_petani);
            if ($getKelompokDpa != null) {
                if (date('Y') < date('Y', strtotime($getKelompokDpa->date))) {
                    $status_proposal = '<small class="label pull-center bg-red">Telah Masuk DPA Tahun ' . date("Y", strtotime($getKelompokDpa->created_date)) . '</small">';
                }
            }

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $brand->nama_kelompok;
            $row[] = $brand->kabupaten;
            $row[] = $brand->kecamatan;
            $row[] = $brand->desa;
            $row[] = $brand->nama_ketua;
            // $row[] = $tipe_petani;
            // $row[] = $status;
            $row[] = $status_proposal;
            //add html for action
            $buttonEdit = '';
            if ($accessEdit->menuview > 0) {
                $buttonEdit = anchor('edit-petani/' . $brand->id_petani, ' <span tooltip="Edit Data"><span class="fa fa-edit" ></span>', ' class="btn btn-sm btn-primary klik ajaxify" ');
            }
            $buttonDel = '';
            if ($accessDel->menuview > 0) {
                $buttonDel = '<button class="btn btn-sm btn-danger hapus-petani" data-id=' . "'" . $brand->id_petani . "'" . '><span tooltip="Hapus Petani"><i class="glyphicon glyphicon-trash"></i></button>';
            }

            $row[] = $buttonEdit . '  ' . $buttonDel;
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function Add() {

        /* ini harus ada boss */
        $data['userdata'] = $this->session->userdata('nama');
        $access = $this->M_sidebar->access('add', self::__kode_menu);
        if ($access->menuview == 0) {
            $data['page'] = self::__title;
            $data['judul'] = self::__title;
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        }

        /* ini harus ada boss */ else {
            $data['page'] = self::__title;
            $data['judul'] = self::__title;
            $data['kabupaten'] = $this->M_petani->selekKab();

            $this->loadkonten('' . self::__folder . 'tambah', $data);
        }
    }

    public function prosesAdd() {

        $username = $this->session->userdata('nama');
        $date = date('Y-m-d H:i:s');

        $this->db->trans_begin();

        $access = $this->M_sidebar->access('add', self::__kode_menu);
        if ($access->menuview == 0) {
            $out = array('status' => false, 'pesan' => 'You dont have access.');
        } else {

            $config['upload_path'] = "./upload/petani/";
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = '2048'; //maksimum besar file 2M
            $config['overwrite'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload("gambar")) {
                $image_data = $this->upload->data();
                $path['link'] = "upload/petani/";

                $data = array(
                    // 'no_biro' => $this->input->post('no_biro'),
                    'nama_kelompok' => $this->input->post('nama_kelompok'),
                    'nama_ketua' => $this->input->post('nama_ketua'),
                    'kabupaten' => $this->input->post('kabupaten'),
                    'kecamatan' => $this->input->post('kecamatan'),
                    'desa' => $this->input->post('desa'),
                    'folder' => $this->input->post('folder'),
                    'longitude' => $this->input->post('longitude'),
                    'latitude' => $this->input->post('latitude'),
                    'gambar' => $path['link'] . '' . $image_data['file_name'],
                    'status' => 'Non aktif',
                    'tipe_petani' => 'Baru',
                    'created_by' => $username,
                    'created_date' => $date
                );

                $result = $this->db->insert(self::__tableName, $data);
                $this->do_upload_others_images();

                if ($this->db->trans_status() === FALSE) {
                    $out = array('status' => false, 'pesan' => 'Maaf data gagal di simpan !');
                }

                if ($result > 0) {
                    $this->db->trans_commit();
                    $out = array('status' => true, 'pesan' => ' Data berhasil di simpan');
                } else {
                    $this->db->trans_rollback();
                    $out = array('status' => false, 'pesan' => 'Maaf data gagal di simpan !');
                }
            } else {
                $data = array(
                    'no_biro' => $this->input->post('no_biro'),
                    'nama_kelompok' => $this->input->post('nama_kelompok'),
                    'nama_ketua' => $this->input->post('nama_ketua'),
                    'kabupaten' => $this->input->post('kabupaten'),
                    'kecamatan' => $this->input->post('kecamatan'),
                    'desa' => $this->input->post('desa'),
                    'folder' => $this->input->post('folder'),
                    'longitude' => $this->input->post('longitude'),
                    'latitude' => $this->input->post('latitude'),
                    'gambar' => 'upload/gambar/no-image.png',
                    'status' => 'Non aktif',
                    'tipe_petani' => 'Baru',
                    'created_by' => $username,
                    'created_date' => $date
                );
                $result = $this->db->insert(self::__tableName, $data);
                $this->do_upload_others_images();
                if ($result > 0) {
                    $this->db->trans_commit();
                    $out = array('status' => true, 'pesan' => ' Data berhasil di simpan');
                } else {
                    $this->db->trans_rollback();
                    $out = array('status' => false, 'pesan' => 'Maaf data gagal di simpan !');
                }
            }
        }

        echo json_encode($out);
    }

    private $allowed_img_types = 'gif|jpg|png|jpeg|JPG|PNG|JPEG|pdf';

    private function do_upload_others_images() {
        $upath = './upload/petani/' . $_POST['folder'] . '/';
        if (!file_exists($upath)) {
            mkdir($upath, 0777);
        }

        $this->load->library('upload');

        $files = $_FILES;
        $cpt = count($_FILES['others']['name']);
        for ($i = 0; $i < $cpt; $i++) {
            unset($_FILES);
            $_FILES['others']['name'] = $files['others']['name'][$i];
            $_FILES['others']['type'] = $files['others']['type'][$i];
            $_FILES['others']['tmp_name'] = $files['others']['tmp_name'][$i];
            $_FILES['others']['error'] = $files['others']['error'][$i];
            $_FILES['others']['size'] = $files['others']['size'][$i];

            $this->upload->initialize(array(
                'upload_path' => $upath,
                'allowed_types' => $this->allowed_img_types
            ));
            $this->upload->do_upload('others');
        }
    }

    public function Edit($id) {

        /* ini harus ada boss */
        $data['userdata'] = $this->session->userdata('nama');
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        if ($access->menuview == 0) {
            $data['page'] = self::__title;
            $data['judul'] = self::__title;
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        }
        /* ini harus ada boss */ else {

            $where = array(self::__tableId => $id);
            $data['brand'] = $this->M_petani->selectById($id);
            $data['datastatus'] = $this->M_petani->selek_status();
            $data['kabupaten'] = $this->M_petani->selekKab();
            $data['kecamatan'] = $this->M_petani->selekKec();
            $data['desa'] = $this->M_petani->selekDes();

            $path = $this->M_petani->selectById($id);
            $path->folder;
            $res = $path->folder;

            $directory = 'upload/petani/' . $res;
            $data["images"] = glob($directory . "/*");


            $data['page'] = self::__title;
            $data['judul'] = self::__title;
            $this->loadkonten('' . self::__folder . 'update', $data);
        }
    }

    public function download() {
        if ($this->input->post('images')) {
            $images = $this->input->post('images');
            foreach ($images as $image) {
                $this->zip->read_file($image);
            }
            $this->zip->download('' . time() . '.zip');
        }
    }

    public function prosesUpdate() {

        $username = $this->session->userdata('nama');
        $date = date('Y-m-d H:i:s');

        $where = trim($this->input->post(self::__tableId));
        $where2 = trim($this->input->post('nama'));

        $this->db->trans_begin();

        $access = $this->M_sidebar->access('add', self::__kode_menu);
        if ($access->menuview == 0) {
            $out = array('status' => false, 'pesan' => 'You dont have access.');
        } else {

            $config['upload_path'] = "./upload/petani/";
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = '2048'; //maksimum besar file 2M
            $config['overwrite'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload("gambar")) {
                $image_data = $this->upload->data();
                $path['link'] = "upload/petani/";

                $data = array(
                    // 'no_biro' => $this->input->post('no_biro'),
                    'nama_kelompok' => $this->input->post('nama_kelompok'),
                    'nama_ketua' => $this->input->post('nama_ketua'),
                    'kabupaten' => $this->input->post('kabupaten'),
                    'kecamatan' => $this->input->post('kecamatan'),
                    'desa' => $this->input->post('desa'),
                    'status' => $this->input->post('status'),
                    'longitude' => $this->input->post('longitude'),
                    'latitude' => $this->input->post('latitude'),
                    'gambar' => $path['link'] . '' . $image_data['file_name'],
                    'created_by' => $username,
                    'created_date' => $date
                );


                $result = $this->db->update(self::__tableName, $data, array(self::__tableId => $where));

                if ($this->db->trans_status() === FALSE) {
                    $out = array('status' => false, 'pesan' => 'Maaf data gagal di update !');
                }

                if ($result > 0) {
                    $this->db->trans_commit();
                    $out = array('status' => true, 'pesan' => ' Data berhasil di update');
                } else {
                    $this->db->trans_rollback();
                    $out = array('status' => false, 'pesan' => 'Maaf data gagal di update !');
                }
            } else {
                $data = array(
                    'no_biro' => $this->input->post('no_biro'),
                    'nama_kelompok' => $this->input->post('nama_kelompok'),
                    'nama_ketua' => $this->input->post('nama_ketua'),
                    'kabupaten' => $this->input->post('kabupaten'),
                    'kecamatan' => $this->input->post('kecamatan'),
                    'desa' => $this->input->post('desa'),
                    'longitude' => $this->input->post('longitude'),
                    'latitude' => $this->input->post('latitude'),
                    'status' => $this->input->post('status'),
                    'created_by' => $username,
                    'created_date' => $date
                );

                $result = $this->db->update(self::__tableName, $data, array(self::__tableId => $where));

                if ($this->db->trans_status() === FALSE) {
                    $out = array('status' => false, 'pesan' => 'Maaf data gagal di update !');
                }

                if ($result > 0) {
                    $this->db->trans_commit();
                    $out = array('status' => true, 'pesan' => ' Data berhasil di update');
                } else {
                    $this->db->trans_rollback();
                    $out = array('status' => false, 'pesan' => 'Maaf data gagal di update !');
                }
            }
        }


        echo json_encode($out);
    }

    public function hapus() {

        $this->load->helper("file");
        $id = $_POST[self::__tableId];
        $path = $this->M_petani->selectById($id);
        $path->folder;
        $res = $path->folder;
        $path = 'upload/petani/' . $res;
        delete_files($path, true, false, 1);
        $result = $this->M_petani->hapus($id);
        if ($result > 0) {
            $out = array('status' => true, 'pesan' => ' Data berhasil di hapus');
        } else {
            $out = array('status' => false, 'pesan' => 'Maaf data gagal di hapus !');
        }
        echo json_encode($out);
    }

    public function import() {
        $error = false;

        $this->form_validation->set_rules('excel', 'File', 'trim|required');
        if ($_FILES['excel']['name'] == '') {
            
        } else {
            $config['upload_path'] = './assets/excel/';
            $config['allowed_types'] = 'xls|xlsx';

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('excel')) {
                $error = array('error' => $this->upload->display_errors());
            } else {
                $dataUpload = $this->upload->data();

                error_reporting(E_ALL);
                date_default_timezone_set('Asia/Jakarta');

                include './assets/phpexcel/Classes/PHPExcel/IOFactory.php';

                $inputFileName = './assets/excel/' . $dataUpload['file_name'];
                $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
                $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);

                $data = array();
                $index = 0;
                
                if (!$error) {
                foreach($sheetData as $row){
                $checkFiles = $this->M_petani->checkFiles($row['D']);
                if ($checkFiles == false) {
                array_push($data, array(
                                    'kabupaten'      => $row['A'],
                                    'kecamatan'      => $row['B'],
                                    'desa'           => $row['C'],
                                    'nama_kelompok'  => $row['D'],
                                    'nama_ketua'     => $row['E'],
                                    'tipe_petani'    => $row['F'],
                                ));
                
                $index++;
            }
        }
        }
        unlink('./assets/excel/' . $dataUpload['file_name']);

            if (!$error) {
                if (empty($data)) {
                    $out = array('status' => 'awas', 'pesan' => 'Maaf, Data sudah ada dalam sistem silahkan cek kembali data import terakhir !');
                    $error = true;
                }
            }

            if (!$error) {
            $result=$this->db->insert_batch('tbl_petani', $data);

            if ($result > 0) {
                $out = array('status' => true, 'pesan' => ' Data berhasil di import');
            } else {
                $out = array('status' => false, 'pesan' => 'Maaf data gagal di import !');
            }
        }
            
        }

            echo json_encode($out);
        }
    }

    public function import2() {

        $access = $this->M_sidebar->access('add', self::__kode_menu);
        if ($access->menuview == 0) {
            $out = array('status' => false, 'pesan' => 'You dont have access.');
        } else {


            $this->form_validation->set_rules('excel', 'File', 'trim|required');
            if ($_FILES['excel']['name'] == '') {
                
            } else {
                $config['upload_path'] = './assets/excel/';
                $config['allowed_types'] = 'xls|xlsx';

                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('excel')) {
                    $error = array('error' => $this->upload->display_errors());
                } else {
                    $data = $this->upload->data();

                    error_reporting(E_ALL);
                    date_default_timezone_set('Asia/Jakarta');

                    include './assets/phpexcel/Classes/PHPExcel/IOFactory.php';

                    $inputFileName = './assets/excel/' . $data['file_name'];
                    $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
                    $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);

                    $index = 0;
                    foreach ($sheetData as $key => $value) {
                        if ($key != 1) {
                            // $check = $this->M_ongkir->check($value['A']);
                            // if ($check != 1) {
                            $resultData[$index]['kode_kabupaten'] = $value['A'];
                            $resultData[$index]['kecamatan'] = $value['B'];
                            $resultData[$index]['kode_kecamatan'] = $value['C'];
                            // }
                        }
                        $index++;
                    }

                    unlink('./assets/excel/' . $data['file_name']);

                    if (count($resultData) != 0) {
                        $result = $this->M_petani->simpan_import2($resultData);
                        if ($result > 0) {
                            $out = array('status' => true, 'pesan' => ' Data berhasil di import');
                        } else {
                            $out = array('status' => false, 'pesan' => 'Maaf data gagal di import !');
                        }
                    } else {
                        $out = array('status' => false, 'pesan' => 'Maaf data gagal di import !');
                    }
                }
            }
        }
        echo json_encode($out);
    }

    public function import3() {

        $access = $this->M_sidebar->access('add', self::__kode_menu);
        if ($access->menuview == 0) {
            $out = array('status' => false, 'pesan' => 'You dont have access.');
        } else {


            $this->form_validation->set_rules('excel', 'File', 'trim|required');
            if ($_FILES['excel']['name'] == '') {
                
            } else {
                $config['upload_path'] = './assets/excel/';
                $config['allowed_types'] = 'xls|xlsx';

                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('excel')) {
                    $error = array('error' => $this->upload->display_errors());
                } else {
                    $data = $this->upload->data();

                    error_reporting(E_ALL);
                    date_default_timezone_set('Asia/Jakarta');

                    include './assets/phpexcel/Classes/PHPExcel/IOFactory.php';

                    $inputFileName = './assets/excel/' . $data['file_name'];
                    $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
                    $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);

                    $index = 0;
                    foreach ($sheetData as $key => $value) {
                        if ($key != 1) {
                            // $check = $this->M_ongkir->check($value['A']);
                            // if ($check != 1) {
                            $resultData[$index]['kecamatan'] = $value['A'];
                            $resultData[$index]['desa'] = $value['B'];
                            // }
                        }
                        $index++;
                    }

                    unlink('./assets/excel/' . $data['file_name']);

                    if (count($resultData) != 0) {
                        $result = $this->M_petani->simpan_import3($resultData);
                        if ($result > 0) {
                            $out = array('status' => true, 'pesan' => ' Data berhasil di import');
                        } else {
                            $out = array('status' => false, 'pesan' => 'Maaf data gagal di import !');
                        }
                    } else {
                        $out = array('status' => false, 'pesan' => 'Maaf data gagal di import !');
                    }
                }
            }
        }
        echo json_encode($out);
    }

    public function import4() {

        $access = $this->M_sidebar->access('add', self::__kode_menu);
        if ($access->menuview == 0) {
            $out = array('status' => false, 'pesan' => 'You dont have access.');
        } else {


            $this->form_validation->set_rules('excel', 'File', 'trim|required');
            if ($_FILES['excel']['name'] == '') {
                
            } else {
                $config['upload_path'] = './assets/excel/';
                $config['allowed_types'] = 'xls|xlsx';

                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('excel')) {
                    $error = array('error' => $this->upload->display_errors());
                } else {
                    $data = $this->upload->data();

                    error_reporting(E_ALL);
                    date_default_timezone_set('Asia/Jakarta');

                    include './assets/phpexcel/Classes/PHPExcel/IOFactory.php';

                    $inputFileName = './assets/excel/' . $data['file_name'];
                    $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
                    $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);

                    $index = 0;
                    foreach ($sheetData as $key => $value) {
                        if ($key != 1) {
                            // $check = $this->M_ongkir->check($value['A']);
                            // if ($check != 1) {
                            $resultData[$index]['jenis_bantuan'] = $value['A'];
                            $resultData[$index]['jenis_kegiatan'] = $value['B'];
                            $resultData[$index]['program'] = $value['C'];
                            $resultData[$index]['created_by'] = $value['D'];
                            // }
                        }
                        $index++;
                    }

                    unlink('./assets/excel/' . $data['file_name']);

                    if (count($resultData) != 0) {
                        $result = $this->M_petani->simpan_import4($resultData);
                        if ($result > 0) {
                            $out = array('status' => true, 'pesan' => ' Data berhasil di import');
                        } else {
                            $out = array('status' => false, 'pesan' => 'Maaf data gagal di import !');
                        }
                    } else {
                        $out = array('status' => false, 'pesan' => 'Maaf data gagal di import !');
                    }
                }
            }
        }
        echo json_encode($out);
    }

    public function getKelompokDpa($kelompok = '') {
        $sql = "SELECT * FROM tbl_history_pengajuan WHERE id_kelompok = \"{$kelompok}\" ORDER BY id_proposal DESC LIMIT 1";
        $data = $this->db->query($sql);
        return $data->row();
    }

}
