<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_proposal_penetapan extends AUTH_Controller
{
    const __tableName = 'tbl_proposal';
    const __tableId = 'id_proposal';
    const __tableIdname = 'name';
    const __folder = 'v_proposal_penetapan/';
    const __kode_menu = 'master-proposal-penetapan';
    const __title = 'Proposal Penetapan ';
    const __model = 'M_proposal_penetapan';
    const __getLimitDpaYear = 2;

    public function __construct()
    {
        parent::__construct();
        $this->load->model(self::__model);
        $this->load->model('M_petani');
        $this->load->model('M_sidebar');
    }

    public function loadkonten($page, $data)
    {

        $data['userdata'] = $this->userdata;
        $ajax = ($this->input->post('status_link') == "ajax" ? true : false);
        if (!$ajax) {
            $this->load->view('Dashboard/layouts/header', $data);
        }
        $this->load->view($page, $data);
        if (!$ajax)
            $this->load->view('Dashboard/layouts/footer', $data);
    }

    public function index()
    {
        $accessAdd = $this->M_sidebar->access('view', self::__kode_menu);
        $data['accessAdd'] = $accessAdd->menuview;
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = self::__title;

        $this->loadkonten('' . self::__folder . 'home', $data);
    }

    public function ajax_list()
    {
        $tanggalAwal = $this->input->post('tanggal_awal');
        $tanggalAkhir = $this->input->post('tanggal_akhir');

        $filter = array(
            'tanggal_awal' => $tanggalAwal,
            'tanggal_akhir' => $tanggalAkhir,
        );

        $accessEdit = $this->M_sidebar->access('edit', self::__kode_menu);
        $accessDel = $this->M_sidebar->access('del', self::__kode_menu);
        $list = $this->M_proposal_penetapan->getData(1, $filter);

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $brand) {
            $idUser = $brand->id_user;
            $dataUser = $this->M_proposal_penetapan->select_user($idUser);
            $namaUser = $dataUser->nama_lengkap;

            $dataKelompok = $this->M_petani->selectById($brand->id_petani);

            $status = '<small class="label pull-center bg-blue">Tidak Direkomendasi</small>';
            if ($brand->status == 'Rekom Bidang') {
                $status = '<small class="label pull-center bg-green">Rekom Bidang</small>';
            } else if ($brand->status == 'Penetapan') {
                $status = '<small class="label pull-center bg-green">Penetapan Proposal</small>';
            }

            if ($brand->kode == '') {
                $kode = '<small class="label pull-center bg-blue">Belum ada kode proposal</small>';
            } else {
                $kode = $brand->kode;
            }

            if ($brand->updated_by == NULL) {
                $proses = "Belum ada yang memproses";
            } else {
                $proses = $brand->updated_by;
            }

            $sqlProposalDetail = "SELECT * FROM tbl_proposal_detail WHERE id_proposal = '{$brand->id_proposal}'";
            $dataProposalDetail = $this->db->query($sqlProposalDetail)->result();

            $jenis_bantuan = "<ul>";
            foreach ($dataProposalDetail as $key => $value) {
                $qJenisBantuan = "SELECT * FROM tbl_jenis_bantuan WHERE id = '{$value->id_jenis_bantuan}'";
                $resJenisBantuan = $this->db->query($qJenisBantuan)->row();
                $jenis_bantuan .= "<li>" . $resJenisBantuan->jenis_bantuan . "</li>";
            }

            $jenis_bantuan .= "</ul>";
            $volume = "<ul>";
            foreach ($dataProposalDetail as $key => $value) {
                $volume .= "<li>" . $value->jumlah . "</li>";
            }
            $volume .= "</ul>";

            $satuan .= "</ul>";
            $satuan = "<ul>";
            foreach ($dataProposalDetail as $key => $value) {
                $satuan .= "<li>" . $value->satuan . "</li>";
            }
            $satuan .= "</ul>";

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $kode;
            $row[] = $dataKelompok->nama_kelompok;
            $row[] = (strlen($brand->nama_komoditi) > 0 ? $brand->nama_komoditi : $brand->komoditi);
            $row[] = $jenis_bantuan;
            $row[] = $volume;
            $row[] = $satuan;
            $row[] = $brand->kabupaten;
            $row[] = '' . $status . '<br> Proposal di Proses oleh : <b>' . $proses . '</b><hr> update terakhir pada : <br><b>' . date('d-m-Y', strtotime($brand->updated_date)) . '</b>';

            //add html for action

            $buttonEdit = '';
            $buttonPrint = '';
            if ($brand->status_dpa > 0) {
                if ($accessEdit->menuview > 0) {
                    $buttonPrint = "<a href='" . site_url('print-proposal/' . $brand->id_proposal) . "' class='btn btn-sm btn-success' target='__blank'><span tooltip='Print Data'><i class='fa fa-print'></i></a></span>";
                }
            } else {
                if ($accessEdit->menuview > 0) {
                    $buttonEdit = anchor('edit-proposal-penetapan/' . $brand->id_proposal, ' <span tooltip="Edit Data"><span class="fa fa-edit"></span> ', ' class="btn btn-sm btn-primary klik ajaxify" ');
                }
            }

            $buttonDel = '';
            if ($accessDel->menuview > 0) {
                $buttonDel = '<button class="btn btn-sm btn-danger hapus-proposal-penetapan" data-id=' . "'" . $brand->id_proposal . "'" . '><span tooltip="Hapus Data"><i class="glyphicon glyphicon-trash"></i></button>';
            }

            $row[] = $buttonPrint . ' ' . $buttonEdit . ' ' . $buttonDel;
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function Edit($id)
    {
        /* ini harus ada boss */
        $data['userdata'] = $this->userdata;
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        if ($access->menuview == 0) {
            $data['page'] = self::__title;
            $data['judul'] = self::__title;
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        } else {
            $where = array(self::__tableId => $id);
            $brand = $this->M_proposal_penetapan->selectById($id);
            $dataKelompok = $this->M_petani->selectById($brand->id_petani);

            $data['brand'] = $brand;
            $data['dataKelompok'] = $dataKelompok;
            $data['detail'] = $this->M_proposal_penetapan->getDataDetail($id);
            $data['status'] = $this->M_proposal_penetapan->selectStatus();
            $data['kode'] = $this->M_proposal_penetapan->kode();

            $data['page'] = self::__title;
            $data['judul'] = self::__title;
            $this->loadkonten('' . self::__folder . 'update', $data);
        }
    }

    public function download()
    {
        if ($this->input->post('images')) {
            $images = $this->input->post('images');
            foreach ($images as $image) {
                $this->zip->read_file($image);
            }
            $this->zip->download('' . time() . '.zip');
        }
    }

    // public function prosesUpdate()
    // {
    //     $errCode = 0;
    //     $errMessage = "";
    //     $username = $this->session->userdata('nama');
    //     $date = date('Y-m-d H:i:s');
    //     $date2 = date('Y-m-d');

    //     $where = trim($this->input->post(self::__tableId));
    //     $status = $this->input->post('status');
    //     $statusDpa = $this->input->post('status_dpa');
    //     $helper = $this->input->post('helper');
    //     $NoBiro = $this->input->post('no_biro');
    //     $KodeCheck = $this->input->post('kode2');

    //     if ($status == 'Penetapan') {
    //         $kode = $this->input->post('kode');
    //         $kodeHelper = 1;
    //     }

    //     if ($helper > 0) {
    //         $kode = $this->input->post('kode2');
    //     } 

    //     $this->db->trans_begin();
    //     if ($errCode == 0) {
    //         $access = $this->M_sidebar->access('edit', self::__kode_menu);
    //         if ($access->menuview == 0) {
    //             $errCode++;
    //             $errMessage = "You dont have access.";
    //         }
    //     }
    //     if ($errCode == 0) {
    //         $getData = $this->M_proposal_penetapan->selectById($where);
    //         $getKelompokDpa = parent::getKelompokDpa($getData->kelompok);
    //         if ($getKelompokDpa != null) {
    //             if (date('Y', strtotime($date2)) < date('Y', strtotime($getKelompokDpa->date))) {
    //                 $errCode++;
    //                 $errMessage = "Gagal approve proposal karena data kelompok telah mengajukan proposal pada tahun " . date('Y', strtotime($getKelompokDpa->created_date)) . " .";
    //             }
    //         }
    //     }

    //     if ($errCode == 0) {
    //         if (strlen($status) == 0) {
    //             $errCode++;
    //             $errMessage = "Status wajib di isi.";
    //         }
    //     }

    //     if ($errCode == 0) {
    //         try {
    //             $data = array(
    //                 'no_biro' => $NoBiro,
    //                 'kode' => $kode,
    //                 'status' => $status,
    //                 'helper' => $kodeHelper,
    //                 'updated_by' => $username,
    //                 'updated_date' => $date,
    //             );
    //             if (strlen($statusDpa) > 0) {
    //                 $data = array_merge($data, array(
    //                     'status_dpa' => $statusDpa,
    //                 ));
    //             }
    //             $result = $this->db->update(self::__tableName, $data, array(self::__tableId => $where));
    //         } catch (Exception $ex) {
    //             $errCode++;
    //             $errMessage = $ex->getMessage();
    //         }
    //     }
    //         if ($errCode == 0) {
    //         $getData = $this->M_proposal_penetapan->selectById($where);
    //         $getKelompokDpa = parent::getKelompokDpa($getData->kelompok);
    //         if ($getKelompokDpa != null) {
    //             if (date('Y', strtotime($date2)) < date('Y', strtotime($getKelompokDpa->date))) {
    //                 $errCode++;
    //                 $errMessage = "Gagal approve proposal karena data kelompok telah mengajukan proposal pada tahun " . date('Y', strtotime($getKelompokDpa->created_date)) . " .";
    //             }
    //         }
    //     }

    //     if ($errCode == 0) {
    //         if ($this->db->trans_status() === FALSE) {
    //             $errCode++;
    //             $errMessage = "Error saving databse.";
    //         }
    //     }
    //     if ($errCode == 0) {
    //         $this->db->trans_commit();
    //         $out['status'] = true;
    //         $out['pesan'] = 'Pengajuan berhasil di update.';
    //     } else {
    //         $this->db->trans_rollback();
    //         $out['status'] = false;
    //         $out['pesan'] = $errMessage;
    //     }

    //     echo json_encode($out);
    // }

    public function prosesUpdate()
    {
        $errCode = 0;
        $errMessage = "";

        $username = $this->session->userdata('nama');
        $date = date('Y-m-d H:i:s');
        $date2 = date('Y-m-d');

        $where = trim($this->input->post(self::__tableId));
        $status = $this->input->post('status');
        $statusDpa = $this->input->post('status_dpa');
        $helper = $this->input->post('helper');
        $NoBiro = $this->input->post('no_biro');
        $KodeCheck = $this->input->post('kode2');

        if ($status == 'Penetapan') {
            $kode = $this->input->post('kode');
            $kodeHelper = 1;
        }

        if ($helper > 0) {
            $kode = $this->input->post('kode2');
        } 


        $this->db->trans_begin();
        if ($errCode == 0) {
            $access = $this->M_sidebar->access('edit', self::__kode_menu);
            if ($access->menuview == 0) {
                $errCode++;
                $errMessage = "You dont have access.";
            }
        }
        if ($errCode == 0) {
            $getData = $this->M_proposal_penetapan->selectById($where);
            $getKelompokDpa = parent::getKelompokDpa($getData->kelompok);
            if ($getKelompokDpa != null) {
                if (date('Y', strtotime($date2)) < date('Y', strtotime($getKelompokDpa->date))) {
                    $errCode++;
                    $errMessage = "Gagal approve proposal karena data kelompok telah mengajukan proposal pada tahun " . date('Y', strtotime($getKelompokDpa->created_date)) . " .";
                }
            }
        }
        if ($errCode == 0) {
            if (strlen($status) == 0) {
                $errCode++;
                $errMessage = "Status wajib di isi.";
            }
        }

        if ($errCode == 0) {
            try {
                $data = array(
                    'status' => $status,
                    'no_biro' => $NoBiro,
                    'kode' => $kode,
                    'status' => $status,
                    'helper' => $kodeHelper,
                    'updated_by' => $username,
                    'updated_date' => $date,
                );
                if (strlen($statusDpa) > 0) {
                    $data = array_merge($data, array(
                        'status_dpa' => $statusDpa,
                    ));
                }
                $result = $this->db->update(self::__tableName, $data, array(self::__tableId => $where));

                $data2 = array(
                    'kode' => $where,
                    'id_admin' => $this->session->userdata('id'),
                    'status' => $status,
                    'keterangan' => 'user <b>' . $username . '</b> Sedang melakukan proses ' . $status . ' proposal ke Dinas Perkebunan Jawa Timur',
                    'created_by' => 'System',
                    'created_date' => $date,
                );
                $result = $this->db->insert('tbl_log', $data2);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if (strlen($statusDpa) > 0) {
                try {
                    $limitDate = date('Y-m-d', strtotime($date . "+" . self::__getLimitDpaYear . " years"));
                    $dataHistoryPengajuan = array(
                        self::__tableId => $where,
                        'id_kelompok' => $getData->id_petani,
                        'date' => $limitDate,
                        'description' => "Pengajuan proposal pada tahun " . date('Y', strtotime($date)),
                        'created_by' => $username,
                        'created_date' => $date,
                        'updated_by' => $username,
                        'updated_date' => $date,
                    );
                    $this->db->insert('tbl_history_pengajuan', $dataHistoryPengajuan);
                } catch (Exception $ex) {
                    $errCode++;
                    $errMessage = $ex->getMessage();
                }
            }
        }

        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }
        if ($errCode == 0) {
            $this->db->trans_commit();
            $out['status'] = true;
            $out['pesan'] = 'Pengajuan berhasil di update.';
        } else {
            $this->db->trans_rollback();
            $out['status'] = false;
            $out['pesan'] = $errMessage;
        }

        echo json_encode($out);
    }

    public function hapus()
    {
        $id = $_POST[self::__tableId];
        $getData = $this->M_proposal_penetapan->selectById($id);
        $result = $this->M_proposal_penetapan->hapus($id);
        $result = $this->db->delete('tbl_log', array('kode' => $id));
        if ($getData->status_dpa > 0) {
            $result = $this->M_proposal_penetapan->hapusDpa($id);
        }
        if ($result > 0) {
            $out = array('status' => true, 'pesan' => ' Data berhasil di hapus');
        } else {
            $out = array('status' => false, 'pesan' => 'Maaf data gagal di hapus !');
        }
        echo json_encode($out);
    }
}
