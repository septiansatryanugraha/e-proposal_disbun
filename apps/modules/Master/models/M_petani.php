<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_petani extends CI_Model
{
    const __tableName = 'tbl_petani';
    const __tableId = 'id_petani';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function checkFiles($value)
    {
        $this->db->select('nama_kelompok');
        $this->db->from('tbl_petani');
        $this->db->like('nama_kelompok', $value);
        // $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    function getData($isAjaxList = 0, $filter = array())
    {
        $status = $filter['status'];
        $tanggalAwal = $filter['tanggal_awal'];
        $tanggalAkhir = $filter['tanggal_akhir'];

        $sql = "SELECT * FROM " . self::__tableName . " WHERE 1=1";
        if (strlen($status) > 0 && $status != 'all') {
            $sql .= " AND status = '{$status}'";
        }
        if (strlen($tanggalAwal) > 0 && strlen($tanggalAkhir) > 0) {
            $tanggalAwal = date('Y-m-d H:i:s', strtotime($tanggalAwal . ' 00:00:00'));
            $tanggalAkhir = date('Y-m-d H:i:s', strtotime($tanggalAkhir . ' 23:58:59'));
            $sql .= " AND created_date >= '{$tanggalAwal}' AND created_date <= '{$tanggalAkhir}'";
        }

        if ($isAjaxList > 0) {
            $sql .= " ORDER BY id_petani DESC";
        }

        $data = $this->db->query($sql);
        return $data->result();
    }

    public function selectById($id)
    {
        $sql = "SELECT * FROM " . self::__tableName . " WHERE " . self::__tableId . " = '{$id}'";
        $data = $this->db->query($sql);
        return $data->row();
    }

    public function selectByName($name)
    {
        $sql = "SELECT * FROM " . self::__tableName . " WHERE nama_kelompok = '{$name}'";
        $data = $this->db->query($sql);
        return $data->row();
    }

    public function selek_status()
    {

        $sql = " select * from status_grup WHERE nama in ('Non aktif','Aktif')";
        $data = $this->db->query($sql);
        return $data->result();
    }

    public function selekKab()
    {
        $sql = " select * from tbl_kabupaten";
        $data = $this->db->query($sql);
        return $data->result();
    }

    public function selekKec()
    {
        $this->db->from('tbl_kecamatan');
        $data = $this->db->get();
        return $data->result();
    }

    public function selekDes()
    {
        $this->db->from('tbl_desa');
        $data = $this->db->get();
        return $data->result();
    }

    public function hapus($id)
    {
        $sql = "DELETE FROM " . self::__tableName . " WHERE  " . self::__tableId . " = '{$id}'";

        $this->db->query($sql);

        return $this->db->affected_rows();
    }

    public function simpan_import($data)
    {
        $this->db->insert_batch(self::__tableName, $data);
        return $this->db->affected_rows();
    }

    public function simpan_import2($data)
    {
        $this->db->insert_batch('tbl_kecamatan', $data);
        return $this->db->affected_rows();
    }

    public function simpan_import3($data)
    {
        $this->db->insert_batch('tbl_desa', $data);
        return $this->db->affected_rows();
    }

    public function simpan_import4($data)
    {
        $this->db->insert_batch('tbl_kegiatan', $data);
        return $this->db->affected_rows();
    }

    public function select_kabupaten($kode)
    {

        $this->db->order_by('id', 'ASC');
        $kab = $this->db->get_where('tbl_kabupaten', array('kode' => $kode));
        foreach ($kab->result_array() as $data) {
            $res .= "$data[kabupaten]";
        }
        return $res;
    }

    public function select_kecamatan($kode)
    {

        $kabupaten = "<option value='0'>-- Pilih Kecamatan --</option>";
        $this->db->order_by('id', 'ASC');
        $kab = $this->db->get_where('tbl_kecamatan', array('kode_kabupaten' => $kode));

        foreach ($kab->result_array() as $data) {
            $kabupaten .= "<option value='$data[kecamatan]'>$data[kecamatan]</option>";
        }
        return $kabupaten;
    }

    function select_desa($kec)
    {


        $kecamatan = "<option value='0'>-- Pilih Desa --</option>";
        $this->db->order_by('id', 'ASC');
        $this->db->group_by("desa");
        $kec = $this->db->get_where('tbl_desa', array('kecamatan' => $kec));

        foreach ($kec->result_array() as $data) {
            $kecamatan .= "<option value='$data[desa]'>$data[desa]</option>";
        }

        return $kecamatan;
    }
}
