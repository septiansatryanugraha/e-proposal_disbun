<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_proposal_penetapan extends CI_Model
{
    const __tableName = 'tbl_proposal';
    const __tableId = 'id_proposal';
    const __tableName2 = 'tbl_user';
    const __tableId2 = 'id_user';
    const __tableId4 = 'kode';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function getData($isAjaxList = 0, $filter = array())
    {
        $groupId = $this->session->userdata('grup_id');
        $tahunSession = isset($_SESSION['tahun']) ? $_SESSION['tahun'] : date('Y');
        $tanggalAwal = $filter['tanggal_awal'];
        $tanggalAkhir = $filter['tanggal_akhir'];

        $sql = "SELECT " . self::__tableName . ".*, tbl_komoditi.nama as nama_komoditi";
        $sql .= " FROM " . self::__tableName . " as " . self::__tableName;
        $sql .= " LEFT JOIN tbl_komoditi ON tbl_komoditi.id_komoditi = " . self::__tableName . ".id_komoditi";
        $sql .= " WHERE " . self::__tableName . ".status in ('Rekom Bidang','Penetapan')";
        $sql .= " AND YEAR(" . self::__tableName . ".created_date) = '{$tahunSession}'";
        if (strlen($tanggalAwal) > 0 && strlen($tanggalAkhir) > 0) {
            $tanggalAwal = date('Y-m-d H:i:s', strtotime($tanggalAwal . ' 00:00:00'));
            $tanggalAkhir = date('Y-m-d H:i:s', strtotime($tanggalAkhir . ' 23:58:59'));
            $sql .= " AND " . self::__tableName . ".created_date >= '{$tanggalAwal}'";
            $sql .= " AND " . self::__tableName . ".created_date <= '{$tanggalAkhir}'";
        }
        if ($groupId > 2) {
            $sql .= " AND tbl_komoditi.grup_id = '{$groupId}'";
        }
        if ($isAjaxList > 0) {
            $sql .= " ORDER BY id_proposal DESC";
        }

        $data = $this->db->query($sql);
        return $data->result();
    }

    function getDataDetail($id)
    {
        $sql = " select * from " . self::__tableName . "_detail WHERE " . self::__tableId . " = '{$id}'";
        $data = $this->db->query($sql);
        return $data->result();
    }

    public function kode()
    {
        $this->db->select('RIGHT(tbl_proposal.kode,4) as kode', FALSE);
        $this->db->order_by('kode', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get('tbl_proposal');  //cek dulu apakah ada sudah ada kode di tabel.    
        if ($query->num_rows() <> 0) {
            //cek kode jika telah tersedia    
            $data = $query->row();
            $kode = intval($data->kode) + 1;
        } else {
            $kode = 1;  //cek jika kode belum terdapat pada table
        }
        $tahun = date('y');
        $bulan = date('m');
        $batas = str_pad($kode, 4, "0", STR_PAD_LEFT);
        $kodetampil = $tahun.'.'. $batas;  //format kode
        return $kodetampil;
    }

    function selectStatus()
    {
        $sql = " select * from status_grup WHERE nama in ('Rekom Bidang','Penetapan')";
        $data = $this->db->query($sql);
        return $data->result();
    }

    public function selectById($id)
    {
        $sql = "SELECT * FROM " . self::__tableName . " WHERE " . self::__tableId . " = '{$id}'";
        $data = $this->db->query($sql);
        return $data->row();
    }

    public function selectById2($id)
    {
        $sql = "SELECT * FROM " . self::__tableName . " WHERE " . self::__tableId4 . " = '{$id}'";
        $data = $this->db->query($sql);
        return $data->row();
    }

    public function select_user($id)
    {
        $sql = "SELECT * FROM " . self::__tableName2 . " WHERE " . self::__tableId2 . " = '{$id}'";
        $data = $this->db->query($sql);
        return $data->row();
    }

    public function hapus($id)
    {
        $sql = "DELETE FROM " . self::__tableName . " WHERE  " . self::__tableId . " = '{$id}'";
        $this->db->query($sql);
        return $this->db->affected_rows();
    }

    public function hapusDpa($id)
    {
        $sql = "DELETE FROM tbl_history_pengajuan WHERE  " . self::__tableId . " = '{$id}'";
        $this->db->query($sql);
        return $this->db->affected_rows();
    }
}
