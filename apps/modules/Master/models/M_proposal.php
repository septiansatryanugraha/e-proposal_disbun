<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_proposal extends CI_Model
{
    const __tableName = 'tbl_proposal';
    const __tableId = 'id_proposal';
    const __tableName2 = 'tbl_user';
    const __tableId2 = 'id_user';
    const __tableName3 = 'tbl_history_dokumen';
    const __tableId3 = 'id_history';
    const __tableId4 = 'kode';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function getData($isAjaxList = 0, $filter = array())
    {
        $groupId = $this->session->userdata('grup_id');
        $tahunSession = isset($_SESSION['tahun']) ? $_SESSION['tahun'] : date('Y');
        $tanggalAwal = $filter['tanggal_awal'];
        $tanggalAkhir = $filter['tanggal_akhir'];

        $sql = "SELECT " . self::__tableName . ".*, tbl_komoditi.nama as nama_komoditi";
        $sql .= " FROM " . self::__tableName . " as " . self::__tableName;
        $sql .= " LEFT JOIN tbl_komoditi ON tbl_komoditi.id_komoditi = " . self::__tableName . ".id_komoditi";
        $sql .= " WHERE " . self::__tableName . ".status in ('Usulan Baru','Verifikasi','Tidak Rekom','Rekom Bidang')";
        $sql .= " AND YEAR(" . self::__tableName . ".created_date) = '{$tahunSession}'";
        if (strlen($tanggalAwal) > 0 && strlen($tanggalAkhir) > 0) {
            $tanggalAwal = date('Y-m-d H:i:s', strtotime($tanggalAwal . ' 00:00:00'));
            $tanggalAkhir = date('Y-m-d H:i:s', strtotime($tanggalAkhir . ' 23:58:59'));
            $sql .= " AND " . self::__tableName . ".created_date >= '{$tanggalAwal}'";
            $sql .= " AND " . self::__tableName . ".created_date <= '{$tanggalAkhir}'";
        }
        if ($groupId > 2) {
            $sql .= " AND tbl_komoditi.grup_id = '{$groupId}'";
        }
        if ($isAjaxList > 0) {
            $sql .= " ORDER BY id_proposal DESC";
        }
        
        $data = $this->db->query($sql);
        return $data->result();
    }

    function getDataDetail($id)
    {
        $sql = " select * from " . self::__tableName . "_detail WHERE " . self::__tableId . " = '{$id}'";
        $data = $this->db->query($sql);
        return $data->result();
    }

    public function selekKategori()
    {
        $this->db->from('tbl_usaha');
        $data = $this->db->get();
        return $data->result();
    }

    function selectStatus()
    {
        $sql = " select * from status_grup WHERE nama in ('Usulan Baru','Verifikasi')";
        $data = $this->db->query($sql);
        return $data->result();
    }

    public function selectById($id)
    {
        $sql = "SELECT * FROM " . self::__tableName . " WHERE " . self::__tableId . " = '{$id}'";
        $data = $this->db->query($sql);
        return $data->row();
    }

    public function selectById2($id)
    {
        $sql = "SELECT * FROM " . self::__tableName . " WHERE " . self::__tableId4 . " = '{$id}'";
        $data = $this->db->query($sql);
        return $data->row();
    }

    public function select_user($id)
    {
        $sql = "SELECT * FROM " . self::__tableName2 . " WHERE " . self::__tableId2 . " = '{$id}'";
        $data = $this->db->query($sql);
        return $data->row();
    }

    public function hapus($id)
    {
        $sql = "DELETE FROM " . self::__tableName . " WHERE  " . self::__tableId . " = '{$id}'";
        $this->db->query($sql);
        return $this->db->affected_rows();
    }
}
