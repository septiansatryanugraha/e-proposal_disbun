<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdl_opd extends CI_Model {

	const __tableName = 'tbl_user';
    const __tableId = 'id_user';
    const __tableDinas = 'tbl_cat_opd';
 

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
 
	// function get_data() {
 //        $this->db->from(self::__tableName);
 //        $this->db->order_by(self::__tableId);
 //        $data = $this->db->get();
 //        return $data->result();
 //    }

    function getData($isAjaxList = 0, $filter = array()) {
        $tanggalAwal = $filter['tanggal_awal'];
        $tanggalAkhir = $filter['tanggal_akhir'];

        $sql = "SELECT * FROM " . self::__tableName . " WHERE 1=1";
        if (strlen($tanggalAwal) > 0 && strlen($tanggalAkhir) > 0) {
            $tanggalAwal = date('Y-m-d H:i:s', strtotime($tanggalAwal . ' 00:00:00'));
            $tanggalAkhir = date('Y-m-d H:i:s', strtotime($tanggalAkhir . ' 23:58:59'));
            $sql .= " AND created_date >= '{$tanggalAwal}' AND created_date <= '{$tanggalAkhir}'";
        } 

        if ($isAjaxList > 0) {
            $sql .= " ORDER BY id_user DESC";
        }

        $data = $this->db->query($sql);
        return $data->result();
    }
	
	public function selectById($id) {
        $sql = "SELECT tbl_user.*, tbl_login.password FROM tbl_user, tbl_login WHERE tbl_user.nama=tbl_login.nama and id_user=$id";
        $data = $this->db->query($sql);
        return $data->row();
    }

    public function selek_status() {
        
        $sql = " select * from status_grup WHERE nama in ('Non aktif','Aktif')";
        $data = $this->db->query($sql);
        return $data->result();
    }


	public function hapus($id) {
		$sql = "DELETE FROM " . self::__tableName . " WHERE  ". self::__tableId . " = '{$id}'";

		$this->db->query($sql);

		return $this->db->affected_rows();
	}

    public function checkEmail($data){
        $this->db->select('email');
        $this->db->from('tbl_user');
        $this->db->where('email', $data['email']);
        $this->db->limit(1);
         
        $query = $this->db->get();
        if($query->num_rows() == 1) { 
            return $query->result();
        } else {
            return false;
        }
    }



    public function selekDinas() {
        $this->db->from(self::__tableDinas);
        $data = $this->db->get();
        return $data->result();
    }

	
}
