<?php $this->load->view('_heading/_headerContent') ?>

<style>
    .font-data {
        font-family: Futura,Trebuchet MS,Arial,sans-serif; 
        color:red;
        font-size: 13px;
    }

    .kiri {
        margin-left: -10px;
    }

    .gap {
        margin-top: 30px;
    }
</style>

<?php
$dataKelompok = $this->M_petani->selectById($brand->id_petani);
$qKomoditi = "SELECT * FROM tbl_komoditi WHERE id_komoditi = '{$brand->id_komoditi}'";
$resKomoditi = $this->db->query($qKomoditi)->row();

?>

<section class="content">
    <!-- style loading -->
    <div class="loading2"></div>
    <!-- -->
    <div class="row">
        <div class="col-md-9">
            <div class="box box-primary">
                <div class="nav-tabs-custom">
                    <form class="form-horizontal" id="form-update" method="POST">
                        <input type="hidden" name="last_update_by" value="<?php echo $userdata->nama; ?>">
                        <input type="hidden" name="id_user" value="<?php echo $brand->id_user; ?>">
                        <input type="hidden" class="form-control" name="id_proposal" value="<?php echo $brand->id_proposal; ?>">
                        <input type="hidden" class="form-control" name="kode" value="<?php echo $brand->kode; ?>">

                        <div class="box-header with-border">
                            <h3 class="box-title">View Data Pengajuan</h3>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Nama Kelompok</label>
                                <div class="col-sm-7">
                                    <p class="form-control-static"><?php echo $dataKelompok->nama_kelompok; ?></p>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Nama Ketua</label>
                                <div class="col-sm-7">
                                    <p class="form-control-static"><?php echo $dataKelompok->nama_ketua; ?></p>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Desa</label>
                                <div class="col-sm-7">
                                    <p class="form-control-static"><?php echo $brand->desa; ?></p>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Kecamatan</label>
                                <div class="col-sm-7">
                                    <p class="form-control-static"><?php echo $brand->kecamatan; ?></p>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Kabupaten</label>
                                <div class="col-sm-7">
                                    <p class="form-control-static"><?php echo $brand->kabupaten; ?></p>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Komoditi</label>
                                <div class="col-sm-7">
                                    <p class="form-control-static"><?php echo $resKomoditi->nama; ?></p>
                                </div>
                            </div>

                            <div class="kiri">
                                <table id="example" class="table" style="width: 80%;" role="grid" aria-describedby="example_info">
                                    <thead>
                                        <tr role="row">
                                            <th tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 30%;">Jenis Bantuan</th>
                                            <th tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 15%;">Volume</th>
                                            <th tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 10%;">Satuan</th>
                                            <!-- <th tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 10%;">Prioritas</th> -->
                                            <th tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 35%;">Keterangan</th>
                                        </tr> 
                                    </thead>
                                    <tbody id="dd_list" class="dd-list outer">
                                        <?php foreach ($detail as $key => $value) { 
                                            $JenisBantuan = "SELECT * FROM tbl_jenis_bantuan WHERE id = '{$value->jenis_bantuan}'";
                                     $resJenisBantuan = $this->db->query($JenisBantuan)->row();

                                            ?>
                                            <tr role="row" class="odd dd-item">
                                                <td><?php echo $resJenisBantuan->jenis_bantuan; ?></td>
                                                <td><?php echo $value->jumlah; ?></td>
                                                <td><?php echo $value->satuan; ?></td>
                                                <!-- <td><?php echo $value->prioritas; ?></td> -->
                                                <td><?php echo $value->catatan; ?></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>

                            <div class="kiri">
                                <table id="example" class="ui celled table dataTable no-footer" style="width: 80%;" role="grid" aria-describedby="example_info">
                                    <thead>
                                        <tr role="row">
                                            <th tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 100%;">Program</th>
                                        </tr> 
                                    </thead>
                                    <tbody id="dd_list_program" class="dd-list-program outer-program">
                                        <?php foreach ($detail as $key => $value) { ?>
                                            <tr role="row" class="odd dd-item">
                                                <td><?php echo $value->program; ?></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>

                            <div class="kiri">
                                <table id="example" class="ui celled table dataTable no-footer" style="width: 80%;" role="grid" aria-describedby="example_info">
                                    <thead>
                                        <tr role="row">
                                            <th tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 100%;">Kegiatan</th>
                                        </tr> 
                                    </thead>
                                    <tbody id="dd_list_kegiatan" class="dd-list-kegiatan outer-kegiatan">
                                        <?php foreach ($detail as $key => $value) { ?>
                                            <tr role="row" class="odd dd-item">
                                                <td><?php echo $value->kegiatan; ?></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>

                            <div class="gap"></div>    
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Status</label>
                                <div class="col-sm-5">
                                    <select name="status" class="form-control selek-status" id="status_dokumen">
                                        <?php foreach ($status as $data) { ?>
                                            <option></option>
                                            <option value="<?php echo $data->nama ?>"<?php
                                            if ($data->nama == $brand->status) {
                                                echo"selected='selected'";
                                            }

                                            ?>><?php echo $data->nama; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>  
                        </div>

                        <div class="gap"></div>

                        <div class="box-footer">
                            <button name="simpan" id="simpan" type="button" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Proses</button>
                            <a class="klik ajaxify" href="<?php echo site_url('master-proposal-rekom'); ?>"><button class="btn btn-primary btn-flat" ><i class="fa fa-arrow-left"></i> Kembali</button></a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>  

<script type="text/javascript">
    //Proses Controller logic ajax
    $('#simpan').click(function () {
        swal({
            title: "Proses Data?",
            text: "Apakah anda yakin memproses data ini?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Simpan",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        },
                function () {
                    $(".confirm").attr('disabled', 'disabled');
                    var data = $("#form-update").serialize();
                    $.ajax({
                        method: 'POST',
                        beforeSend: function () {
                            $("#loadingImgSpecial").show();
                        },
                        url: '<?php echo base_url('Master/Master_proposal_rekom/prosesUpdate'); ?>',
                        data: data,
                    }).done(function (data) {
                        var result = jQuery.parseJSON(data);
                        if (result.status == true) {
                            $(".loading2").hide();
                            $(".loading2").modal('hide');
                            swal("Success", result.pesan, "success");
                            setTimeout("window.location='<?php echo site_url('Master/Master_proposal_rekom'); ?>'", 450);
                        } else {
                            $(".loading2").hide();
                            $(".loading2").modal('hide');
                            swal("Warning", result.pesan, "warning");
                        }
                    })
                });
    });

    // untuk select2 ajak pilih department
    $(function () {
        $(".selek-status").select2({
            placeholder: " -- Pilih Status -- "
        });
    });

</script>









