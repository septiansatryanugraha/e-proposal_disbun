<?php $this->load->view('_heading/_headerContent') ?>

<style>
#osas {
color:red;
font-weight:bold;
margin-left:0px;
}

  .field-icon {
  float: left;
  margin-left: 93%;
  margin-top: -25px;
  position: relative;
  z-index: 2;
 }

 #loadingImg
 {
 margin-left: 17%;
 }

 #loadingImg2
 {
 margin-left: 17%;
 }

 #slider
 {
 margin-left: 1%;
 }

  .font-loading
{
  font-family: Futura,Trebuchet MS,Arial,sans-serif; 
  color:red;
  font-size: 14px;
}

</style>
        
<section class="content">
    
    
    <!-- style loading -->
    <div class="loading2"></div>
    <!-- -->
    
    <div class="box">
        <div class="row">
        <div class="col-md-10">
        <!-- form start -->
        <form class="form-horizontal" id="form-tambah" method="POST">
        <input type="hidden" name="created_by" value="<?php echo $userdata->nama; ?>">
        <input type="hidden"  name="id_petani" value="<?php echo $brand->id_petani; ?>">
      
        <div class="box-body">
               
       <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Nama Kelompok Tani</label>
        <div class="col-sm-5">
        <input type="text" class="form-control" name="nama_kelompok" placeholder="Nama Kelompok" id="nama_kelompok" value="<?php echo $brand->nama_kelompok; ?>">
        </div>
        </div>

        <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Ketua Kelompok</label>
        <div class="col-sm-5">
        <input type="text" class="form-control" name="nama_ketua" placeholder="Nama Ketua Kelompok" id="nama_ketua" value="<?php echo $brand->nama_ketua; ?>">
        </div>
        </div>

     <div class="form-group">
        <label class="col-sm-2 control-label">Kabupaten</label>
        <div class="col-sm-3">
        <select  class="form-control selek-status" id="kabupaten">
        <?php foreach ($kabupaten as $data) { ?>
        <option value="<?php echo $data->kode; ?>"<?php if($data->kabupaten == $brand->kabupaten){echo "selected='selected'";} ?>><?php echo $data->kabupaten; ?>
        </option>
        <?php } ?>
        </select>
        </div>
        </div> 

     <div id="loadingImg">
     <img src="<?php echo base_url().'assets/' ?>tambahan/gambar/loading-bubble.gif">
     <p class="font-loading">Proccessing Data</font></p>
     </div>

     <input type="hidden" class="form-control"  name="kabupaten" id="nama_kab" value="<?php echo $brand->kabupaten; ?>">

     <div class="form-group">
        <label class="col-sm-2 control-label">Kecamatan</label>
        <div class="col-sm-3">
        <select name="kecamatan" class="form-control selek-kecamatan" id="kecamatan">
        <?php foreach ($kecamatan as $data) { ?>
        <option value="<?php echo $data->kecamatan; ?>"<?php if($data->kecamatan == $brand->kecamatan){echo "selected='selected'";} ?>><?php echo $data->kecamatan; ?>
        </option>
        <?php } ?>
        </select>
        </div>
        </div>

     <div id="loadingImg2">
     <img src="<?php echo base_url().'assets/' ?>tambahan/gambar/loading-bubble.gif">
     <p class="font-loading">Proccessing Data</font></p>
     </div> 

     <div class="form-group">
     <label class="col-sm-2 control-label">Desa</label>
     <div class="col-sm-3">
     <select name="desa" class="form-control selek-desa" id="desa">
     <?php foreach ($desa as $data) { ?>
     <option value="<?php echo $data->desa; ?>"<?php if($data->desa == $brand->desa){echo "selected='selected'";} ?>><?php echo $data->desa; ?>
     </option>
     <?php } ?>
     </select>
     </div>
     </div>

     <div class="form-group">
        <label class="col-sm-2 control-label">Koordinat</label>
        <div class="row">
        <div class="col-sm-3">
        <input type="text" class="form-control" name="latitude" placeholder="latitude" value="<?php echo $brand->latitude; ?>">
        </div>
        <div class="col-sm-3">
       <input type="text" class="form-control" name="longitude" placeholder="longitude"value="<?php echo $brand->longitude; ?>" >
        </div>
        </div>    
        </div>

     <div class="form-group">
        <div id="slider">
        <div id="slider">
        <?php if($petani->gambar != NULL) {  ?>
        <img class="img-thumbnail" src='../<?php echo $brand->gambar; ?>'>
        <?php } else { ?>
        <img class="img-thumbnail" src="<?php echo base_url();?>/assets/tambahan/gambar/tidak-ada.png" width="200px" />
        <?php }  ?>
        </div><br>
        </div>
            
        <label for="inputFoto" class="col-sm-2 control-label">Gambar</label>
        <div class="col-sm-5">
        <input type="file" class="form-control" placeholder="gambar" name="gambar" id="gambar" onchange="return fileValidation()"/>
        <p style='color: red; font-size: 14px;'> *Maksimal File Foto 2 MB</p>
        </div>
        </div>

     <div class="form-group">
     <label class="col-sm-2 control-label">Status</label>
     <div class="col-sm-3">
     <select name="status" class="form-control selek-status" aria-describedby="sizing-addon2">
     <?php foreach ($datastatus as $status) { ?>
     <option value="<?php echo $status->nama; ?>"<?php if($status->nama == $brand->status){echo "selected='selected'";} ?>><?php echo $status->nama; ?>
     </option>
     <?php } ?>
     </select>
     </div>
     </div>


        </div>
        <div class="box-footer">
        <button name="submit" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Simpan</button>
        <a class="klik ajaxify" href="<?php echo site_url('master-petani'); ?>"><button class="btn btn-primary btn-flat" ><i class="fa fa-arrow-left"></i> Kembali</button></a>
        </div>
        </form>
      
        </div>
        <!-- /.box -->
       
        </div>
          <!-- /.row -->
    </div>

    </section>


    <script type="text/javascript">
    //Proses Controller logic ajax

    function cekemail(a) {
    re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(a);
    }

    function cekpassword(a) {
    re2 = /^\S{3,}$/;
    return re2.test(a);
    }



    $('#form-tambah').submit(function(e) {
      
        var error = 0;
        var message = "";

        var data = $(this).serialize();

        var nama_kelompok = $("#nama_kelompok").val();
        var nama_kelompok = nama_kelompok.trim();

          if (error == 0) {
            if (nama_kelompok.length == 0) {
                error++;
                message = "Nama Kelompok wajib di isi.";
            }
        }

        var nama_ketua = $("#nama_ketua").val();
        var nama_ketua = nama_ketua.trim();

          if (error == 0) {
            if (nama_ketua.length == 0) {
                error++;
                message = "Nama Kelompok wajib di isi.";
            }
        }


        var nama_kab = $("#kabupaten").val();
        var nama_kab = nama_kab.trim();

          if (error == 0) {
            if (nama_kab.length == 0) {
                error++;
                message = "Kabupaten wajib di isi.";
            }
        }

        var kecamatan = $("#kecamatan").val();
        var kecamatan = kecamatan.trim();

          if (error == 0) {
            if (kecamatan.length == 0) {
                error++;
                message = "Kecamatan wajib di isi.";
            }
        }

        var desa = $("#desa").val();
        var desa = desa.trim();

          if (error == 0) {
            if (desa.length == 0) {
                error++;
                message = "Desa wajib di isi.";
            }
        }

      
    if (error == 0) {
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                $(".loading2").show();
                $(".loading2").modal('show');
                },
                url: '<?php echo base_url('Master/Petani/prosesUpdate'); ?>',
                type:"post",
                data:new FormData(this),
                processData:false,
                contentType:false,
                cache:false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == true) {
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    swal("Success", result.pesan, "success");
                } else {
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    swal("Warning", result.pesan, "warning");
                }
            })
            e.preventDefault();
        } else {
            toastr.error(message,'Warning', {timeOut: 5000},toastr.options = {
             "closeButton": true}); 
            return false;

        }
    });


    // untuk show hide password
    $(".toggle-password").click(function() {

    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $($(this).attr("toggle"));
    if (input.attr("type") == "password") {
    input.attr("type", "text");
    } else {
    input.attr("type", "password");
    }
    });


     // untuk select2 ajak pilih department
    $(function () 
    {
    $(".selek-dinas").select2({
        placeholder: " -- Pilih Kabupaten -- "
        });
    });

      // untuk select2 ajak pilih department
    $(function () 
    {
    $(".selek-kecamatan").select2({
        placeholder: " -- Pilih Kecamatan -- "
        });
    });


      // untuk select2 ajak pilih department
    $(function () 
    {
    $(".selek-desa").select2({
        placeholder: " -- Pilih desa -- "
        });
    });

      // untuk select2 ajak pilih department
    $(function () 
    {
    $(".selek-status").select2({
        placeholder: " -- Pilih status -- "
        });
    });

  </script>


  <script type="text/javascript">
    $(function(){
    $("#loadingImg").hide();
    $("#loadingImg2").hide();

    $.ajaxSetup({
    type:"POST",
    url: "<?php echo base_url('Master/Petani/ambil_data') ?>",
    cache: false,
    });


    // custom untuk input

     $("#kabupaten").change(function(){
    var value=$(this).val();
    console.log(value);
    if(value>0){
    $.ajax({
    beforeSend: function (){
    $("#loadingImg").fadeIn();
        },
    data:{modul:'kabupaten',kode:value},
    success: function(respond){
    $("#nama_kab").val(respond);
    $("#loadingImg").fadeOut();  
    console.log(respond);
    }
    })
    }
    });

    // custom untuk input

    $("#kabupaten").change(function(){
    var value=$(this).val();
    console.log(value);
    if(value>0){
    $.ajax({
    beforeSend: function (){
    $("#loadingImg").fadeIn();
        },
    data:{modul:'kecamatan',kode:value},
    success: function(respond){
    $("#kecamatan").html(respond);
    $("#loadingImg").fadeOut();  
    console.log(respond);
    }
    })
    }
    });
    
    
    $("#kecamatan").change(function(){
    var value=$(this).val();
    console.log(value);
    $.ajax({
    beforeSend: function (){
    $("#loadingImg2").fadeIn();
        },
    data:{modul:'desa',kecamatan:value},
    success: function(respond){
    $("#desa").html(respond);
    $("#loadingImg2").fadeOut();
    console.log(respond);
    }
    })
    
    });


    
    
    })
  </script>

        
        

        
        