<?php $this->load->view('_heading/_headerContent') ?>

<style>
#osas {
color:red;
font-weight:bold;
margin-left:0px;
}

  .field-icon {
  float: left;
  margin-left: 93%;
  margin-top: -25px;
  position: relative;
  z-index: 2;
 }


</style>
		
<section class="content">
    
    
    <!-- style loading -->
    <div class="loading2"></div>
    <!-- -->
    
    <div class="box">
        <div class="row">
        <div class="col-md-8">
        <!-- form start -->
        <form class="form-horizontal" id="form-tambah" method="POST">
        <input type="hidden" name="created_by" value="<?php echo $userdata->nama; ?>">
        <input type="hidden" name="status" value="Non aktif">
      
        <div class="box-body">
               
       <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Nama</label>
        <div class="col-sm-7">
        <input type="text" class="form-control" name="_nama_lengkap" placeholder="Nama Lengkap / Nama User OPD" id="nama_lengkap" aria-describedby="sizing-addon2">
        </div>
        </div>

        <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">NIK</label>
        <div class="col-sm-7">
        <input type="text" class="form-control" name="_nik" placeholder="NIK" id="nik" aria-describedby="sizing-addon2">
        </div>
        </div>

        <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Jabatan</label>
        <div class="col-sm-7">
        <input type="text" class="form-control" name="_jabatan" placeholder="Jabatan User" id="jabatan" aria-describedby="sizing-addon2">
        </div>
        </div>
    
       <div class="form-group">
       <label class="col-sm-2 control-label">Dinas / Badan</label>
       <div class="col-sm-7">
       <select name="dinas" class="form-control selek-dinas" id="dinas">
                <option></option>
                <?php
                foreach ($dinas as $data) {
                ?>
                <option value="<?php echo $data->nama_kategori; ?>">
                <?php echo $data->nama_kategori; ?>
                </option>
                <?php
                }
                ?>
                </select>
       </div>
       </div>


    

         <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
        <div class="col-sm-7">
        <input type="text" class="form-control" placeholder="email" name="_email" id="email" aria-describedby="sizing-addon2">
        </div>
        </div>  

        <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Password</label>
        <div class="col-sm-7">
        <input type="password" class="form-control" placeholder="password" id="password-field" name="_password" aria-describedby="sizing-addon2" ><span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
        </div>
        </div>      

        </div>
        <div class="box-footer">
        <button name="submit" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Simpan</button>
        <a class="klik ajaxify" href="<?php echo site_url('master-opd'); ?>"><button class="btn btn-primary btn-flat" ><i class="fa fa-arrow-left"></i> Kembali</button></a>
        </div>
        </form>
      
        </div>
        <!-- /.box -->
       
        </div>
          <!-- /.row -->
    </div>

    </section>


    <script type="text/javascript">
    //Proses Controller logic ajax

    function cekemail(a) {
    re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(a);
    }

    function cekpassword(a) {
    re2 = /^\S{3,}$/;
    return re2.test(a);
    }



    $('#form-tambah').submit(function(e) {
      
        var error = 0;
        var message = "";

        var data = $(this).serialize();

        var nama_lengkap = $("#nama_lengkap").val();
        var nama_lengkap = nama_lengkap.trim();

          if (error == 0) {
            if (nama_lengkap.length == 0) {
                error++;
                message = "Nama Lengkap wajib di isi.";
            }
        }

        var nik = $("#nik").val();
        var nik = nik.trim();

         if (error == 0) {
            if (nik.length == 0) {
                error++;
                message = "No NIK wajib di isi.";
            }
        }

        var jabatan= $("#jabatan").val();
        var jabatan = jabatan.trim();

         if (error == 0) {
            if (jabatan.length == 0) {
                error++;
                message = "Jabatan wajib di isi.";
            }
        }

        var dinas= $("#dinas").val();
        var dinas = dinas.trim();

         if (error == 0) {
            if (dinas.length == 0) {
                error++;
                message = "Dinas / Badan wajib di isi.";
            }
        }

        var email = $("#email").val();
        var email = email.trim();

         if (error == 0) {
            if (email.length == 0) {
                error++;
                message = "Email wajib di isi.";
            }

              else if (!cekemail(email)) {
                error++;
                message = " Format Email tidak sesuai (admin@gmail.com). ";  
            }
        }

        var password = $("#password-field").val();
        var password = password.trim();

         if (error == 0) {
            if (password.length == 0) {
                error++;
                message = "Password wajib di isi.";
            }

             else if (!cekpassword(password)) {
                error++;
                message = "Password tidak boleh ada spasi !! ";  
            }
        }

      
    if (error == 0) {
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                $(".loading2").show();
                $(".loading2").modal('show');
                },
                url: '<?php echo base_url('Master/Master_proposal/prosesAdd'); ?>',
                type:"post",
                data:new FormData(this),
                processData:false,
                contentType:false,
                cache:false,
            }).done(function (data) {

                var result = jQuery.parseJSON(data);
                if (result.status == 'berhasil') {
                
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    save_berhasil();
                    setTimeout(location.reload.bind(location), 500);

                 } else if (result.status == 'peringatan') {
                   $(".loading2").hide();
                    $(".loading2").modal('hide');
                    swal("Peringatan", "Email sudah digunakan User lain !!", "warning");

                }  else {
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    gagal();
                    
                }

                  console.log(result);
            })
            e.preventDefault();
        } else {
            swal("Peringatan", message, "warning");
            return false;

        }
    });


    // untuk show hide password
    $(".toggle-password").click(function() {

    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $($(this).attr("toggle"));
    if (input.attr("type") == "password") {
    input.attr("type", "text");
    } else {
    input.attr("type", "password");
    }
    });


     // untuk select2 ajak pilih department
    $(function () 
    {
    $(".selek-dinas").select2({
        placeholder: " -- Pilih Dinas / Badan OPD -- "
        });
    });

  </script>

		
		

		
		