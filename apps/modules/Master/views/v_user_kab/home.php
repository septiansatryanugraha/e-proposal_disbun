<?php $this->load->view('_heading/_headerContent') ?>

<section class="content">
  <div class="box">

    <div class="box-header">
      <div class="col-md-2">
        <?php if ($accessAdd > 0) { ?>
          <a class="klik ajaxify" href="<?php echo site_url('add-user-kab'); ?>"><button class="btn btn-success" ><i class="glyphicon glyphicon-plus-sign"></i> Add Data</button></a>
        <?php } ?>
      </div>

      <div class="col-md-4" style="margin-left:-60px; margin-bottom:20px;">
        <div class="btn-group">
          <button id="search-button" name="search-button" type="button" class="btn grey"><i class="fa fa-search"></i> Advanced Search</button>
        </div>
      </div>

      <br><br>
      <div class="search-form" style="display: none;margin-left: 20px"><br>

        <div class="col-md-3"><br>
          <div class="form-group">
            <label>Tanggal Awal:</label>
            <div class="input-group date">
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
              <input type="text" name="tanggal_awal" id="tanggal_awal" class="form-control datepicker">
            </div>
          </div>
        </div>
        <div class="col-md-3"><br>
          <div class="form-group">
            <label>Tanggal Akhir:</label>
            <div class="input-group date">
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
              <input type="text" name="tanggal_akhir" id="tanggal_akhir" class="form-control datepicker" >
            </div>
          </div>
        </div>

        <div class="box-footer">
          <button name="button_filter" id="button_filter" style="margin-top: 30px" type="button" class="btn btn-success btn-flat"><i class="fa fa-refresh"></i> Filter</button>
        </div>
        <div class="box-footer"><br></div>
      </div>
    </div>
    <!-- /.box-header -->

    <div class="box-body">
     <div class="table-responsive">
       <table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
         <thead>
           <tr>
             <th>#</th>
             <th>Email</th>
             <th>Nama</th>
             <th>Alamat</th>
             <th>Status</th>
             <th style="width:125px;">Action</th>
           </tr>
         </thead>
         <tbody>
         </tbody>
       </table>
     </div>
   </div>
 </div>

</section>


<script type="text/javascript">
    // untuk datepicker
    $(function () {
     $(".datepicker").datepicker({
       orientation: "left",
       autoclose: !0,
       format: 'dd-mm-yyyy'
     })
   });

    //untuk load data table ajax  
    var save_method; //for save method string
    var table;

    $(document).ready(function () {
      reloadTable();
    });

    function reloadTable() {
      var tanggal_awal = $("#tanggal_awal").val();
      var tanggal_akhir = $("#tanggal_akhir").val();

      table = $('#table').DataTable({
            "processing": true, //Feature control the processing indicator.
            "order": [], //Initial no order.
            oLanguage: {
              "sProcessing": "<img src='<?php base_url(); ?>assets/tambahan/gambar/loading.gif' width='25px'>",
              "sLengthMenu": "_MENU_ &nbsp;&nbsp;Data Per Halaman",
              "sInfo": "Menampilkan _START_ s/d _END_ dari <b>_TOTAL_ data</b>",
              "sInfoFiltered": "(difilter dari _MAX_ total data)",
              "sEmptyTable": "Data tidak ada di server",
              "sInfoPostFix": "",
              "sSearch": "<i class='fa fa-search fa-fw'></i> Pencarian : ",
              "sPaginationType": "simple_numbers",
              "sUrl": "",
              "oPaginate": {
                "sFirst": "Pertama",
                "sPrevious": "Sebelumnya",
                "sNext": "Selanjutnya",
                "sLast": "Terakhir"
              }
            },
            // Load data for the table's content from an Ajax source
            "ajax": {
              "url": "<?php echo site_url('Master/User_Kab/ajax_list') ?>",
              "type": "POST",
              data: {tanggal_awal: tanggal_awal, tanggal_akhir: tanggal_akhir},
            },
            //Set column definition initialisation properties.
            "columnDefs": [{
                    "targets": [-1], //last column
                    "orderable": false, //set not orderable
                  },
                  ],
                });
    }

    $('#search-button').click(function () {
      $('.search-form').toggle();
      return false;
    });

    $("#button_filter").click(function () {
      table.destroy();
      reloadTable();
    });


    function reload_table()
    {
    table.ajax.reload(null,false); //reload datatable ajax 
  }

  $(document).on("click",".hapus-master-opd",function(){
   var nama=$(this).attr("data-id");

   swal({
    title:"Hapus Data?",
    text:"Yakin anda akan menghapus data ?",
    type: "warning",
    showCancelButton: true,
    confirmButtonText: "Hapus",
    confirmButtonColor: '#dc1227',
    customClass: ".sweet-alert button",
    closeOnConfirm: true,
    
    html: true
  },
  function(){
   $.ajax({
     method: "POST",
     url: "<?php echo base_url('Master/User_Kab/hapus'); ?>",
     data: "nama=" +nama,
     success: function(data){
      var result = jQuery.parseJSON(data);
      if (result.status == true) {
        $("tr[data-id='"+nama+"']").fadeOut("fast",function(){
          $(this).remove();
        });
        swal("Success", result.pesan, "success");
      } else {
        swal("Warning", result.pesan, "warning");      
      }
        // hapus_berhasil();
        reload_table();
      }
    });
 });
 });

</script>






