 <?php $this->load->view('_heading/_headerContent') ?>
 
 <style>
  .field-icon {
  float: left;
  margin-left: 93%;
  margin-top: -25px;
  position: relative;
  z-index: 2;
}

  .font-loading
{
  font-family: Futura,Trebuchet MS,Arial,sans-serif; 
  color:red;
  font-size: 14px;
}


 </style>


 <?php
  $password=$brand->password;  
  $dec_pass = base64_decode($password);   
 ?>
 
 
 <section class="content">

  <!-- style loading -->
    <div class="loading2"></div>
    <!-- -->
  
    <div class="box">
    <form class="form-horizontal" id="form-update" method="POST">
    <input type="hidden"  name="id_user" value="<?php echo $brand->id_user; ?>">
    <input type="hidden"  name="nama" value="<?php echo $brand->nama; ?>">
  
        <div class="row">
        <div class="col-md-10">
      
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body">
    
        <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Nama</label>
        <div class="col-sm-6">
        <input type="text" class="form-control" name="nama_lengkap" placeholder="Nama Lengkap / Nama User OPD" id="nama_lengkap" value="<?php echo $brand->nama_lengkap; ?>">
        </div>
        </div>

        <div class="form-group">
         <label class="col-sm-2 control-label">Alamat</label>
         <div class="col-sm-7">
         <textarea rows="4" cols="54" id="alamat" placeholder="alamat kabupaten" name="alamat"><?php echo $brand->alamat; ?></textarea>
         </div>
         </div>


        <div class="form-group">
        <label class="col-sm-2 control-label">Kabupaten</label>
        <div class="col-sm-3">
        <select name="kabupaten" class="form-control selek-dinas" id="kabupaten">
        <?php foreach ($kabupaten as $data) { ?>
        <option value="<?php echo $data->kabupaten; ?>"<?php if($data->kabupaten == $brand->kabupaten){echo "selected='selected'";} ?>><?php echo $data->kabupaten; ?>
        </option>
        <?php } ?>
        </select>
        </div>
        </div>

         <div id="loadingImg">
        <img src="<?php echo base_url().'assets/' ?>tambahan/gambar/loading-bubble.gif">
        <p class="font-loading">Please wait still request data...</font></p>
        </div>

        <input type="hidden" class="form-control"  name="kode_kabupaten" id="kode" aria-describedby="sizing-addon2" value="<?php echo $brand->kode; ?>">


        <div class="form-group">
        <label class="col-sm-2 control-label">Status</label>
        <div class="col-sm-3">
        <select name="status" class="form-control selek-status" aria-describedby="sizing-addon2">
        <?php foreach ($datastatus as $status) { ?>
        <option value="<?php echo $status->nama; ?>"<?php if($status->nama == $brand->status){echo "selected='selected'";} ?>><?php echo $status->nama; ?>
        </option>
        <?php } ?>
        </select>
        </div>
        </div>

   
        <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
        <div class="col-sm-6">
        <input type="text" class="form-control" placeholder="email" name="email" id="email" aria-describedby="sizing-addon2" value="<?php echo $brand->email; ?>" disabled>
        </div>
        </div>

         <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Password</label>
        <div class="col-sm-6">
        <input type="password" class="form-control" placeholder="password" id="password-field" name="password" aria-describedby="sizing-addon2" value="<?php echo $dec_pass; ?>"><span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
        </div>
        </div>      
       
     </div>
        <!-- /.box-body -->
    
    <div class="box-footer">
        <button name="simpan" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Update</button>
        <a class="klik ajaxify" href="<?php echo site_url('master-user-kab'); ?>"><button class="btn btn-primary btn-flat" ><i class="fa fa-arrow-left"></i> Kembali</button></a>
        </div>
        </form>
        </div>
        <!-- /.box -->
        </div>
        <!-- /.row -->
    </div>
  </section>  
    
    
     <script type="text/javascript">
    //Proses Controller logic ajax

    function cekemail(a) {
    re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(a);
    }

    function cekpassword(a) {
    re2 = /^\S{3,}$/;
    return re2.test(a);
    }


    $('#form-update').submit(function(e) {
      
        var error = 0;
        var message = "";

        var data = $(this).serialize();

        var nama_lengkap = $("#nama_lengkap").val();
        var nama_lengkap = nama_lengkap.trim();

          if (error == 0) {
            if (nama_lengkap.length == 0) {
                error++;
                message = "Nama Lengkap wajib di isi.";
            }
        }


        var kabupaten = $("#kabupaten").val();
        var kabupaten = kabupaten.trim();

          if (error == 0) {
            if (kabupaten.length == 0) {
                error++;
                message = "Kabupaten wajib di isi.";
            }
        }



        var password = $("#password-field").val();
        var password = password.trim();

         if (error == 0) {
            if (password.length == 0) {
                error++;
                message = "Password wajib di isi.";
            }

             else if (!cekpassword(password)) {
                error++;
                message = "Password tidak boleh ada spasi !! ";  
            }
        }

      
    if (error == 0) {

            $.ajax({
                method: 'POST',
                beforeSend: function () {
                $(".loading2").show();
                $(".loading2").modal('show');
                },
                url: '<?php echo base_url('Master/User_Kab/prosesUpdate'); ?>',
                type:"post",
                data:new FormData(this),
                processData:false,
                contentType:false,
                cache:false,
            }).done(function (data) {

                var result = jQuery.parseJSON(data);
                if (result.status == true) {
                
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    swal("Success", result.pesan, "success");
                    setTimeout(location.reload.bind(location), 500);

                 }  else {
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    swal("Warning", result.pesan, "warning");
                    
                }

                  console.log(result);
            })
            e.preventDefault();
        } else {
            toastr.error(message,'Warning', {timeOut: 5000},toastr.options = {
             "closeButton": true}); 
            return false;

        }
    });


    // untuk show hide password
    $(".toggle-password").click(function() {

    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $($(this).attr("toggle"));
    if (input.attr("type") == "password") {
    input.attr("type", "text");
    } else {
    input.attr("type", "password");
    }
    });


       // untuk select2 ajak pilih department
    
    $(function () 
    {
    $(".selek-status").select2({
        placeholder: " -- Pilih Status -- "
        });
    });

    // untuk select2 ajak pilih department
    $(function () 
    {
    $(".selek-dinas").select2({
        placeholder: " -- Pilih Kabupaten -- "
        });
    });


  </script>

  <script type="text/javascript">

    $(function(){
    $("#loadingImg").hide();
    $("#tampil").hide();

    $.ajaxSetup({
    type:"POST",
    url: "<?php echo base_url('Master/User_kab/ambil_data') ?>",
    cache: true,
    });

    $("#kabupaten").change(function(){
    var value=$(this).val();
    console.log(value);
    $.ajax({
    beforeSend: function (){
    $("#loadingImg").fadeIn();
    },
    data:{modul:'kode_kab',kabupaten:value},
    success: function(respond){
    $("#kode").val(respond);
    $("#loadingImg").fadeOut(); 
    console.log(respond);
    }
    })
   
    });
    
    })
    

  </script> 
