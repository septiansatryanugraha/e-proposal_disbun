<?php $this->load->view('_heading/_headerContent') ?>

<style>
#osas {
color:red;
font-weight:bold;
margin-left:0px;
}

  .field-icon {
  float: left;
  margin-left: 93%;
  margin-top: -25px;
  position: relative;
  z-index: 2;
 }

  .font-loading
{
  font-family: Futura,Trebuchet MS,Arial,sans-serif; 
  color:red;
  font-size: 14px;
}


</style>
		
<section class="content">
    
    
    <!-- style loading -->
    <div class="loading2"></div>
    <!-- -->
    
    <div class="box">
        <div class="row">
        <div class="col-md-8">
        <!-- form start -->
        <form class="form-horizontal" id="form-tambah" method="POST">
        <input type="hidden" name="created_by" value="<?php echo $userdata->nama; ?>">
      
        <div class="box-body">
               
       <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Nama Kabupaten</label>
        <div class="col-sm-7">
        <input type="text" class="form-control" name="nama_lengkap" placeholder="Nama Kabupaten" id="nama_kab" aria-describedby="sizing-addon2">
        </div>
        </div>

        <div class="form-group">
         <label class="col-sm-2 control-label">Alamat</label>
         <div class="col-sm-7">
         <textarea rows="4" cols="54" id="alamat" placeholder="alamat kabupaten" name="alamat"></textarea>
         </div>
         </div>

    
     <div class="form-group">
     <label class="col-sm-2 control-label">Kabupaten</label>
     <div class="col-sm-4">
     <select name="kabupaten" class="form-control selek-dinas" id="kabupaten">
     <option></option>
     <?php foreach ($kabupaten as $data) { ?>
     <option value="<?php echo $data->kabupaten; ?>">
     <?php echo $data->kabupaten; ?>
     </option>
     <?php } ?>
     </select>
     </div>
     </div>

      <div id="loadingImg">
        <img src="<?php echo base_url().'assets/' ?>tambahan/gambar/loading-bubble.gif">
        <p class="font-loading">Please wait still request data...</font></p>
        </div> 

        <input type="hidden" class="form-control"  name="kode_kabupaten" id="kode" aria-describedby="sizing-addon2">
    

         <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
        <div class="col-sm-7">
        <input type="text" class="form-control" placeholder="email" name="email" id="email" aria-describedby="sizing-addon2">
        </div>
        </div>  

        <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Password</label>
        <div class="col-sm-7">
        <input type="password" class="form-control" placeholder="password" id="password-field" name="password" aria-describedby="sizing-addon2" ><span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
        </div>
        </div>      

        </div>
        <div class="box-footer">
        <button name="submit" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Simpan</button>
        <a class="klik ajaxify" href="<?php echo site_url('master-user-kab'); ?>"><button class="btn btn-primary btn-flat" ><i class="fa fa-arrow-left"></i> Kembali</button></a>
        </div>
        </form>
      
        </div>
        <!-- /.box -->
       
        </div>
          <!-- /.row -->
    </div>

    </section>


    <script type="text/javascript">
    //Proses Controller logic ajax

    function cekemail(a) {
    re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(a);
    }

    function cekpassword(a) {
    re2 = /^\S{3,}$/;
    return re2.test(a);
    }



    $('#form-tambah').submit(function(e) {
      
        var error = 0;
        var message = "";

        var data = $(this).serialize();

        var nama_kab = $("#nama_kab").val();
        var nama_kab = nama_kab.trim();

          if (error == 0) {
            if (nama_kab.length == 0) {
                error++;
                message = "Nama Kabupaten wajib di isi.";
            }
        }

        var alamat = $("#alamat").val();
        var alamat = alamat.trim();

          if (error == 0) {
            if (alamat.length == 0) {
                error++;
                message = "Alamat Kabupaten wajib di isi.";
            }
        }

        var kabupaten = $("#kabupaten").val();
        var kabupaten = kabupaten.trim();

          if (error == 0) {
            if (kabupaten.length == 0) {
                error++;
                message = "Kabupaten wajib di isi.";
            }
        }


        var email = $("#email").val();
        var email = email.trim();

         if (error == 0) {
            if (email.length == 0) {
                error++;
                message = "Email wajib di isi.";
            }

              else if (!cekemail(email)) {
                error++;
                message = " Format Email tidak sesuai (admin@gmail.com). ";  
            }
        }

        var password = $("#password-field").val();
        var password = password.trim();

         if (error == 0) {
            if (password.length == 0) {
                error++;
                message = "Password wajib di isi.";
            }

             else if (!cekpassword(password)) {
                error++;
                message = "Password tidak boleh ada spasi !! ";  
            }
        }

      
    if (error == 0) {
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                $(".loading2").show();
                $(".loading2").modal('show');
                },
                url: '<?php echo base_url('Master/User_Kab/prosesAdd'); ?>',
                type:"post",
                data:new FormData(this),
                processData:false,
                contentType:false,
                cache:false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == true) {
                    document.getElementById("form-tambah").reset();
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    setTimeout(location.reload.bind(location), 500);
                    swal("Success", result.pesan, "success");
                } else {
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    swal("Warning", result.pesan, "warning");
                }
            })
            e.preventDefault();
        } else {
            toastr.error(message,'Warning', {timeOut: 5000},toastr.options = {
             "closeButton": true}); 
            return false;

        }
    });


    // untuk show hide password
    $(".toggle-password").click(function() {

    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $($(this).attr("toggle"));
    if (input.attr("type") == "password") {
    input.attr("type", "text");
    } else {
    input.attr("type", "password");
    }
    });


     // untuk select2 ajak pilih department
    $(function () 
    {
    $(".selek-dinas").select2({
        placeholder: " -- Pilih Kabupaten -- "
        });
    });

  </script>


<script type="text/javascript">

    $(function(){
    $("#loadingImg").hide();
    $("#tampil").hide();

    $.ajaxSetup({
    type:"POST",
    url: "<?php echo base_url('Master/User_kab/ambil_data') ?>",
    cache: true,
    });

    $("#kabupaten").change(function(){
    var value=$(this).val();
    console.log(value);
    $.ajax({
    beforeSend: function (){
    $("#loadingImg").fadeIn();
    },
    data:{modul:'kode_kab',kabupaten:value},
    success: function(respond){
    $("#kode").val(respond);
    $("#loadingImg").fadeOut(); 
    console.log(respond);
    }
    })
   
    });
    
    })
    

  </script> 
		
		

		
		