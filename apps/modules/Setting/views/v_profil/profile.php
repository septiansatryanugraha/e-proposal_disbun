<?php $this->load->view('_heading/_headerContent') ?>

<?php 

$foto_user=$this->session->userdata('foto');
$nama_user=$this->session->userdata('nama');
?>

<!-- style loading -->
		<div class="loading2"></div>
		<!-- -->
<section class="content">
<div class="row">
  <div class="col-md-3">
    <!-- Profile Image -->
    <div class="box box-primary">
      <div class="box-body box-profile">
        <img class="profile-user-img img-responsive img-circle" src="<?php echo base_url(); ?>upload/user/<?php echo $this->session->userdata('foto') ?>" alt="User profile picture">
        <h3 class="profile-username text-center"><?php echo $this->session->userdata('nama') ?></h3>
		 <?php foreach ($datagrup as $data) { ?>
		 <?php if($data->grup_id ==  $userdata->grup_id){ ?>
		 <p class="text-muted text-center"><?php echo $data->nama_grup; ?></p>
         <?php 
	     }} ?>
	    
      </div>
    </div>
  </div>

    <div class="col-md-9">
    <div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
    <li class="active"><a href="#settings" data-toggle="tab">Settings</a></li>
    <li><a href="#password" data-toggle="tab">Changes Password</a></li>
    </ul>
      
	 <div class="tab-content">
     <div class="active tab-pane" id="settings">
		
     <form class="form-horizontal" id="form-update" method="POST">
      <input type="hidden" class="form-control" placeholder="Name" name="id" value="<?php echo $this->session->userdata('id') ?>">
	 <input type="hidden" name="last_update_by" value="<?php echo $this->session->userdata('nama') ?>">
     <div class="form-group">
     <label for="inputUsername" class="col-sm-2 control-label">Username</label>
     <div class="col-sm-6">
     <input type="text" class="form-control" id= placeholder="Username" name="username" value="<?php echo $this->session->userdata('username') ?>">
     </div>
     </div>
            
	 <div class="form-group">
     <label for="inputNama" class="col-sm-2 control-label">Name</label>
     <div class="col-sm-6">
     <input type="text" class="form-control" placeholder="Name" name="nama" value="<?php echo $this->session->userdata('nama') ?>">
     </div>
     </div>


			
	  <div class="form-group">
      <label for="inputNama" class="col-sm-2 control-label">Level</label>
      <div class="col-sm-6">
			  
      <?php
	  foreach ($datagrup as $data) {
	  $level=$this->session->userdata('grup_id')?>
	  <?php if($data->grup_id ==  $level){ ?>
      <input type="text" class="form-control" value="<?php echo $data->nama_grup; ?>" readonly>
      <?php
	   }} ?>  
	   </div>
       </div>
			  
			  <div class="form-group">
              <label for="inputNama" class="col-sm-2 control-label">Status</label>
              <div class="col-sm-6">
			  
			  <?php
		      foreach ($dataStatus as $data) {
		       $status=$this->session->userdata('status')?>
		      <?php if($data->id_status == $status){ ?>
              <p class="form-control-static"><?php echo $data->nama; ?></p>
              <?php
			  }} ?>
			  </div>
              </div>
		
            <div class="form-group">
              <label for="inputFoto" class="col-sm-2 control-label">Photo</label>
              <div class="col-sm-5">
                <input type="file" class="form-control" placeholder="Foto" name="foto">
              </div>
            </div>
            
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-danger">Submit</button>
              </div>
            </div>
          </form>
        </div>
		
		<!---- ini buat update password -->
		
        <div class="tab-pane" id="password">
           <form class="form-horizontal" id="form-update-pass" method="POST">
		   <input type="hidden" name="last_update_by" value="<?php echo $userdata->nama; ?>">
            <div class="form-group">
              <label for="passLama" class="col-sm-2 control-label">Old Password </label>
              <div class="col-sm-6">
                <input type="password" class="form-control" placeholder="Old Password" name="passLama">
              </div>
            </div>
            <div class="form-group">
              <label for="passBaru" class="col-sm-2 control-label">New Password</label>
              <div class="col-sm-6">
                <input type="password" class="form-control" placeholder="New Password" name="passBaru">
              </div>
            </div>
            <div class="form-group">
              <label for="passKonf" class="col-sm-2 control-label">Confirmation Password</label>
              <div class="col-sm-6">
                <input type="password" class="form-control" placeholder="Confirmation Password" name="passKonf">
              </div>
            </div>
            
            <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-danger">Submit</button>
            </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

	</section>

    <script type="text/javascript">
	
	//Proses update ajax
		
		$(document).ready(function(){
		$('#form-update').submit(function(e){
		 e.preventDefault(); 
		 $.ajax({
		 url:'<?php echo base_url();?>Setting/Profile/update',
		 type:"post",
		 data:new FormData(this),
		 contentType: false,
		 cache: false,
		 processData:false,
		             
		 beforeSend: function (){
		 $(".loading2").show();
		 $(".loading2").modal('show');	
		 },
		 success: function(data){
		 $(".loading2").hide();
		 $(".loading2").modal('hide');
		 setTimeout(location.reload.bind(location), 500);		 
		 swal("Success",  'Data has been updated', "success");
		 }
		 });
		 });
	});
	
	
	//Proses update password ajax
		
		$(document).ready(function(){
		$('#form-update-pass').submit(function(e){
		 e.preventDefault(); 
		 $.ajax({
		 url:'<?php echo base_url();?>Setting/Profile/update',
		 type:"post",
		 data:new FormData(this),
		 contentType: false,
		 cache: false,
		 processData:false,
		             
		 beforeSend: function (){
		 $(".loading2").show();
		 $(".loading2").modal('show');	
		 },
		 success: function(data){
		 $(".loading2").hide();
		 $(".loading2").modal('hide');	
		 update_berhasil();
		 }
		 });
		 });
	});
	
	
	

</script>