 <style>
  .field-icon {
  float: left;
  margin-left: 93%;
  margin-top: -25px;
  position: relative;
  z-index: 2;
}

 </style>


<?php $this->load->view('_heading/_headerContent') ?>
	<section class="content">
 
		<!-- style loading -->
		<div class="loading2"></div>
		<!-- -->
		
		<div class="box">
		<form class="form-horizontal" id="form-update" method="POST">
        <input type="hidden" name="id" value="<?php echo $dataButton->id; ?>">
		<input type="hidden" name="last_update_by" value="<?php echo $userdata->nama; ?>">
        <div class="row">
        <div class="col-md-7">

        <div class="box-header with-border">
        <h3 class="box-title">Edit Status</h3>
        </div>
			
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body">

        <div class="form-group">
     	<label for="inputEmail3" class="col-sm-2 control-label">Keterangan</label>
     	<div class="col-sm-9">
     	<input type="text" class="form-control" name="keterangan" placeholder="Keterangan" value="<?php echo $dataButton->keterangan; ?>">
     	</div>
     	</div>
		
		<div class="form-group">
        <label for="inputPassword3" class="col-sm-2 control-label">Status</label>
        <div class="col-sm-3">
        <select name="status" class="form-control select2"  aria-describedby="sizing-addon2">
		<?php
		foreach ($dataStatus as $status) {
		?>
		<option value="<?php echo $status->nama; ?>" <?php if($status->nama == $dataButton->status){echo "selected='selected'";} ?>><?php echo $status->nama; ?></option>
		<?php
		}
		?>
		</select>
        </div>
        </div>
		
        </div>
        <!-- /.box-body -->
		
		<div class="box-footer">
        <button name="simpan" type="submit" class="btn btn-sm btn-primary"><i class="fa fa-save"></i> Update</button>
        <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-retweet"></i> Cancel</button>
        </div>
        </form>
        </div>
        <!-- /.box -->
        </div>
        <!-- /.row -->
		</div>
		</section>
		
		
		
		<script type="text/javascript">	
	
		//Proses Controller logic ajax
		
		$('#form-update').submit(function(e) {
		
		var data = $(this).serialize();
		
		$.ajax({
        beforeSend: function (){
        $(".loading2").show();
		$(".loading2").modal('show');	
        },
		url:'<?php echo base_url();?>Setting/Button/prosesUpdate',
		type:"post",
		data:new FormData(this),
		processData:false,
		contentType:false,
		cache:false,	
		})
		.done(function(data) {
			var result = jQuery.parseJSON(data);
                if (result.status == true) {
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    swal("Success", result.pesan, "success");

                 }  else {
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    swal("Warning", result.pesan, "warning");
                    
                }
		})
		e.preventDefault();
	});
	
	
	
	// untuk select2 original
		$(function () 
		{
		$(".select2").select2({
        });
		});	

		
</script>	
	