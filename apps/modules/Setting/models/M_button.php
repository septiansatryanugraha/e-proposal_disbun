<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_button extends CI_Model 
{
	public function get_datatables() {
		$data = $this->db->get('tbl_action');
		return $data->result();
	}

	public function select_by_id($id) {
		$sql = "SELECT * FROM tbl_action WHERE id = '{$id}'";

		$data = $this->db->query($sql);

		return $data->row();
	}
	
	public function select_status() {
		
		$sql = " select * from status_grup WHERE nama in ('Enabled','Disabled')";
		$data = $this->db->query($sql);
		return $data->result();
	}
	
	
	
	function simpan_data($data){
		$result= $this->db->insert('tbl_action',$data);
	    return $result;
	}
	

	public function update($data,$where) {
		$result= $this->db->update('tbl_action',$data,$where);
	    return $result;
	}

	public function hapus($id) {
		$sql = "DELETE FROM tbl_action WHERE id='" .$id ."'";

		$this->db->query($sql);

		return $this->db->affected_rows();
	}

}

