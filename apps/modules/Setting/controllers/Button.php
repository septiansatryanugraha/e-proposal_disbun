<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Button extends AUTH_Controller {

	public function __construct()
    {
		parent::__construct();
		$this->load->model('M_button');
		$this->load->model('M_sidebar');
	}
	
	public function loadkonten($page, $data) {
		
		$data['userdata'] 	= $this->userdata;
		$ajax = ($this->input->post('status_link') == "ajax" ? true : false);
		if (!$ajax) { 
			$this->load->view('Dashboard/layouts/header', $data);
		}
		$this->load->view($page, $data);
		if (!$ajax) $this->load->view('Dashboard/layouts/footer', $data);
	}

	public function index()
	{
        $data['userdata'] 	= $this->userdata; 	
		$data['page'] 		= "Setting Button";
		$data['judul'] 		= "Setting Button";
		$this->loadkonten('v_button/v_home',$data);
	}
	
	public function ajax_list()
	{

		$accessEdit = $this->M_sidebar->access('edit','button');
        $accessDel = $this->M_sidebar->access('del','button');
		$list = $this->M_button->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $brand) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $brand->keterangan;
			$row[] = $brand->status;
		
			$buttonEdit = '';
            if ($accessEdit->menuview > 0) {
                $buttonEdit = anchor('edit-button/' . $brand->id, ' <span tooltip="Edit Data"><span class="fa fa-edit" ></span>', ' class="btn btn-sm btn-primary klik ajaxify" ');
            }
            $buttonDel = '';
            if ($accessDel->menuview > 0) {
                $buttonDel = '<button class="btn btn-sm btn-danger hapus-master-opd" data-id=' . "'" . $brand->id . "'" . '><span tooltip="Hapus Data"><i class="glyphicon glyphicon-trash"></i></button>';
            }

			$row[] = $buttonEdit . '  ' . $buttonDel;
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
		
		
	}
	
	// public function add() {
		
	//     /*ini harus ada boss */
	// 	$data['userdata'] = $this->userdata;
	// 	$access = $this->M_sidebar->access('add','user');
	// 	if ($access->menuview == 0){
	// 		$data['userdata'] = $this->userdata;
	// 		$data['page'] 	= "user";
	// 		$data['judul'] 	= "add user";
	// 		$this->loadkonten('layouts/no_akses',$data);
	// 	 }
	// 	 /*ini harus ada boss */
	// 	 else
	// 	 {
		
	// 	 $data['datagrup']   = $this->M_user->select_group();	
	// 	 $data['page'] 	= "user";
	// 	 $data['judul'] 	= "user";
	// 	 $this->loadkonten('v_user/v_tambah-user',$data);
	// 	 }
	// }

	// public function prosesTambah() {
		
		
	// 	if (isset($_POST["username"]) && !empty($_POST["password"])) 
	// 	{
	// 	$data2 = array( 
	// 				'nama'   	           => $this->input->post('nama'), 
	// 				'email'            	   => $this->input->post('email'),
	// 				'username'             => $this->input->post('username'),
	// 				'password'             => md5($this->input->post('password')),
	// 				'grup_id'              => $this->input->post('grup_id'),
	// 				'foto'                 => 'no-image.jpg',	
	// 				'hidden'               => '1',
	// 				'status'               => $this->input->post('status'),
	// 				'created_by'           => $this->input->post('created_by'),					
	// 				'last_created_date'    => date('Y-m-d H:i:s')						 
	// 				);  

	//     $result = $this->M_user->simpan_data($data2);
	// 	if ($result > 0) {
	// 	$out['status'] = 'berhasil';
	// 	} else {
	// 	$out['status'] = 'gagal';
	// 	}
	// 	} else {
	// 	$out['status'] = 'gagal';
	// 	}

	// 	echo json_encode($out);
	// }
	
	public function Edit($id) {
		
		/*ini harus ada boss */
		$data['userdata'] = $this->userdata;
		$access = $this->M_sidebar->access('edit','button');
		if ($access->menuview == 0){
			$data['userdata'] = $this->userdata;
			$data['page'] 		= "Setting Button";
			$data['judul'] 		= "Setting Button";
			$this->loadkonten('layouts/no_akses',$data);
		 }
		 /*ini harus ada boss */
		 else{
		$where=array('id' => $id);
		$data['dataStatus']   = $this->M_button->select_status();
		$data['dataButton']   = $this->M_button->select_by_id($id);
		
		$data['page'] 		= "Setting Button";
		$data['judul'] 		= "Setting Button";
	    $this->loadkonten('v_button/v_ed_button',$data);
		 }
	}
	
	
	
	public function prosesUpdate() {
		
		$where = array(
		'id'   => $this->input->post('id')
		);
	    $data2 = array( 
	    'keterangan'   	       => $this->input->post('keterangan'), 
		'status'   	           => $this->input->post('status'), 				 
		); 
		
		$result = $this->M_button->update($data2,$where);
			
			
		if ($result > 0) {
		$this->db->trans_commit();
		$out = array('status' => true, 'pesan' => ' Data berhasil di simpan');
		} else {
		$this->db->trans_rollback();
		$out = array('status' => false, 'pesan' => 'Maaf data gagal di simpan !');
		}

		echo json_encode($out);
	}
	
	
	public function hapus() {
		
		/*ini harus ada boss */
		$access = $this->M_sidebar->access('del','user');
		if ($access->menuview == 0){
			$data['userdata'] = $this->userdata;
			$data['page'] 	= "hapus ";
			$data['judul'] 	= "Delete User";
			$this->loadkonten('layouts/no_akses',$data);
		 }
		 /*ini harus ada boss */
		
		else{	
		$id = $_POST['id'];
		$result = $this->M_user->hapus($id);
		
		if ($result > 0) {
			$out['status'] = 'berhasil';
		} else {
			$out['status'] = 'gagal';
		}
	  }
	}
	
	
}
