<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <title>Login | Dinas Perkebunan</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/tambahan.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/fonts/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/fonts/ionicons.min.css">
  
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/tambahan/style-login.css">
  
    <link rel="icon" href="<?php echo base_url()?>/assets/tambahan/gambar/logo-icon.png">
  
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.css">
  
    <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/iCheck/square/blue.css">
  <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/sweetalert/sweetalert.css">
  <script src="<?php echo base_url(); ?>assets/plugins/sweetalert/sweetalert.min.js"></script>
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/toastr/toastr.css">



  
  
  <?php echo $script_captcha; // javascript recaptcha ?>
  <style type="text/css">
  .login-page { background: url(<?php echo base_url(); ?>assets/tambahan/gambar/login-background.jpg) !important; no-repeat; }
  </style>
  

  </head>
  <body class="hold-transition login-page">
  <div id='LoadingDulu'></div>
    <div class="login-box">
      <div class="login-logo">
        <img src="<?php echo base_url(); ?>assets/tambahan/gambar/logo.png"> 
      </div>

      <!-- /.login-logo -->
      <div class="login-box-body">
      <p class="login-box-msg">
           Login Admin
      </p>

        <form action="<?php echo base_url('Default/Auth/login2'); ?>" method="post" id="FormLogin">
          <div class="form-group has-feedback">
            <input type="text" class="form-control" id="username" placeholder="Username" name="username">
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
             <input type='password' class="form-control" id="password" name="password" placeholder="password" class="span12">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
      
       <label>
  <input type="checkbox" class="form-ok"> <font style='color: black; font-size: 13px;'> <b>Show password</b></font><br>
    </label>

    <?php echo $captcha  ?>
     <br>
      
          <div class="row">
          <div id='btn_loading'></div>
          <div id="hilang">
            <div class="col-xs-offset-8 col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat klik"><i class="fa fa-lock" aria-hidden="true"></i> &nbsp;Login</button>
            </div>
          </div>

          <div id="buka">
            <div class="col-xs-offset-8 col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat klik"><i class="fa fa-unlock" aria-hidden="true"></i> &nbsp;Login</button>
            </div>
          </div>
          </div>
        </form>
      </div>
    
    
      <!-- /.login-box-body -->
     
    </div>
    
  
  <script>
    $(document).ready(function(){   
    $('.form-ok').click(function(){
      if($(this).is(':checked')){
        $('#password').attr('type','text');
      }else{
        $('#password').attr('type','password');
      }
    });
  });
  
  </script>

  <script>
$(function(){
  $("#buka").hide();
  $('#FormLogin').submit(function(e){
    e.preventDefault();
    $.ajax({
    beforeSend: function () {
    $("#buka").hide();
    $("#hilang").hide();
    $("#btn_loading").html("<div class='col-xs-offset-8 col-xs-4'><button type='submit' class='btn btn-primary btn-block btn-flat klik'><i class='fa fa-refresh fa-spin'></i> &nbsp;Wait..</button></div>");
    $("#btn_loading").show();
    },
      url: $(this).attr('action'),
      type: "POST",
      cache: false,
      data: $(this).serialize(),
      dataType:'json',
      success: function(json){
        if(json.status == true){ 
          $("#btn_loading").hide();
          $("#buka").show();
          toastr.success(json.pesan,'Success', {timeOut: 5000},toastr.options = {
          "closeButton": true}); 
          window.location = json.url_home; 
        } else {
          $("#btn_loading").hide();
          $("#hilang").show();
          setTimeout(location.reload.bind(location), 1000);
          toastr.error(json.pesan,'Warning', {timeOut: 5000},toastr.options = {
          "closeButton": true});
        }
      }
    });
  });
});
</script>


    <!-- /.login-box -->
    <!-- jQuery 2.2.3 -->
    <script src="<?php echo base_url(); ?>assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/toastr/toastr.js"></script>
  
   
  </body>
</html>
