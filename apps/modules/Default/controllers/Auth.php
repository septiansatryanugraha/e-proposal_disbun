<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('M_auth');
	}
	
	public function index() {
		$session = $this->session->userdata('status');
        
        $data = array(
				'captcha' => $this->recaptcha->getWidget(), // menampilkan recaptcha
                'script_captcha' => $this->recaptcha->getScriptTag(),
				   );

		if ($session == '') {
			$this->load->view('login', $data);
		} else {
			$this->load->view('login', $data);
		}
	}

	public function login2() {
		
		if($this->input->is_ajax_request())
		{
		    $this->form_validation->set_rules('username', 'Username', 'required|min_length[4]|max_length[30]');
		    $this->form_validation->set_rules('password', 'Password', 'required');
		    $this->form_validation->set_rules('g-recaptcha-response', '<strong>Captcha</strong>', 'callback_getResponseCaptcha');
			$recaptcha = $this->input->post('g-recaptcha-response');
       		$response = $this->recaptcha->verifyResponse($recaptcha);
			
			if ($this->form_validation->run() == TRUE || !isset($response['success']) || $response['success'] == true) {

				$username = trim($_POST['username']);
				$password = trim($_POST['password']);

				$data = $this->M_auth->login($username, $password);
			
				/* ketika status aktif tidak sama*/	
			
            $session['id'] = $data->id;
            $session['username'] = $data->username;
            $session['status'] = $data->status;
         
            $stat = $data->status;
            if($stat==3) {
                $this->session->set_userdata($session);	
                $status = 3;
                $this->M_auth->update($session['id'],$status);
				$data = array(
				'last_login_user' => date('Y-m-d H:i:s')
				);
				
		        $this->M_auth->update_user($session['id'],$data);
				$URL_home = base_url('Dashboard');
				$json['status']		= true;
				$json['url_home'] 	= $URL_home;
				$json['pesan'] 	= "Success Login.";
					
            } else {
                $json['pesan'] 	= "Maaf akun belum aktif.";
            }
			
			
			if ($data == false) {
				$json['pesan'] 	= "Username / Password salah";
			} else {			
				$this->session->set_userdata($session);
			}

			} elseif (isset($recaptcha)) {

				$json['pesan'] 	= "Captcha harus di isi";

			}
			
			else {
			$json['pesan'] 	= "Maaf tidak bisa login, silahkan cek user dan password anda";
		}
		
		}
		else
		{
			redirect('login');
		}

		echo json_encode($json);
			
	}


	 public function getResponseCaptcha($str)
    {
        $this->load->library('recaptcha');
        $response = $this->recaptcha->verifyResponse($str);
        if ($response['success'])
        {
            return true;
        }
        else
        {
            $this->form_validation->set_message('getResponseCaptcha', '%s is required.' );
            return false;
        }
    }

	public function logout() {
		$this->session->sess_destroy();
		redirect('login');
	}
}
