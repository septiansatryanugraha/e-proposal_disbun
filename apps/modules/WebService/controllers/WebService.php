<?php
if(!defined('BASEPATH'))exit('No direct script access allowed');

class WebService extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_service');
	}
	
	
    	// function login web service
     	function checklogin(){
	    
         $datainput = json_decode(file_get_contents('php://input'), true);
               
                $data=array(
                    'email'=>$datainput['email'],
                    'password'=> base64_encode($datainput['password']),
                );
                $res=$this->M_service->login($data);
                
                $user=$datainput['email'];
                
                $data2=array(
                    'last_login_user' => date('Y-m-d H:i:s')
                );
                
                $this->M_service->update_user($user,$data2);
                
                
                if($res){
                    
                    if($res[0]->status == "Non Aktif"){
		                $result = array(
		                	'status'=> 'false',
							'pesan'=> 'Maaf akun anda belum aktif, Silahkan hubungi Admin kami !',
		            	);
		                echo json_encode($result);
	                } 
                    
                    
	                else if($res[0]->tipe == "employe"){
		               
	                	$res2=$this->M_service->getEmploye($res[0]->email);
						$res3=$this->M_service->getTake5($res[0]->nama);
						$res4=$this->M_service->getPSB($res[0]->nama);
				
		                $result = array(
		                	'status'=> 'true',
		                	'Profile'=> $res2,
							'Take 5 Form'=> $res3,
							'PSB Form'=> $res4,
		            	);
		                echo json_encode($result);
					}
						
				    
	            }
				
				  else{
	                $result = array(
	                	'status'=>  'false',
						'pesan'=> 'Maaf email atau password anda salah',
	            	);
	                echo json_encode($result);
	            }
			}
			
			
			
		// registrasi siswa
		public function daftar() {
		
		$date = date('Y-m-d H:i:s');
   
        $data = array(
		       'nama_employe'      => $this->input->post('nama_employe'),
		       'email'             => $this->input->post('email'),
		       'divisi'            => $this->input->post('divisi'),
		       'cabang'            => $this->input->post('cabang'),
		       'nama'              => $this->input->post('nama_employe'),
		       'gambar'            => 'upload/foto/no-image.jpg',
		       'status' 		   => "Non aktif",
               'created_date'      => $date,
               'updated_date'      => $date,
               'tanggal'           => $date,
		); 
		
		$data2=array(
		'nama' 		             => $this->input->post('nama_employe'),
		'email' 		   		 => $this->input->post('email'),
		'status' 		         => "Non Aktif",
		'gambar'                 => 'upload/foto/no-image.jpg',
		'tipe'                   => 'employe',		
		'password' 		   		 => base64_encode($this->input->post("password")),
		 );	
		
        
         $checkuser = $this->M_service->checkUsername($data);
	     if(!$checkuser){
		 $res=$this->M_service->saveRegis($data);
		 $res=$this->M_service->insertLogin($data2);
		 $result = array(
	     'status'=>  'true',
		 'pesan'=>   'Selamat pendaftaran berhasil !!',
	      );
		 echo json_encode($result);
		 } 
		 else {
		 $result = array(
	     'status'=>  'false',
		 'pesan'=>   'Email sudah terdaftar, gunakan Email yang lain !!',
	      );
		 echo json_encode($result);
		 }
      }
      
       public function simpan_form_psb(){

        $date = date('Y-m-d H:i:s');
       	$files = $_FILES;
               $count = count($_FILES['gambar']['name']);
               
              //Multiple upload (hanya gambar)
               for($i=0; $i<$count; $i++)
                 {
                 $_FILES['gambar']['name']= $files['gambar']['name'][$i];
                 $_FILES['gambar']['type']= $files['gambar']['type'][$i];
                 $_FILES['gambar']['tmp_name']= $files['gambar']['tmp_name'][$i];
                 $_FILES['gambar']['error']= $files['gambar']['error'][$i];
                 $_FILES['gambar']['size']= $files['gambar']['size'][$i];
                 $config['upload_path'] = './upload/psb/';
                 $config['allowed_types']='gif|jpg|png|jpeg';
	             $config['max_size'] = '5048'; //maksimum besar file 2M
                 $config['overwrite'] = TRUE;
                 $config['remove_spaces'] = true;

                 $this->load->library('upload', $config);
                 $this->upload->initialize($config);
                 $this->upload->do_upload('gambar');
                 
                 
                //Compress Image
                $config['image_library']='gd2';
                $config['source_image']='./upload/psb/'.$_FILES['gambar']['name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= TRUE;
                $config['width']= 500;
                $config['height']= 500;
                $config['quality']= '70%';
                $config['new_image']= './upload/psb/'.$_FILES['gambar']['name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize(); 
                 
                 $path['link']= "upload/psb/";
                 $fileName = $path['link'] . ''.$_FILES['gambar']['name'];
                 $images[] = $fileName;
                 }

                 $fileName = implode(',',$images);
   		         $data = array(
   			     'gambar'            => $fileName,
		         'nama_pelapor'      => $this->input->post('nama_pelapor'),
		         'cabang'            => $this->input->post('cabang'),
		         'department'        => $this->input->post('department'),
		         'survey'            => $this->input->post('survey'),
		         'tgl_lapor'         => $this->input->post('tgl_lapor'),
		         'lokasi_kerja'      => $this->input->post('lokasi_kerja'),
		         'rincian_kejadian'  => $this->input->post('rincian_kejadian'),
		         'tindakan'          => $this->input->post('tindakan'),
		         'komentar'          => $this->input->post('komentar'),
		         'created_by'        => $this->input->post('nama_pelapor'),
		         'created_date'      => $date,
                 'updated_date'      => $date,
                 'tanggal'           => $date,
		); 
		$res = $this->db->insert('tbl_form_psb', $data);

		if ($res > 0) {
		 $result = array(
	     'status'=>  'true',
		 'pesan'=>   'Form Berhasil Disimpan !!',
	      );
		  echo json_encode($result);
		  } else {
		  $result = array(
	     'status'=>  'false',
		 'pesan'=>   'Form Gagal Disimpan !!',
	      );
		  echo json_encode($result);
		  }
		 
		}
		
// 		public function simpan_tes() {

//               $files = $_FILES;
//               $count = count($_FILES['gambar']['name']);
//               for($i=0; $i<$count; $i++)
//                  {
//                  $_FILES['gambar']['name']= $files['gambar']['name'][$i];
//                  $_FILES['gambar']['type']= $files['gambar']['type'][$i];
//                  $_FILES['gambar']['tmp_name']= $files['gambar']['tmp_name'][$i];
//                  $_FILES['gambar']['error']= $files['gambar']['error'][$i];
//                  $_FILES['gambar']['size']= $files['gambar']['size'][$i];
//                  $config['upload_path'] = './upload/tes/';
//                  $config['allowed_types']='gif|jpg|png|jpeg';
// 	             $config['max_size'] = '5048'; //maksimum besar file 2M
//                  $config['overwrite'] = TRUE;
//                  $config['remove_spaces'] = true;

//                  $this->load->library('upload', $config);
//                  $this->upload->initialize($config);
//                  $this->upload->do_upload('gambar');
//                  $path['link']= "upload/tes/";
//                  $fileName = $path['link'] . ''.$_FILES['gambar']['name'];
//                  $images[] = $fileName;
//                  }

//                  $fileName = implode(',',$images);
		
// 		        $data = array(
// 		        'gambar'            => $fileName,
// 		        );

// 		         $res = $this->db->insert('tbl_gambar', $data);

		
// 		  if ($res > 0) {
// 		 $result = array(
// 	     'status'=>  'true',
// 		 'pesan'=>   'Form Berhasil Disimpan !!',
// 	      );
// 		  echo json_encode($result);
// 		  } else {
// 		  $result = array(
// 	     'status'=>  'false',
// 		 'pesan'=>   'Form Gagal Disimpan !!',
// 	      );
// 		  echo json_encode($result);
// 		  }

//      }


		public function simpan_take5(){

		$date = date('Y-m-d H:i:s');
   		$data = array(
		       'nama_pelapor'       => $this->input->post('nama_pelapor'),
		       'cabang'             => $this->input->post('cabang'),
		       'department'         => $this->input->post('department'),
		       'lokasi'             => $this->input->post('lokasi'),
		       'tgl_lapor'          => $this->input->post('tgl_lapor'),
		       'pekerjaan'          => $this->input->post('pekerjaan'),
		       'survey_1'           => $this->input->post('survey_1'),
		       'survey_2'           => $this->input->post('survey_2'),
		       'survey_3'           => $this->input->post('survey_3'),
		       'survey_4'           => $this->input->post('survey_4'),
		       'survey_5'           => $this->input->post('survey_5'),
		       'survey_6'           => $this->input->post('survey_6'),
		       'survey_7'           => $this->input->post('survey_7'),
		       'survey_8'           => $this->input->post('survey_8'),
		       'catatan_bahaya'     => $this->input->post('catatan_bahaya'),
		       'catatan_kontrol'    => $this->input->post('catatan_kontrol'),
		       'catatan_survey'     => $this->input->post('catatan_survey'),
		       'created_by'        => $this->input->post('nama_pelapor'),
		       'created_date'      => $date,
               'updated_date'      => $date,
               'tanggal'           => $date,
		); 
		$res = $this->db->insert('tbl_form_5', $data);

		if ($res > 0) {
		 $result = array(
	     'status'=>  'true',
		 'pesan'=>   'Form Berhasil Disimpan !!',
	      );
		  echo json_encode($result);
		  } else {
		  $result = array(
	     'status'=>  'false',
		 'pesan'=>   'Form Gagal Disimpan !!',
	      );
		  echo json_encode($result);
		  }
		 
		}
	  
	  
	    // selek web service slider
	      public function selek_slider(){
		  $res = $this->M_service->selek_slider();
		  if($res){
		  $result = array(
          'status'=>  'true',
           'data'=> $res
           );
		   } else {
           $result = array(
           'status'=>  'false'
           );  
           } 	
           echo json_encode($result);
		}
		
		
	   // selek web service devisi employe
	      public function selek_folder(){
		  $res = $this->M_service->selek_folder();
		  if($res){
		  $result = array(
          'status'=>  'true',
           'data'=> $res
           );
		   } else {
           $result = array(
           'status'=>  'false'
           );  
           } 	
            echo json_encode($result);
		}


		// selek web service devisi employe
	      public function selek_divisi(){
		  $res = $this->M_service->selek_divisi();
		  if($res){
		  $result = array(
          'status'=>  'true',
           'data'=> $res
           );
		   } else {
           $result = array(
           'status'=>  'false'
           );  
           } 	
            echo json_encode($result);
		}
		
		 // selek web service download
		  public function download(){
		  $id_sop = $this->input->post('id_sop');
          $simpan_count =$this->M_service->update_download($id_sop);
          
          $nama_employe = $this->input->post('nama_employe');
		  $divisi = $this->input->post('divisi');
		  $nama_sop = $this->input->post('nama_sop');
          $simpan_log = $this->M_service->history(''.$id_sop. '',''.$divisi. '' ,'' . $nama_employe . '', 'User <b>'. $nama_employe . '</b> Telah Mendownload file SOP <b>'.$nama_sop.'</b>.');
               
		}

		// selek web service cabang employe
	      public function selek_cabang(){
		  $res = $this->M_service->selek_cabang();
		  if($res){
		  $result = array(
          'status'=>  'true',
           'data'=> $res
           );
		   } else {
           $result = array(
           'status'=>  'false'
           );  
           } 	
            echo json_encode($result);
		}


		// selek web service kontak profil
	      public function selek_profil(){
		  $res = $this->M_service->selek_profil();
		  if($res){
		  $result = array(
          'status'=>  'true',
           'data'=> $res
           );
		   } else {
           $result = array(
           'status'=>  'false'
           );  
           } 	
            echo json_encode($result);
		}	

		// selek web service SOP file pdf
	      public function selek_sop(){
		  $res = $this->M_service->selek_sop();
		  if($res){
		  $result = array(
          'status'=>  'true',
          'data'=> $res
           );
		   } else {
           $result = array(
           'status'=>  'false'
           );  
           } 	
            echo json_encode($result);
		}

		// selek web service kontak profile
	      public function selek_kontak(){
		  $res = $this->M_service->selek_kontak();
		  if($res){
		  $result = array(
          'status'=>  'true',
           'data'=> $res
           );
		   } else {
           $result = array(
           'status'=>  'false'
           );  
           } 	
            echo json_encode($result);
		}


		 // selek web service brand perusahaan
	      public function selek_brand(){
		  $res = $this->M_service->selek_brand();
		  if($res){
		  $result = array(
          'status'=>  'true',
           'data'=> $res
           );
		   } else {
           $result = array(
           'status'=>  'false'
           );  
           } 	
            echo json_encode($result);
		}			
						
		  // untuk edit profile foto employe
		    function editphoto(){
				
				$config['upload_path']="./upload/foto/";
				$config['allowed_types']='gif|jpg|png|jpeg';
				$config['max_size'] = '2048'; //maksimum besar file 2M
				$config['encrypt_name'] = TRUE;
				$config['overwrite'] = TRUE;
 
				$this->load->library('upload', $config);
				$path['link']= "upload/foto/";
 
				$this->upload->do_upload('gambar');
				$image_data = $this->upload->data();
				
				$nama = $this->input->post('nama');
        
			    $data=array(
				'nama' 		   		 => $this->input->post('nama'),
				'gambar'             => $path['link'] . ''. $image_data['file_name'],				
				);
				$res = $this->M_service->updateEmploye($data);
				$res = $this->M_service->updateEmploye2($data);
				$res = $this->M_service->selek_empoye($nama);
				
				if($res){
				$result = array(
                	'status'=>  'true',
                	'pesan' =>  'foto berhasil di update',
                	'data'  => $res,
            	);
				}

            	else
            	{
            	    $result = array(
                	'status'=>  'false',
					'pesan' =>  'foto gagal di update',
            	 );
            	 
            	}
				
            	echo json_encode($result);
			}
			
			
			
			// untuk edit profile employe password
// 			function editpassword(){

// 			    $data=array(
// 				'email' 		   		 => $this->input->post('email'),				
// 				'password' 		   		 => $this->input->post('password')
// 				);
// 				$res = $this->M_service->updateSiswa($data);
				
				
// 				if($res){
// 				$result = array(
//                 	'status'=>  'true',
//                 	'pesan' =>  'password berhasil di update',
//             	);
// 				}

//             	else
//             	{
//             	    $result = array(
//                 	'status'=>  'false',
// 					'pesan' =>  'password gagal di update',
//             	 );
            	 
//             	}
				
//             	echo json_encode($result);
		 
// 			}
			
			
			
			
}
