<?php
class M_service extends CI_Model{
    
	
	public function login($data) {
	   
		$this->db->select('*');
		$this->db->from('tbl_login');
		$this->db->where('email', $data['email']);
		$this->db->where('password', $data['password']);
		
		$this->db->limit(1);
		 
		$query = $this->db->get();
		if($query->num_rows() == 1) { 
			return $query->result();
		} else {
			return false;
		}
	}
	
	function update_user($id,$data2)
    {
        $this->db->where('email', $id);
        $this->db->update('tbl_login', $data2);
        return TRUE;
    }
    
    function update_download($id_sop)
    {
    $this->db->where('id_sop', $id_sop);
    $this->db->select('download');
    $count = $this->db->get('tbl_sop')->row();
    $this->db->where('id_sop', $id_sop);
    $this->db->set('download', ($count->download + 1));
    $this->db->update('tbl_sop');
    }
    
    function history($id_sop,$department,$nama_employe, $deskripsi) 
    {
     $date = date('Y-m-d');
     $created = 'System';
     $data['id_sop'] = $id_sop;
     $data['nama'] = $nama_employe;
     $data['department'] = $department;
     $data['deskripsi'] = $deskripsi;
     $data['created_date'] = $date;
     $data['created_by'] = $created;
     $insert_history = $this->db->insert('tbl_histori_download', $data);
     return $insert_history;
    }
	
	
	
	public function selek_slider() {
		
		$sql = " select * from tbl_slider WHERE status ='Aktif'";
		$data = $this->db->query($sql);
		return $data->result();
	}
	
	function selek_divisi()
	{
		$data = $this->db->get('tbl_department');
		return $data->result();
	}

    function selek_folder()
	{
		$data = $this->db->get('tbl_folder');
		return $data->result();
	}

	function selek_cabang()
	{
		$data = $this->db->get('tbl_cabang');
		return $data->result();
	}
	
	function selek_profil()
	{
		$data = $this->db->get('tbl_company');
		return $data->result();
	}

	function selek_sop()
	{
		$sql = " select * from tbl_sop ORDER by id_sop DESC";
		$data = $this->db->query($sql);
		return $data->result();
	}


	function selek_kontak()
	{
		$data = $this->db->get('tbl_kontak');
		return $data->result();
	}

	function selek_brand()
	{
		$data = $this->db->get('tbl_brand');
		return $data->result();
	}
	
	public function saveRegis($data){
		$query = $this->db->insert('tbl_employe', $data); 
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}
	
	public function insertLogin($data){
		$query = $this->db->insert('tbl_login', $data); 
		return $query;
	}

	
	
// 	public function kode(){
// 		  $this->db->select('RIGHT(pendaftaran.no_registrasi,2) as no_registrasi', FALSE);
// 		  $this->db->order_by('no_registrasi','DESC');    
// 		  $this->db->limit(1);    
// 		  $query = $this->db->get('pendaftaran');  //cek dulu apakah ada sudah ada kode di tabel.    
// 		  if($query->num_rows() <> 0){      
// 			   //cek kode jika telah tersedia    
// 			   $data = $query->row();      
// 			   $kode = intval($data->no_registrasi) + 1; 
// 		  }
// 		  else{      
// 			   $kode = 1;  //cek jika kode belum terdapat pada table
// 		  } 
// 			  $tahun=date('Y');
// 			  $batas = str_pad($kode, 3, "0", STR_PAD_LEFT);    
// 			  $kodetampil = "CM".$tahun.$batas;  //format kode
// 			  return $kodetampil;  
// 		 }
	
	
		public function updateEmploye($data)
	{
		$this->db->where('nama',$data['nama']);
		$query=$this->db->update('tbl_login',$data); 
		return $query;
	}
	
		public function updateEmploye2($data)
	{
		$this->db->where('nama',$data['nama']);
		$query=$this->db->update('tbl_employe',$data); 
		return $query;
	}
	
	    public function getEmploye($email){
		$sql = "SELECT * FROM tbl_employe WHERE email='$email' AND status='Aktif'";
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	    public function selek_empoye($nama){
		$sql = "SELECT gambar FROM tbl_login WHERE nama='$nama'";
		$query = $this->db->query($sql);
		return $query->result();
	}
	
     	public function getTake5($nama){
		$sql = "SELECT * FROM tbl_form_5 WHERE nama_pelapor='$nama'";
		$query = $this->db->query($sql);
		return $query->result();
	}
	
		public function getPSB($nama){
		$sql = "SELECT * FROM tbl_form_psb WHERE nama_pelapor='$nama'";
		$query = $this->db->query($sql);
		return $query->result();
	}
	
        public function updatetoken($token, $table, $email){
		$this->db->where('email',$email); 
		$query=$this->db->update($table,array('token'=>$token)); 
		return $query;
	}
	
	
	    public function checkUsername($data){
		$this->db->select('email');
		$this->db->from('tbl_employe');
		$this->db->where('email', $data['email']);
		$this->db->limit(1);
		 
		$query = $this->db->get();
		if($query->num_rows() == 1) { 
			return $query->result();
		} else {
			return false;
		}
	}
}
?>