<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_total extends CI_Model
{

    public function total_petani()
    {
        $data = $this->db->get('tbl_petani');
        return $data->num_rows();
    }

    public function total_user_kab()
    {
        // $data = $this->db->get('tbl_user');
        $sql = " SELECT DISTINCT (kabupaten) as kabupaten FROM tbl_petani ";
        $data = $this->db->query($sql);
        return $data->num_rows();
    }

    public function total_proposal()
    {
        $tahunSession = isset($_SESSION['tahun']) ? $_SESSION['tahun'] : date('Y');
        $this->db->from('tbl_proposal');
        $this->db->where('YEAR(created_date)', $tahunSession);
        $data = $this->db->get();
        return $data->num_rows();
    }

    public function total_proposal_dpa()
    {
        $tahunSession = isset($_SESSION['tahun']) ? $_SESSION['tahun'] : date('Y');
        $sql = " SELECT * FROM tbl_proposal WHERE status_dpa='1' ";
        $sql .= " AND YEAR(created_date) = '{$tahunSession}'";
        $data = $this->db->query($sql);
        return $data->num_rows();
    }

    public function log()
    {
        $sql = " SELECT *FROM tbl_log where status = 'Belum Direkomendasi' ORDER BY id_history DESC limit 30";
        $data = $this->db->query($sql);
        return $data->result();
    }

    public function get_total_proposal()
    {
        $tahunSession = isset($_SESSION['tahun']) ? $_SESSION['tahun'] : date('Y');
        $sql = "SELECT tanggal,COUNT(id_proposal) AS id_proposal";
        $sql .= " FROM tbl_proposal";
        $sql .= " WHERE tanggal between DATE_ADD(date(now()), INTERVAL -30 DAY) and date(now())";
        $sql .= " AND YEAR(created_date) = '{$tahunSession}'";
        $sql .= " GROUP BY tanggal";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }
}
