<?php $this->load->view('_heading/_headerContent') ?>

<style>
  #sertifikasi {
  max-width: 1200px;
  height: 330px;
  margin: 0 auto
  } 
  
 </style>

<section class="content">
<div class="row">

    <div class="col-lg-4 col-xs-6">
    <div class="small-box bg-red">
    <div class="inner">
    <h3><span class="count"><?php echo $petani ?></span></h3>
    <p>Jumlah Kelompok Tani</p>
    </div>
    <div class="icon">
     <i class="fa fa-file"></i>
    </div>
  <!-- <a href="<?php echo site_url('master-petani'); ?>" class="ajaxify small-box-footer klik">Lihat data <i class="fa fa-arrow-circle-right"></i></a> -->
    </div>
    </div>
    
  <div class="col-lg-4 col-xs-6">
    <div class="small-box bg-yellow">
      <div class="inner">
        <h3><span class="count"><?php echo $user ?></span></h3>
       <p>Jumlah User Kabupaten</p>
      </div>
      <div class="icon">
      <i class="ion ion-person-add"></i>
      </div>
    <!-- <a href="<?php echo site_url('master-opd'); ?>" class="ajaxify small-box-footer klik">Lihat data <i class="fa fa-arrow-circle-right"></i></a> -->
    </div>
  </div>

   <div class="col-lg-4 col-xs-6">
    <div class="small-box bg-aqua">
    <div class="inner">
    <h3><span class="count"><?php echo $proposal ?></span></h3>
    <p>Jumlah Pengajuan Proposal</p>
    </div>
    <div class="icon">
     <i class="fa fa-file-text"></i>
    </div>
 <!--  <a href="<?php echo site_url('master-sts'); ?>" class="ajaxify small-box-footer klik">Lihat data <i class="fa fa-arrow-circle-right"></i></a> -->
    </div>
    </div>

    <div class="col-lg-4 col-xs-6">
    <div class="small-box bg-green">
    <div class="inner">
    <h3><span class="count"><?php echo $proposal_dpa ?></span></h3>
    <p>Jumlah Proposal Yang Masuk DPA</p>
    </div>
    <div class="icon">
     <i class="fa fa-file-text"></i>
    </div>
  <!-- <a href="<?php echo site_url('master-sts'); ?>" class="ajaxify small-box-footer klik">Lihat data <i class="fa fa-arrow-circle-right"></i></a> -->
    </div>
    </div>
 
 </div>

<div class="row">
 <section class="col-lg-12">
  <div class="box box-default">
  <div class="box-header with-border">
  <i class="fa fa-file-text" aria-hidden="true"></i>

    <!-- pesan customer -->
  
   <h3 class="box-title">Log Data Pengajuan Terbaru</h3>
   <div class="box-tools pull-right">
   <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
   <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
   </div>
   </div>
   <!-- /.box-header -->
   <div class="box-body">
   <div class="row">
   <div class="col-md-12">

   <table id="tableku" class="table table-bordered table-striped">
   <thead>
   <tr>
   <th>#</th>
   <th>Tanggal Pengajuan</th>
   <th>Keterangan</th>
   </tr>
   </thead>
   <tbody>
   <?php
   $no = 1;
    foreach ($log as $data) {

     $status = '<small class="label pull-center bg-blue">Belum Direkomendasi</small>';
            if ($data->status == 'Selesai Approve') {
                $status = '<small class="label pull-center bg-green">Sertifikat segera dikirim</small>';
            }
    ?>
    <tr>
    <td><?php echo $no; ?></td>
    <td><?php echo date('d-m-Y',strtotime($data->created_date)) ?></td>
    <td><?php echo $data->keterangan ?></td>

    </tr>
    <?php
    $no++;
  }
  ?>
  </tbody>
  </table>

   </div>
   </div>
   </div>
   </section>
 </div>
 
 

    <!-- highchart -->
  <script src="<?php echo base_url(); ?>assets/plugins/highchart/highcharts.js"></script>
  <script src="<?php echo base_url(); ?>assets/plugins/highchart/modules/exporting.js"></script>
  <script src="<?php echo base_url(); ?>assets/plugins/highchart/modules/offline-exporting.js"></script>

   <!-- REPORT BARU -->
   
   
  <div class="row">
  <section class="col-lg-12">
  <div class="box box-default">
  <div class="box-header with-border">
  <i class="fa fa-area-chart" aria-hidden="true"></i>

    <!-- pesan customer -->
  
   <h3 class="box-title">Report Data ( Pengajuan Proposal )</h3>
   <div class="box-tools pull-right">
   <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
   <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
   </div>
   </div>
   <!-- /.box-header -->
   <div class="box-body">
   <div class="row">
   <div class="col-md-12">
   
   
   
  <div id="sertifikasi"></div><br><br>

 <?php
 if (!empty($graph)) {
     foreach($graph as $data){
     $tanggal[] = date('d-m-Y',strtotime($data->tanggal));
     $data1[] = (float) $data->id_proposal;
      }
      } else {
      echo "Belum Ada data yang masuk";
      }
    ?>  
   
   <script>
  jQuery(function(){
    new Highcharts.Chart({
        chart: {
            renderTo: 'sertifikasi',
             type: 'column',
        },
        credits: {
         enabled: false
        },
        title: {
            text: 'Grafik Data Pengajuan',
            x: -20
        },
        xAxis: {
         categories:  <?php echo json_encode($tanggal);?>
        },
        yAxis: {
            title: {
                text: 'Jumlah Data Pengajuan'
            }
        },
        series: [{
            name: 'Data Pengajuan ',
            data: <?php echo json_encode($data1); ?>
        },
    ]
    });
  }); 
   </script>
   
   
    </div>
   </div>
   </div>
   </section>

   
   
   
</section>
 
<br>
   </section>
   
   <script>
   // Animasi angka bergerak dashboard
  
  $('.count').each(function () {
    $(this).prop('Counter',0).animate({
        Counter: $(this).text()
    }, {
        duration: 1000,
        easing: 'swing',
        step: function (now) {
            $(this).text(Math.ceil(now));
        }
    });
  });
   
   
   </script>

   <script type="text/javascript">
     $(document).ready(function () {
        $('#tableku').DataTable();
    });
   </script>

