<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends AUTH_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_total');
    }

    public function loadkonten($page, $data)
    {

        $data['userdata'] = $this->userdata;
        $ajax = ($this->input->post('status_link') == "ajax" ? true : false);
        if (!$ajax) {
            $this->load->view('layouts/header', $data);
        }
        $this->load->view($page, $data);
        if (!$ajax)
            $this->load->view('layouts/footer', $data);
    }

    public function index()
    {
        $data['petani'] = $this->M_total->total_petani();
        $data['user'] = $this->M_total->total_user_kab();
        $data['proposal'] = $this->M_total->total_proposal();
        $data['proposal_dpa'] = $this->M_total->total_proposal_dpa();
        $data['graph'] = $this->M_total->get_total_proposal();
        $data['log'] = $this->M_total->log();
        $data['userdata'] = $this->userdata;

        $data['page'] = "home";
        $data['judul'] = "Beranda";
        $this->loadkonten('home', $data);
    }

    public function changeYear()
    {
        $tahun = $this->input->post('tahun');
        $_SESSION['tahun'] = $tahun;
        echo json_encode($_SESSION);
    }
}

/* End of file Home.php */
/* Location: ./application/controllers/Home.php */