<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mdl_rekap_proposal extends CI_Model
{

    public function log($id_user)
    {
        $tahunSession = isset($_SESSION['tahun']) ? $_SESSION['tahun'] : date('Y');
        
        $sql = "SELECT tbl_log.*,
                IFNULL(tbl_petani.nama_kelompok, '') AS nama_kelompok,
                IFNULL(tbl_petani.nama_ketua, '') AS nama_ketua
                FROM tbl_log AS tbl_log
                LEFT JOIN tbl_proposal ON tbl_log.kode = tbl_proposal.id_proposal
                LEFT JOIN tbl_petani ON tbl_petani.id_petani = tbl_proposal.id_petani
                WHERE tbl_log.id_admin IS NULL
                AND tbl_proposal.id_user = '{$id_user}'
                AND YEAR(tbl_proposal.created_date) = '{$tahunSession}'
                ORDER BY id_history LIMIT 30";
        $data = $this->db->query($sql);
        return $data->result();
    }

    public function export_data($tanggalAwal, $tanggalAkhir, $kab, $status)
    {
        if (strlen($tanggalAwal) > 0) {
            $tanggalAwal = date('Y-m-d', strtotime($tanggalAwal));
        }
        if (strlen($tanggalAkhir) > 0) {
            $tanggalAkhir = date('Y-m-d', strtotime($tanggalAkhir));
        }

        $sql = "SELECT tbl_proposal.*, 
                IFNULL(tbl_komoditi.nama, '') AS komoditi,
                IFNULL(tbl_sub_kegiatan.jenis_kegiatan, '') AS jenis_kegiatan,
                IFNULL(tbl_petani.nama_kelompok, '') AS nama_kelompok
                FROM tbl_proposal AS tbl_proposal
                LEFT JOIN tbl_komoditi ON tbl_komoditi.id_komoditi = tbl_proposal.id_komoditi
                LEFT JOIN tbl_sub_kegiatan ON tbl_sub_kegiatan.id = tbl_proposal.id_sub_kegiatan
                LEFT JOIN tbl_petani ON tbl_petani.id_petani = tbl_proposal.id_petani
                WHERE tbl_proposal.tanggal >= '{$tanggalAwal}'
                AND tbl_proposal.tanggal <= '{$tanggalAkhir}'
                AND tbl_proposal.kabupaten = '{$kab}'";
        if (strlen($status) > 0) {
            $sql .= " AND tbl_proposal.status = '{$status}'";
        }

        $data = $this->db->query($sql);
        if ($data->num_rows() > 0) {
            return $data->result();
        }
    }

    function selectStatus()
    {
        $sql = " select * from status_grup WHERE nama in ('Usulan Baru','Verifikasi','Rekom Bidang','Tidak Rekom','Penetapan')";
        $data = $this->db->query($sql);
        return $data->result();
    }
}
