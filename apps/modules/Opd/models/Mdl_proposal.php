<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mdl_proposal extends CI_Model
{
    const __tableName = 'tbl_proposal';
    const __tableId = 'id_proposal';
    const __tableName2 = 'tbl_user';
    const __tableId2 = 'id_user';
    const __tableName3 = 'tbl_history_dokumen';
    const __tableId3 = 'id_history';
    const __tableId4 = 'kode';
    const __tableusaha = 'tbl_usaha';

    function __construct()
    {
        parent::__construct();
    }

    function getDataDetail($id)
    {
        $sql = " select * from " . self::__tableName . "_detail WHERE " . self::__tableId . " = '{$id}'";
        $data = $this->db->query($sql);
        return $data->result();
    }

    function get_table()
    {
        $table = "tbl_proposal";
        return $table;
    }

    function get_table2()
    {
        $table = "tbl_history";
        return $table;
    }

    public function selekKategori2()
    {
        $this->db->from(self::__tableusaha);
        $data = $this->db->get();
        return $data->result();
    }

    public function selekButton()
    {

        $sql = " select * from tbl_action WHERE id=1";
        $data = $this->db->query($sql);
        return $data->row();
    }

    function get($order_by)
    {
        $table = $this->get_table();
        $this->db->order_by($order_by);
        $query = $this->db->get($table);
        return $query;
    }

    public function selectById($id)
    {
        $sql = "SELECT * FROM tbl_proposal WHERE id_proposal = '{$id}'";
        $data = $this->db->query($sql);
        return $data->row();
    }

    function get_where_custom($col, $value)
    {
        $tahunSession = isset($_SESSION['tahun']) ? $_SESSION['tahun'] : date('Y');

        $table = $this->get_table();
        $this->db->where($col, $value);
        if (strlen($tahunSession) > 0) {
//            $this->db->where("YEAR(created_date)", $tahunSession);
        }
        $this->db->order_by("created_date", "desc");
        $query = $this->db->get($table);
        return $query;
    }

    function get_where_history($col, $value)
    {
        $table = $this->get_table2();
        $this->db->where($col, $value);
        $this->db->order_by("created_date", "desc");
        $query = $this->db->get($table);
        return $query;
    }

    function total_dokumen($column, $value)
    {
        $table = $this->get_table();
        $this->db->where($column, $value);
        $query = $this->db->get($table);
        $num_rows = $query->num_rows();
        return $num_rows;
    }

    function total_histori($column, $value)
    {
        $table = $this->get_table2();
        $this->db->where($column, $value);
        $query = $this->db->get($table);
        $num_rows = $query->num_rows();
        return $num_rows;
    }

    public function get_total($id_user)
    {
        $query = $this->db->query("SELECT tanggal,COUNT(id_dokumen) AS id_dokumen FROM tbl_dokumen WHERE  id_user = '{$id_user}' and tanggal between DATE_ADD(date(now()), INTERVAL -30 DAY) and date(now()) GROUP BY tanggal");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    public function kode()
    {
        $query = $this->db->query("SELECT MAX(RIGHT(kode,3)) AS kode FROM tbl_proposal WHERE DATE(tanggal)=CURDATE()");
        $kd = "";
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $k) {
                $tmp = ((int) $k->no_reg) + 1;
                $kd = sprintf("%03s", $tmp);
            }
        } else {
            $kd = "001";
        }
        date_default_timezone_set('Asia/Jakarta');
        $kodetampil = "DISBUN." . date('y') . "." . date('m') . "." . date('d') . "." . $kd;  //format kode
        return $kodetampil;
    }

    public function selekKomoditi()
    {
        $this->db->from('tbl_komoditi');
        $data = $this->db->get();
        return $data->result();
    }

    public function selekSubKegiatan()
    {
        $this->db->from('tbl_sub_kegiatan');
        $data = $this->db->get();
        return $data->result();
    }

    public function select_kabupaten($kode)
    {
        $this->db->order_by('id', 'ASC');
        $kab = $this->db->get_where('tbl_kabupaten', array('kode' => $kode));
        foreach ($kab->result_array() as $data) {
            $res .= "$data[kabupaten]";
        }
        return $res;
    }

    public function select_kecamatan($kode)
    {

        $this->db->order_by('id', 'ASC');
        $kab = $this->db->get_where('tbl_kecamatan', array('kode_kabupaten' => $kode));

        foreach ($kab->result_array() as $data) {
            $kabupaten .= "<option value='$data[kecamatan]'>$data[kecamatan]</option>";
        }
        return $kabupaten;
    }

    function select_desa($kec)
    {
        $this->db->order_by('id', 'ASC');
        $this->db->group_by("desa");
        $kec = $this->db->get_where('tbl_desa', array('kecamatan' => $kec));

        foreach ($kec->result_array() as $data) {
            $kecamatan .= "<option value='$data[desa]'>$data[desa]</option>";
        }

        return $kecamatan;
    }

    function select_kelompok($desa)
    {
        $status = 'Aktif';
        $this->db->order_by('id_petani', 'ASC');
        $kec = $this->db->get_where('tbl_petani', array('desa' => $desa, 'status' => $status));

        foreach ($kec->result_array() as $data) {
            $kelompok .= "<option value='$data[id_petani]'>$data[nama_kelompok]</option>";
        }

        return $kelompok;
    }

    public function select_ketua($id)
    {
        $this->db->order_by('id_petani', 'ASC');
        $hasil = $this->db->get_where('tbl_petani', array('id_petani' => $id));
        foreach ($hasil->result_array() as $data) {
            $res .= "$data[nama_ketua]";
        }
        return $res;
    }

    function select_sub_kegiatan($res)
    {
        $this->db->order_by('id', 'ASC');
        $kec = $this->db->get_where('tbl_sub_kegiatan', array('id_komoditi' => $res));

        foreach ($kec->result_array() as $data) {
            $sub_kegiatan .= "<option value='$data[id]'>$data[jenis_kegiatan]</option>";
        }

        return $sub_kegiatan;
    }

    function select_jenis_bantuan($res)
    {
        $this->db->order_by('id', 'ASC');
        $result = $this->db->get_where('tbl_jenis_bantuan', array('id_sub_kegiatan' => $res));

        foreach ($result->result_array() as $data) {
            $jenis_bantuan .= "<option value='$data[id]'>$data[jenis_bantuan]</option>";
        }

        return $jenis_bantuan;
    }
    // public function select_satuan($id){
    // $this->db->order_by('id','ASC');
    // $hasil= $this->db->get_where('tbl_jenis_bantuan',array('jenis_bantuan'=>$id));
    // foreach ($hasil->result_array() as $data ){
    // $res.= "$data[satuan]";
    // }
    // return $res;
    // }
    // function select_jenis_kegiatan($desa){
    // $status='Aktif';
    // $kelompok="<option value='null'>-- Pilih Jenis Kegiatan --</option>";
    // $this->db->order_by('id','ASC');
    // $kec= $this->db->get_where('tbl_kegiatan',array('sub_kegiatan'=>$desa));
    // foreach ($kec->result_array() as $data ){
    // $kelompok.= "<option value='$data[jenis_kegiatan]'>$data[jenis_kegiatan]</option>";
    // }
    // return $kelompok;
    // }
    //  public function select_jenis_program($id){
    // $this->db->order_by('id','ASC');
    // $hasil= $this->db->get_where('tbl_kegiatan',array('jenis_kegiatan'=>$id));
    // foreach ($hasil->result_array() as $data ){
    // $res.= "$data[program]";
    // }
    // return $res;
    // }
}
