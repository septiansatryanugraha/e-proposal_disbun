<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mdl_petani extends CI_Model
{
    const __tableName = 'tbl_petani';
    const __tableId = 'id_petani';

    function __construct()
    {
        parent::__construct();
    }

    function get_table()
    {
        $table = "tbl_petani";
        return $table;
    }

    function get_where_custom($col, $value)
    {
        $tahunSession = isset($_SESSION['tahun']) ? $_SESSION['tahun'] : date('Y');

        $table = $this->get_table();
        $this->db->where($col, $value);
        if (strlen($tahunSession) > 0) {
            $this->db->where("YEAR(created_date)", $tahunSession);
        }
        $this->db->order_by("id_petani", "desc");
        $query = $this->db->get($table);
        return $query;
    }

    public function selekButton()
    {

        $sql = " select * from tbl_action WHERE id=2";
        $data = $this->db->query($sql);
        return $data->row();
    }

    public function selekFormat()
    {

        $sql = "select * from tbl_file_form WHERE id_file=1";
        $data = $this->db->query($sql);
        return $data->result();
    }

    public function selectById($id)
    {
        $sql = "SELECT * FROM " . self::__tableName . " WHERE " . self::__tableId . " = '{$id}'";
        $data = $this->db->query($sql);
        return $data->row();
    }

    public function selekStatus()
    {

        $sql = " select * from status_grup WHERE nama in ('Non aktif','Aktif')";
        $data = $this->db->query($sql);
        return $data->result();
    }

    public function selekKab()
    {
        $this->db->from('tbl_kabupaten');
        $data = $this->db->get();
        return $data->result();
    }

    public function selekKec()
    {
        $this->db->from('tbl_kecamatan');
        $data = $this->db->get();
        return $data->result();
    }

    public function selekDes()
    {
        $this->db->from('tbl_desa');
        $data = $this->db->get();
        return $data->result();
    }

    public function simpan_import($data)
    {
        $this->db->insert_batch('tbl_petani', $data);
        return $this->db->affected_rows();
    }

    public function hapus($id)
    {
        $sql = "DELETE FROM " . self::__tableName . " WHERE  " . self::__tableId . " = '{$id}'";

        $this->db->query($sql);

        return $this->db->affected_rows();
    }

    public function select_kabupaten($kode)
    {

        $this->db->order_by('id', 'ASC');
        $kab = $this->db->get_where('tbl_kabupaten', array('kode' => $kode));
        foreach ($kab->result_array() as $data) {
            $res .= "$data[kabupaten]";
        }
        return $res;
    }

    public function select_kecamatan($kode)
    {

        $kabupaten = "<option value='0'>-- Pilih Kecamatan --</option>";
        $this->db->order_by('id', 'ASC');
        $kab = $this->db->get_where('tbl_kecamatan', array('kode_kabupaten' => $kode));

        foreach ($kab->result_array() as $data) {
            $kabupaten .= "<option value='$data[kecamatan]'>$data[kecamatan]</option>";
        }
        return $kabupaten;
    }

    function select_desa($kec)
    {


        $kecamatan = "<option value='0'>-- Pilih Desa --</option>";
        $this->db->order_by('id', 'ASC');
        $this->db->group_by("desa");
        $kec = $this->db->get_where('tbl_desa', array('kecamatan' => $kec));

        foreach ($kec->result_array() as $data) {
            $kecamatan .= "<option value='$data[desa]'>$data[desa]</option>";
        }

        return $kecamatan;
    }

    public function checkFiles($value)
    {
        $this->db->select('nama_kelompok');
        $this->db->from('tbl_petani');
        $this->db->like('nama_kelompok', $value);
        // $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
    // public function check($value) {
    //     $array = array('nama_kelompok' => $value);
    //     $this->db->where($array); 
    //     $data = $this->db->get('tbl_petani');
    //     return $data->num_rows();
    // }
}
