<style type="text/css">
    .filter {
        padding-top: 22px;
    }
</style>
<div class="twelve wide column except">
    <h3 class="ui dividing header"><?= $title ?></h3>
    <div class="ui form">
        <form method="post" action="<?php echo site_url('filter-rekap-proposal'); ?>"> 
            <input type="hidden" name="kab" value="<?php echo $kab ?>">
            <div class="fields">
                <div class="field">           
                    <label>Tanggal Awal</label>
                    <div class="ui calendar">
                        <div class="ui input left icon">
                            <i class="calendar icon"></i>
                            <input type="text" id="from" name="tanggal_awal" value="<?php echo date('01-m-Y'); ?>">
                        </div>
                    </div>
                </div>
                <div class="field">
                    <label>Tanggal Akhir</label>
                    <div class="ui calendar">
                        <div class="ui input left icon">
                            <i class="calendar icon"></i>
                            <input type="text" name="tanggal_akhir" id="to"  value="<?php echo date('t-m-Y'); ?>">
                        </div>
                    </div>
                </div>
                <div class="field ">
                    <label>Status</label>
                    <select name="status"   class="ui dropdown">
                        <option value="">-- pilih status --</option>
                        <?php foreach ($status as $data) { ?>
                            <option value="<?php echo $data->nama; ?>">
                                <?php echo $data->nama; ?>
                            </option>
                        <?php } ?>
                    </select>
                </div>
                <div class="field filter">
                    <button class="ui orange button" type="submit"  ><i class="fa fa-refresh" aria-hidden="true"></i> Filter </button>
                </div>
            </div>
        </form>
    </div>
    <div class="field ">
        <form method="post" action="<?php echo site_url('export-rekap-proposal'); ?>">
            <div class="col-md-1 jarak-kiri">
                <div class="form-group">
                    <label></label>
                    <div class="input-group date">
                        <input type="hidden" name="tanggal_awal"  value="<?php echo $tanggal_awal; ?>">
                        <input type="hidden" name="tanggal_akhir"  value="<?php echo $tanggal_akhir; ?>">
                        <input type="hidden" name="kab"  value="<?php echo $data_kab; ?>">
                        <input type="hidden" name="status"  value="<?php echo $data_status; ?>">
                        <button name="simpan" type="submit" class="btn btn-sm btn-primary batas-export"><i class="fa fa-download"></i> Export Excel</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <br><br>
    <table id="example" class="ui celled table" style="width:100%">
        <thead>
            <tr>
                <th align="center">No</th>
                <th align="center">Kabupaten</th>
                <th align="center">Kecamatan</th>
                <th align="center">Desa</th>
                <th align="center">Kelompok</th>
                <th align="center">Ketua</th>
                <th align="center">Status</th>
                <th align="center">Tanggal Pengajuan</th>
            </tr> 
        </thead>
        <tbody>
            <?php
            if (!empty($filter)) {
                $no = 1;
                foreach ($filter as $data) {
                    $idKelompok = $data->id_petani;
                    $dataKelompok = $this->Mdl_petani->selectById($idKelompok);

                    ?>
                    <tr>
                        <td><?php echo $no ?></td>
                        <td><?php echo $data->kabupaten ?></td>
                        <td><?php echo $data->kecamatan ?></td>
                        <td><?php echo $data->desa ?></td>
                        <td><?php echo $dataKelompok->nama_kelompok ?></td>
                        <td><?php echo $dataKelompok->nama_ketua ?></td>
                        <td><?php echo $data->status ?></td>
                        <td><?php echo date('d-m-Y', strtotime($data->tanggal)); ?></td>
                    </tr>
                    <?php
                    $no++;
                }

                ?>
            <?php } ?>
        </tbody>
    </table>
 </div>
 </div>
 </div>
 </div>
 </div>

<script>


    $(document).ready(function () {
        $('#example').DataTable();
    });

// untuk datetime from
    $(function ()
    {
        $("#from").datepicker({
            dateFormat: 'dd-mm-yy'
        })
    });

    // untuk datetime to
    $(function ()
    {
        $("#to").datepicker({
            orientation: "left",
            autoclose: !0,
            dateFormat: 'dd-mm-yy'
        })
    });

</script>