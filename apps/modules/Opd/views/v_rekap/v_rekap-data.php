<style type="text/css">
    .filter {
        padding-top: 22px;
    }
</style>
<div class="twelve wide column except">
    <h3 class="ui dividing header"><?= $title ?></h3>
    <div class="ui form">
        <form method="post" action="<?php echo site_url('filter-rekap-proposal'); ?>"> 
            <input type="hidden" name="kab" value="<?php echo $kab ?>">
            <div class="fields">
                <div class="field">           
                    <label>Tanggal Awal</label>
                    <div class="ui calendar">
                        <div class="ui input left icon">
                            <i class="calendar icon"></i>
                            <input type="text" id="from" name="tanggal_awal" value="<?php echo date('01-m-Y'); ?>">
                        </div>
                    </div>
                </div>
                <div class="field">
                    <label>Tanggal Akhir</label>
                    <div class="ui calendar">
                        <div class="ui input left icon">
                            <i class="calendar icon"></i>
                            <input type="text" name="tanggal_akhir" id="to"  value="<?php echo date('t-m-Y'); ?>">
                        </div>
                    </div>
                </div>
                <div class="field ">
                    <label>Status</label>
                    <select name="status"   class="ui dropdown">
                        <option value="">-- pilih status --</option>
                        <?php foreach ($status as $data) { ?>
                            <option value="<?php echo $data->nama; ?>"><?php echo $data->nama; ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="field filter">
                    <button class="ui orange button" type="submit"  ><i class="fa fa-refresh" aria-hidden="true"></i> Filter</button>
                </div>
            </div>
        </form>
   </div>
 </div>
 </div>
 </div>
 </div>


<script>

// untuk datetime from
    $(function ()
    {
        $("#from").datepicker({
            dateFormat: 'dd-mm-yy'
        })
    });

    // untuk datetime to
    $(function ()
    {
        $("#to").datepicker({
            orientation: "left",
            autoclose: !0,
            dateFormat: 'dd-mm-yy'
        })
    });

</script>






