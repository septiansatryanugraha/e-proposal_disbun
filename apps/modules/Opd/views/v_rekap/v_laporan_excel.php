<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$title.xls");
header("Pragma: no-cache");
header("Expires: 0");

?>
<center><h2>Report Data Pengajuan Proposal Dinas Perkebunan Kab <?php echo $data_kab ?> </h2></center>
<br>
<table>
    <tr><td></td><td><b>Periode Report &nbsp;&nbsp;&nbsp; :</b></td><td><b>Periode awal &nbsp;&nbsp;&nbsp; : <?php echo date('d-m-Y', strtotime($tanggal_awal)) ?></b></td></tr>
</table>
<table>
    <tr><td></td><td></td><td><b>Periode akhir &nbsp;&nbsp;&nbsp; : <?php echo date('d-m-Y', strtotime($tanggal_akhir)) ?></td></tr></table>
<br><br>
<table border="1" width="80%">
    <thead>
        <tr>
            <th align="center">No</th>
            <th align="center">Tanggal Pengajuan</th>
            <th align="center">Kabupaten</th>
            <th align="center">Kecamatan</th>
            <th align="center">Desa</th>
            <th align="center">Kelompok</th>
            <th align="center">Ketua</th>
            <th align="center">Komoditi</th>
            <th align="center">Sub Kegiatan</th>
            <th align="center">Jenis Bantuan</th>
            <th align="center">Jumlah</th>
            <th align="center">Satuan</th>
            <!-- <th align="center">Prioritas</th> -->
            <th align="center">Kegiatan</th>
            <th align="center">Program</th>
            <th align="center">Status</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (!empty($excel)) {
            $no = 1;
            foreach ($excel as $data) {
                $qKomoditi = "SELECT * FROM tbl_komoditi WHERE id_komoditi = '{$data->id_komoditi}'";
                $resKomoditi = $this->db->query($qKomoditi)->row();

                $dataKelompok = $this->Mdl_petani->selectById($data->id_petani);

                $qSubKegiatan = "SELECT * FROM tbl_sub_kegiatan WHERE id = '{$data->id_sub_kegiatan}'";
                $resSubKegiatan = $this->db->query($qSubKegiatan)->row();

                $sqlProposalDetail = "SELECT * FROM tbl_proposal_detail WHERE id_proposal = '{$data->id_proposal}'";
                $dataProposalDetail = $this->db->query($sqlProposalDetail)->result();

                $kegiatan = "<li>";
                $jenis_bantuan = "<li>";
                foreach ($dataProposalDetail as $key => $value) {
                    $qJenisBantuan = "SELECT * FROM tbl_jenis_bantuan WHERE id = '{$value->id_jenis_bantuan}'";
                    $resJenisBantuan = $this->db->query($qJenisBantuan)->row();
                    $jenis_bantuan .= "" . ($key + 1) . ". " . $resJenisBantuan->jenis_bantuan . "<br>";
                    $kegiatan .= "" . ($key + 1) . ". " . $resJenisBantuan->jenis_bantuan . "<br>";
                }
                $jenis_bantuan .= "</li>";
                $kegiatan .= "</li>";

                $satuan = "<li>";
                foreach ($dataProposalDetail as $key => $value) {
                    $satuan .= "" . $value->satuan . "<br>";
                }
                $satuan .= "</li>";

                $jumlah = "<li>";
                foreach ($dataProposalDetail as $key => $value) {
                    $jumlah .= "" . $value->jumlah . "<br>";
                }
                $jumlah .= "</li>";

                $prioritas = "<li>";
                foreach ($dataProposalDetail as $key => $value) {
                    $prioritas .= "" . $value->prioritas . "<br>";
                }
                $prioritas .= "</li>";

                $program = "<li>";
                $no_jenis_program = 1;
                foreach ($dataProposalDetail as $key => $value) {
                    $program .= "" . $no_jenis_program . ". " . $value->program . "<br>";
                    $no_jenis_program++;
                }
                $program .= "</li>";

                $catatan = "<li>";
                $no_catatan = 1;
                foreach ($dataProposalDetail as $key => $value) {
                    $catatan .= "" . $no_catatan . ". " . $value->catatan . "<br>";
                    $no_catatan++;
                }
                $catatan .= "</li>";

                ?>
                <tr>
                    <td><?php echo $no ?></td>
                    <td><?php echo date('d-m-Y', strtotime($data->tanggal)); ?>&nbsp;</td>
                    <td><?php echo $data->kabupaten ?></td>
                    <td><?php echo $data->kecamatan ?></td>
                    <td><?php echo $data->desa ?></td>
                    <td><?php echo $dataKelompok->nama_kelompok ?></td>
                    <td><?php echo $dataKelompok->nama_ketua ?></td>
                    <td><?php echo $resKomoditi->nama ?></td>
                    <td><?php echo $resSubKegiatan->jenis_kegiatan ?></td>
                    <td><?php echo $jenis_bantuan ?></td>
                    <td><?php echo $jumlah ?></td>
                    <td><?php echo $satuan ?></td>
                    <!-- <td><?php echo $prioritas ?></td> -->
                    <td><?php echo $kegiatan ?></td>
                    <td><?php echo $program ?></td>
                    <td><?php echo $data->status ?></td>
                </tr><?php
                $no++;
            }
        } else {

            ?>
        <center><?php echo "Belum Ada data pengajuan" ?></center>
    <?php } ?>
</tbody>
</table>





