

<style>
.form-ok {
  padding-left: -30px;
}

 .field-icon {
  float: left;
  margin-left: 91%;
  margin-top: -27px;
  position: relative;
  z-index: 2;
}
</style>

<?php
$email = array(
  'type'  => 'email',
  'name'  => '_email',
  'placeholder' => 'E-Mail'
);
$password = array(
  'type'  => 'password',
  'id'    => 'password-field',
  'name'  => '_password',
  'placeholder' => 'Password'
);
$submit = array(
  'type'  => 'submit',
  'name'  => '_login',
  'value' => 'Login',
  'class' => 'ui fluid large teal submit button'
);


?>

<div class="ui middle aligned center aligned grid">
  <div class="column">
    <h2 class="ui teal image header">
    <a href="<?php echo base_url()?>"><img class="image" src="<?php echo base_url(); ?>assets/publik/img/images/logo-dinas.png"></a>
    </h2>
  <?php echo $this->session->flashdata("not_login"); ?>

  <?php if (isset($error)): ?>
    <?php echo '<div class="ui error message"><i class="close icon"></i><div class="header">'.$error.'</div></div>'; ?>
  <?php endif ?>

  <?php
  $attributes = array('class' => 'ui large form');
  echo form_open($this->uri->uri_string, $attributes);
  ?>
      <div class="ui stacked segment">
        <div class="ui error message"></div>
        <div class="field">
          <div class="ui left icon input">
            <i class="user icon"></i>
            <?php
            echo form_error('_email');
            echo form_input($email);
            ?>
          </div>
        </div>
        <div class="field">
          <div class="ui left icon input">
            <i class="lock icon"></i>
            <?php
            echo form_error('_password');
            echo form_input($password);
            ?>
          </div>
          <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password">
        </div>
        <div class="field">
        <?php echo $captcha  ?>
        </div>


        <?php echo form_submit($submit);?>
        
      </div>
  <?php echo form_close();?>
 <!--  <div class="ui message">
      BELUM PUNYA AKUN? <a href="<?php echo base_url().'daftar-akun.html'?>">Daftar Sekarang!</a><br>
    </div> -->

  </div>
</div>


<script>
      // untuk show hide password
  $(".toggle-password").click(function() {

  $(this).toggleClass("fa-eye fa-eye-slash");
  var input = $($(this).attr("toggle"));
  if (input.attr("type") == "password") {
    input.attr("type", "text");
  } else {
    input.attr("type", "password");
  }
  });
      
</script>
