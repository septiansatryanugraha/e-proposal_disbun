<div class="twelve wide column except">
    <h3 class="ui dividing header"><?= $title ?></h3>
    <div class="ui info message">
        <div class="content">
            <h5>Selamat Datang Kabupaten <?php echo $this->session->userdata('kabupaten'); ?></h5> <br>
        </div>
        <?php foreach ($bantuan as $data) { ?>
            <div class="header">Persyaratan <?php echo $data->judul_bantuan ?></div>
            <p><?php echo $data->konten ?></p>
        <?php } ?>
    </div>
    
        <div class="ui info message">
        <div class="content">
            <h5>History Pengajuan Kabupaten <?php echo $this->session->userdata('kabupaten'); ?></h5> <br>
        </div>
        <table id="example" class="ui celled table" style="width:100%">
        <thead>
                <tr>
                    <th style="text-align: center; width: 15%">Tanggal Pengajuan</th>
                    <th style="text-align: center; width: 15%">Kelompok</th>
                    <th style="text-align: center; width: 15%">Ketua</th>
                    <th style="text-align: center; width: 40%">Keterangan</th>
                    <th style="text-align: center; width: 15%">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($log as $data) { ?>
                    <tr>
                        <td style="text-align: left;"><?php echo date('d-m-Y', strtotime($data->created_date)) ?></td>
                        <td style="text-align: left;"><?php echo $data->nama_kelompok ?></td>
                        <td style="text-align: left;"><?php echo $data->nama_ketua ?></td>
                        <td style="text-align: left;"><?php echo $data->keterangan ?></td>
                        <td><a href="<?php echo site_url('view-data-proposal/' . $data->kode) ?>" class='tiny ui primary button' target='__blank'><span tooltip='Lihat Data'><i class='fa fa-eye'></i></span></a></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
</div>
</div>


// <script>


//     $(document).ready(function () {
//         $('#example').DataTable();
//     });


// </script>

<script type="text/javascript">
    $(document).ready(function () {
        $('#example').DataTable({
            oLanguage: {
                "sLengthMenu": "_MENU_ &nbsp;&nbsp;Data Per Halaman",
                "sInfo": "Menampilkan _START_ s/d _END_ dari <b>_TOTAL_ data</b>",
                "sInfoFiltered": "(difilter dari _MAX_ total data)",
                "sEmptyTable": "Data tidak ada di server",
                "sInfoPostFix": "",
                "sSearch": "<i class='fa fa-search fa-fw'></i> Pencarian : ",
                "sPaginationType": "simple_numbers",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Pertama",
                    "sPrevious": "Sebelumnya",
                    "sNext": "Selanjutnya",
                    "sLast": "Terakhir"
                }
            }
        });
    });
</script>

<!-- highchart -->
<script src="<?php echo base_url(); ?>assets/plugins/highchart/highcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/highchart/modules/exporting.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/highchart/modules/offline-exporting.js"></script>
<script>
    jQuery(function () {
        new Highcharts.Chart({
            chart: {
                renderTo: 'graph',
                type: 'column',
            },
            credits: {
                enabled: false
            },
            title: {
                text: 'Grafik Data',
                x: -20
            },
            xAxis: {
                categories: <?php echo json_encode($tanggal); ?>
            },
            yAxis: {
                title: {
                    text: 'Jumlah Data Pengajuan'
                }
            },
            series: [{
                    name: 'Data Pengajuan ',
                    data: <?php echo json_encode($data1); ?>
                },
            ]
        });
    });
</script>
<script>
// Animasi angka bergerak dashboard

    $('.count').each(function () {
        $(this).prop('Counter', 0).animate({
            Counter: $(this).text()
        }, {
            duration: 1000,
            easing: 'swing',
            step: function (now) {
                $(this).text(Math.ceil(now));
            }
        });
    });


</script>

