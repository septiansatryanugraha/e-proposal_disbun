<div class="ui vertical segment">
    <div class="ui stackable grid container">
      <div class="row">
        <div class="four wide column except">
          <div class="ui vertical pointing menu">
             <a href="<?php echo base_url();?>beranda.html" class="item">
              <i class="fa fa-tachometer" aria-hidden="true"></i>&nbsp;&nbsp; Dashboard
            </a>
            <a href="<?php echo base_url();?>data-kelompok-petani.html" class="item">
              <i class="fa fa-group" aria-hidden="true"></i>&nbsp;&nbsp; Data Kelompok Tani
            </a>
            <a href="<?php echo base_url();?>data-proposal.html" class="item">
              <i class="fa fa-file-text-o" aria-hidden="true"></i>&nbsp;&nbsp; Pengajuan Proposal
            </a>
           <!--  <a href="<?php echo base_url();?>data-history.html" class="item">
              <i class="fa fa-undo" aria-hidden="true"></i>&nbsp;&nbsp; Proses
            </a> -->
            <a href="<?php echo base_url();?>rekap-proposal.html" class="item">
              <i class="fa fa-files-o" aria-hidden="true"></i>&nbsp;&nbsp; Rekap Data Pengajuan
            </a>
            <a href="<?php echo base_url();?>profil-user.html" class="item">
              <i class="fa fa-user" aria-hidden="true"></i>&nbsp;&nbsp; Profil
            </a>
          </div>
        </div>