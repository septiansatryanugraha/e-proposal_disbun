
<style>

  /* pop out login */


  

.jwpopup-head {
margin-left:10px;
margin-top:10px;
margin-right:10px;
}

.jwpopup-main {padding-right:10px;}

/* tambahkan efek animasi */
@-webkit-keyframes animatetop {
    from {top:-300px; opacity:0}
    to {top:0; opacity:1}
}

@keyframes animatetop {
    from {top:-300px; opacity:0}
    to {top:0; opacity:1}
}

.jwpopup3 {
    display: none;
    position: fixed;
    z-index: 3;
    padding-top: 10%;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    overflow: auto;
    background-color: rgb(0,0,0);
    background-color: rgba(0,0,0,0.7);
}
.jwpopup-content3 {
  border-radius: 10px 10px 10px 10px;
    position: relative;
    background-color: #ffffff;
    margin: auto;
    padding: 0;
    max-width: 600px;
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
    -webkit-animation-name: animatetop;
    -webkit-animation-duration: 0.4s;
    animation-name: animatetop;
    animation-duration: 0.4s
}

/* style untuk tombol close */

.exit3 {
  margin-top: 6px;
  margin-right: 6px;
    color: #737272;
    float: right;
    font-size: 20px;
    font-weight: bold;
}

.exit3:hover, .close2:focus {
    color: #000000;
    text-decoration: none;
    cursor: pointer;
}

#btn_loading2 {
    display: none; }

</style>

<div class="twelve wide column except">
<h3 class="ui dividing header"><?=$title?></h3>

<div class="ui form">
<div class="fields">
<div class="field">
      <?php if ($status_button->status=='Enabled') { ?>
      <a href="<?php echo site_url('add-kelompok-petani'); ?>"><button class="ui primary button" ><i class="fa fa-plus" aria-hidden="true"></i> Tambah Kelompok Tani</button></a><br><br>
  <?php } else { ?>
  <?php } ?>
</div>

<!-- <div class="field">
      <?php if ($status_button->status=='Enabled') { ?>
      <a href="javascript:void(0);" id="jwpopupLink-login"><button class="ui green button" ><i class="fa fa-plus" aria-hidden="true"></i> Import Excel</button></a><br><br>
  <?php } else { ?>
  <?php } ?>
</div> -->

<!-- <div class="field">
<?php foreach  ($file_form as $brand) { ?>
<a href="<?php echo base_url().$brand->path; ?>"><button class="ui orange button" ><i class="fa fa-download" aria-hidden="true"></i> Download format excel </button></a>
<?php }  ?>
</div> -->
</div>
</div>

 <table id="example" class="ui celled table" style="width:100%">
 <thead>
 <tr>
 <th>No</th>
 <th>Nama Kelompok</th>
 <th>Ketua</th>
 <th>Kecamatan</th>
 <th>Desa</th>
 <th>Action</th>
 </tr>	
 </thead>
 <tbody>
		
 </tbody>
 </table>

 <div id="jwpopupBox3" class="jwpopup3">
  <!-- jwpopup content -->
  <div class="jwpopup-content3 ">
  <div class="jwpopup-head">
  
  <span class="exit3">×</span>
  
  <div class="jwpopup-main">

  <div class="omb_login"> 
  <div class="title-javascript ">
  <br>
  <b><h3><center><p class="title-left">Import Excel</p></center></h3></b>
  </div>
   
  <div class="ui stacked segment"> 
  <form class="form-horizontal" id="form-import" method="post" enctype="multipart/form-data" role="form">
  <div class="field">
  <label>File Excel</label>
  <input type="file" class="form-control" name="excel" id="excel" onchange="return valid_file_excel()"/>
  </div><br>
  <div id="buka2">
  <button name="simpan" type="submit" class="tiny ui teal submit button"><i class="fa fa-upload"></i> Upload</button>
  </form>
  </div>
  </div> <br>
    
  </div>
  </div>
  </div>
  </div>

 </div>
 </div>
 </div>
 </div>
  </div>



  <script type="text/javascript">
    //Proses Controller logic ajax
   
    $('#form-import').submit(function(e) {
       var error = 0;
       var message = "";

       var data = $(this).serialize();

        var excel = $("#excel").val();
        var excel = excel.trim();

          if (error == 0) {
            if (excel.length == 0) {
                error++;
                message = "File wajib di isi.";
            }
        }

        if (error == 0) {
            $.ajax({
                method: 'POST',
                beforeSend: function (){
         $("#LoadingDulu").html("<div id='LoadingContent'><i class='fa fa-spinner fa-spin'></i> Tunggu sebentar..</div>");
         $("#LoadingDulu").show();
         },
                url:'<?php echo site_url();?>Opd/Petani/import',
                type:"post",
                data:new FormData(this),
                processData:false,
                contentType:false,
                cache:false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == true) {
                    document.getElementById("form-import").reset();
                    $("#LoadingDulu").hide();
                    swal("Success", result.pesan, "success");
                    reload_table();
                    setTimeout(location.reload.bind(location), 500);
                } else if (result.status == 'awas') {
                    $("#LoadingDulu").hide();
                    swal("Warning", result.pesan, "warning");
                }
                
                 else {
                    $("#LoadingDulu").hide();
                    swal("Warning", result.pesan, "warning");
                    setTimeout(location.reload.bind(location), 500); 
                }
            })
            e.preventDefault();
        } else {
            swal("Peringatan", message, "warning");
            return false;
        }
    });        
  </script>


<script type="text/javascript">

 //untuk load data table ajax	
	
var save_method; //for save method string
var table;

$(document).ready(function() {

    //datatables
    table = $('#example').DataTable({ 
      "aLengthMenu": [[25, 50, 75, 100, -1], [25, 50, 75, 100, "All"]],
            "pageLength": 25,
            "processing": true, //Feature control the processing indicator.
            "order": [], //Initial no order.
             oLanguage: {
                "sProcessing": "<img src='<?php base_url(); ?>assets/tambahan/gambar/loading.gif' width='25px'>",
                "sLengthMenu": "_MENU_ &nbsp;&nbsp;Data Per Halaman",
                "sInfo": "Menampilkan _START_ s/d _END_ dari <b>_TOTAL_ data</b>",
                "sInfoFiltered": "(difilter dari _MAX_ total data)",
                "sEmptyTable": "Data tidak ada di server",
                "sInfoPostFix": "",
                "sSearch": "<i class='fa fa-search fa-fw'></i> Pencarian : ",
                "sPaginationType": "simple_numbers",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Pertama",
                    "sPrevious": "Sebelumnya",
                    "sNext": "Selanjutnya",
                    "sLast": "Terakhir"
                }
            },

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('Opd/Petani/ajax_list')?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],

    });

});


function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}

$(document).on("click",".hapus-petani",function(){
	var id_petani=$(this).attr("data-id");
	
	swal({
    title:"Hapus Data?",
    text:"Yakin anda akan menghapus data ?",
    type: "warning",
    showCancelButton: true,
    confirmButtonText: "Hapus",
    confirmButtonColor: '#dc1227',
    customClass: ".sweet-alert button",
    closeOnConfirm: false,
    html: true
  },
		function(){
     $(".confirm").attr('disabled', 'disabled');
		 $.ajax({
			method: "POST",
			url: "<?php echo base_url('Opd/Petani/hapus'); ?>",
			data: "id_petani=" +id_petani,
			success: function(data){
     		var result = jQuery.parseJSON(data);
     	 if (result.status == true) {
			$("tr[data-id='"+id_petani+"']").fadeOut("fast",function(){
			$(this).remove();
          });
          swal("Success", result.pesan, "success");
          $("#LoadingDulu").hide();
          } else {
          swal("Warning", result.pesan, "warning");      
          }
				// hapus_berhasil();
				reload_table();
			}
		 });
	});
});
</script>

<script type="text/javascript">
  

// untuk mendapatkan jwpopup
var jwpopup3 = document.getElementById('jwpopupBox3');

// untuk mendapatkan link untuk membuka jwpopup
var mpLink = document.getElementById("jwpopupLink-login");


// untuk mendapatkan aksi elemen close
var close = document.getElementsByClassName("exit3")[0];

// membuka jwpopup ketika link di klik
mpLink.onclick = function() {
    jwpopup3.style.display = "block";
}

// membuka jwpopup ketika elemen di klik
close.onclick = function() {
    jwpopup3.style.display = "none";
}

// membuka jwpopup ketika user melakukan klik diluar area popup
window.onclick = function(event) {
    if (event.target == jwpopup3) {
        jwpopup3.style.display = "none";
    }
}

</script>

<script type="text/javascript">
  function valid_file_excel()
{
     var fileInput =document.getElementById("excel").value;
     if(fileInput!='')
     {
           var checkfile = fileInput.toLowerCase();
          if (!checkfile.match(/(\.xls|\.xlsx)$/)){ // validasi ekstensi file
               swal("Peringatan", "File harus format .excel", "warning");
               document.getElementById("excel").value = '';
              return false;
           }
            var ukuran = document.getElementById("files"); 
            if(ukuran.files[0].size > 507200)  // validasi ukuran size file
            {
             swal("Peringatan", "File harus maksimal 1MB", "warning");
             ukuran.value = '';
             return false;
             }
             return true;
      }
}
</script>