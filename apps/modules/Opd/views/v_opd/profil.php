<style>

#loadingImg{
    margin-top: -3px;
    position: absolute;
    margin-left: 120px;
}

 .field-icon {
  float: left;
  margin-left: 91%;
  margin-top: -27px;
  position: relative;
  z-index: 2;
}

.text-peta
{
  margin-top: 23px;
}

</style>
<?php  

  $dec_pass = base64_decode($user->password);  

 ?>


<div class="twelve wide column except">
<h3 class="ui dividing header"><?=$title?></h3>


<form class="ui large form" id="form-tambah"  method="POST" enctype="multipart/form-data">
<input type="hidden" name="id_user" value="<?= $sessid; ?>">
<input type="hidden" name="nama" value="<?= $nama; ?>">
<div class="ui stacked segment">

<div class="field">
<label>Nama Lengkap</label>
<div class="ui left icon input">
<i class="user icon"></i>
<input type="text" name="nama_lengkap" placeholder="Nama Lengkap" id="nama_lengkap" max-length="35" autocomplete="off" value="<?php echo $user->nama_lengkap; ?>">
</div>
</div>
   
	
	 <div class="field">
   <label>Email Kabupaten</label>
   <div class="ui left icon input">
   <i class="envelope icon"></i>
   <input type="text" name="email" placeholder="Masukan Email" autocomplete="off" value="<?php echo $user->email ?>" disabled>
   </div>
   </div>

   <div class="field">
   <label>Alamat Kabupaten</label>
   <div class="ui left icon input">
   <i class="home icon"></i>
   <input type="text" name="alamat" placeholder="Alamat Pendaftar" id="alamat" autocomplete="off" value="<?php echo $user->alamat ?>">
   </div>
   </div>

   <div class="field">
   <label>Kabupaten</label>
   <div class="ui left icon input">
   <i class="home icon"></i>
   <input type="text" name="alamat" placeholder="Alamat Pendaftar" id="alamat" autocomplete="off" value="<?php echo $user->kabupaten?>"disabled>
   </div>
   </div>


    <div class="two fields">
    <div class="field">
    <label>Password</label>
    <div class="ui left icon input">
    <i class="user icon"></i>
    <input type="password" name="password" id="password-field" placeholder="Password"  autocomplete="off" value="<?= $dec_pass; ?>">
    </div>
    <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password">
    </div>
    
     
    <div class="field">
    <label>Konfirmasi Password</label>
    <div class="ui left icon input">
    <i class="user icon"></i>
    <input type="password" name="password" id="password-field2"  placeholder="Konfirmasi Password"  autocomplete="off" value="<?= $dec_pass; ?>">
    </div>
    <span toggle="#password-field2" class="fa fa-fw fa-eye field-icon toggle-password">
    </div>
    </div>

    <!-- <div class="ui info message">
    <div class="content">
    <div class="header">Info !! </div>
    <p>Silahkan lengkapi data profil anda</p>
    </div>
    </div> -->

    

  <div id="loadingImg">
  <img src="<?php echo base_url().'assets/' ?>tambahan/gambar/loader-muter.gif" width="45px">
  </div>

  <button name="simpan" type="submit" class="ui large teal submit button"><i class="fa fa-save"></i> Update</button>
	</div>
  </form>
  </div>
  </div>
  </div>
  </div>




<script type="text/javascript">

  $(document).ready(function(){
  $("#loadingImg").hide();
  });
    
  $('#form-tambah').submit(function(e) {
        var error = 0;
        var message = "";

        var data = $(this).serialize();
        var nama_lengkap = $("#nama_lengkap").val();
        var nama_lengkap = nama_lengkap.trim();

          if (error == 0) {
            if (nama_lengkap.length == 0) {
                error++;
                message = "Nama Lengkap Wajib di isi.";
            }
        }

        var alamat = $("#alamat").val();
        var alamat = alamat.trim();

          if (error == 0) {
            if (alamat.length == 0) {
                error++;
                message = "Alamat User Wajib di isi.";
            }
        }

      

        var password_field = $("#password-field").val();
        var password_field = password_field.trim();

          if (error == 0) {
            if (password_field.length == 0) {
                error++;
                message = "Password wajib di isi.";
            }
        }

        var password_field2 = $("#password-field2").val();
        var password_field2 = password_field2.trim();

          if (error == 0) {
            if (password_field2.length == 0) {
                error++;
                message = "Konfirmasi password wajib di isi.";
            }
        }


         if (error == 0) {
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                $("#loadingImg").show();
                },
                url: '<?php echo base_url('Opd/Profil_opd/prosesUpdate'); ?>',
                type:"post",
                data:new FormData(this),
                processData:false,
                contentType:false,
                cache:false,
            }).done(function (data) {

                var result = jQuery.parseJSON(data);
                if (result.status == true) {
                    $("#loadingImg").hide();
                    swal("Success", result.pesan, "success");
                } else {
                    $("#loadingImg").hide();
                    swal("Warning", result.pesan, "warning");
                }

                console.log(result);
            })
            e.preventDefault();
        } else {
            swal("Peringatan", message, "warning");
            return false;
          }
    });

</script>


 

<script>
            // untuk show hide password
    $(".toggle-password").click(function() {

    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $($(this).attr("toggle"));
    if (input.attr("type") == "password") {
    input.attr("type", "text");
    } else {
    input.attr("type", "password");
    }
    });
            
</script>   