<style>
    #loadingImgSpecial {
        margin-top: -3px;
        position: absolute;
        margin-left: 120px;
    }

    .font-loading {
        font-family: Futura,Trebuchet MS,Arial,sans-serif; 
        color:red;
        font-size: 14px;
    }
</style>

<div class="twelve wide column except">
    <h3 class="ui dividing header"><?= $title ?></h3>

    <form class="ui large form" id="form-tambah" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="id_user" value="<?= $sessid; ?>">
        <div class="ui stacked segment">

            <div class="ui info message">
                <div class="content">
                    <div class="header">Penting !! </div>
                    <p>Lengkapi form data pengajuan proposal</p>
                </div>
            </div>

            <div class="two fields">
                <div class="five wide column field">
                    <label>Kabupaten</label>
                    <select class="kab" id="kabupaten">
                        <option></option>
                        <option value="<?php echo $this->session->userdata('kode_kabupaten'); ?>"><?php echo $this->session->userdata('kabupaten'); ?></option>
                    </select>
                </div>

                <div class="five wide column field">
                    <label>Kecamatan</label>
                    <select name="kecamatan" id="kecamatan" class="kec"></select>
                </div>

                <div class="five wide column field">
                    <label>Desa</label>
                    <select name="desa" id="desa" class="desa"></select>
                </div>
            </div>
            <div class="field ">
                <input type="hidden" class="form-control"  name="kabupaten" id="nama_kab" aria-describedby="sizing-addon2">
            </div>
            <div class="two fields">
                <div class="five wide column field">
                    <label>Kelompok Tani</label>
                    <select name="kelompok" id="kelompok" class="tani">
                        <option></option>
                    </select>
                </div>
                <div class="five wide column field">
                    <label>Ketua</label>
                    <input type="text" name="ketua" id="ketua" max-length="35" autocomplete="off">
                </div> 
            </div>

            <div class="field ">
                <div id="loadingImg">
                    <img src="<?php echo base_url() . 'assets/' ?>publik/img/images/loading-bubble.gif">
                    <p class="font-loading">Proccessing Data</font></p>
                </div>
            </div>

            <div class="two fields">
                <div class="five wide column field">
                    <label>Jenis Komoditi</label>
                    <select name="komoditi" class="komoditi" id="komoditi" class="ui dropdown">
                        <option value=""></option>
                        <?php foreach ($komoditi as $data) { ?>
                            <option value="<?php echo $data->id_komoditi; ?>"><?php echo $data->nama; ?></option>
                        <?php } ?>
                    </select>
                </div>

                <div class="ten wide column field">
                    <label>Sub Kegiatan</label>
                    <select name="sub_kegiatan" id="sub_kegiatan" class="sub_kegiatan">
                    </select>
                </div>
            </div>

            <div class="field ">    
                <div id="loadingImg5">
                    <img src="<?php echo base_url() . 'assets/' ?>publik/img/images/loading-bubble.gif">
                    <p class="font-loading">Proccessing Data</font></p>
                </div>
            </div>

            <div class="two fields">
                <div class="six wide column field">
                    <label>Jenis Bantuan</label>
                    <select name="jenis_bantuan" id="jenis_bantuan" class="bantuan">
                    </select>
                </div>
            </div>

            <div class="two fields">
                <div class="six wide column field">
                    <button type="button" class="tiny ui primary button" id="addBantuan" ><i class="fa fa-plus" aria-hidden="true"></i> Tambahkan</button>
                </div>
            </div>

            <div class="row dt-table">
                <div class="sixteen wide column">
                    <table id="example" class="ui celled table dataTable no-footer" style="width: 100%;" role="grid" aria-describedby="example_info">
                        <thead>
                            <tr role="row">
                                <th tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 25%;">Jenis Bantuan</th>
                                <th tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 15%;">Volume</th>
                                <th tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 10%;">Satuan</th>
                                <!-- <th tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 10%;">Prioritas</th> -->
                                <th tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 35%;">Keterangan</th>
                                <th tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 5%;">#</th>
                            </tr>	
                        </thead>
                        <tbody id="dd_list" class="dd-list outer"></tbody>
                    </table>
                </div>
            </div>

            <div class="field "></div>

            <div class="row dt-table">
                <div class="sixteen wide column">
                    <table id="example" class="ui celled table dataTable no-footer" style="width: 100%;" role="grid" aria-describedby="example_info">
                        <thead>
                            <tr role="row">
                                <th tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 100%;">Program</th>
                            </tr>	
                        </thead>
                        <tbody id="dd_list_program" class="dd-list-program outer-program"></tbody>
                    </table>
                </div>
            </div>

            <div class="field "></div>

            <div class="row dt-table">
                <div class="sixteen wide column">
                    <table id="example" class="ui celled table dataTable no-footer" style="width: 100%;" role="grid" aria-describedby="example_info">
                        <thead>
                            <tr role="row">
                                <th tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 100%;">Kegiatan</th>
                            </tr>	
                        </thead>
                        <tbody id="dd_list_kegiatan" class="dd-list-kegiatan outer-kegiatan"></tbody>
                    </table>
                </div>
            </div>

            <div class="field ">
                <div id="loadingImg7">
                    <img src="<?php echo base_url() . 'assets/' ?>publik/img/images/loading-bubble.gif">
                    <p class="font-loading">Proccessing Data</font></p>
                </div>
            </div>

            <div id="loadingImgSpecial">
                <img src="<?php echo base_url() . 'assets/' ?>tambahan/gambar/loader-muter.gif" width="45px">
            </div>

            <button name="simpan" id="simpan" type="button" class="ui large teal submit button"><i class="fa fa-save"></i> Ajukan</button>
        </div>
    </form>
</div>
</div>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        $("#loadingImgSpecial").hide();
        $(".kab").select2({
            placeholder: " -- Pilih Kabupaten -- "
        });
        $(".kec").select2({
            placeholder: " -- Pilih Kecamatan -- "
        });
        $(".desa").select2({
            placeholder: " -- Pilih Desa -- "
        });
        $(".tani").select2({
            placeholder: " -- Pilih Kelompok Tani -- "
        });
        $(".komoditi").select2({
            placeholder: " -- Pilih Jenis Komoditi -- "
        });
        $(".bantuan").select2({
            placeholder: " -- Pilih Jenis Bantuan -- "
        });
        $(".sub_kegiatan").select2({
            placeholder: " -- Pilih Sub Kegiatan -- "
        });
        $(".kegiatan").select2({
            placeholder: " -- Pilih Sub Kabupaten -- "
        });
    });

    $(function () {
        $("#loadingImg").hide();
        $("#loadingImg2").hide();
        $("#loadingImg3").hide();
        $("#loadingImg4").hide();
        $("#loadingImg5").hide();
        $("#loadingImg6").hide();
        $("#loadingImg7").hide();
        $("#loadingImg8").hide();

        $.ajaxSetup({
            type: "POST",
            url: "<?php echo base_url('Opd/Pengajuan_proposal/ambil_data') ?>",
            cache: false,
        });

        // custom untuk input

        $("#kabupaten").change(function () {
            var value = $(this).val();
//            console.log(value);
if (value > 0) {
    $.ajax({
        beforeSend: function () {
            $("#loadingImg").fadeIn();
        },
        data: {modul: 'kabupaten', kode: value},
        success: function (respond) {
            $("#nama_kab").val(respond);
            $("#loadingImg").fadeOut();
                        //    console.log(respond);
                    }
                })
}
});
        // custom untuk input

        $("#kabupaten").change(function () {
            var value = $(this).val();
//            console.log(value);
if (value > 0) {
    $.ajax({
        beforeSend: function () {
            $("#loadingImg").fadeIn();
        },
        data: {modul: 'kecamatan', kode: value},
        success: function (respond) {
            $("#loadingImg").fadeOut();
            $("#kecamatan").html(respond);
            $('#kecamatan').val('').trigger('change');
            $("#desa").html("");
            $('#desa').val('').trigger('change');
            $("#kelompok").html("");
            $('#kelompok').val('').trigger('change');
            $("#ketua").html("");
                        //    console.log(respond);
                    }
                })
}
});


        $("#kecamatan").change(function () {
            var value = $(this).val();
//            console.log(value);
$.ajax({
    beforeSend: function () {
        $("#loadingImg").fadeIn();
    },
    data: {modul: 'desa', kecamatan: value},
    success: function (respond) {
        $("#desa").html(respond);
        $('#desa').val('').trigger('change');
        $("#kelompok").html("");
        $('#kelompok').val('').trigger('change');
        $("#ketua").html("");
        $("#loadingImg").fadeOut();
                    //    console.log(respond);
                }
            })
});

        $("#desa").change(function () {
            var value = $(this).val();
//            console.log(value);
$.ajax({
    beforeSend: function () {
        $("#loadingImg").fadeIn();
    },
    data: {modul: 'kelompok', desa: value},
    success: function (respond) {
        $("#kelompok").html(respond)
        $('#kelompok').val('').trigger('change');
        $("#ketua").html("");
        $("#loadingImg").fadeOut();
                    //    console.log(respond);
                }
            })
});

        $("#kelompok").change(function () {
            var value = $(this).val();
            $.ajax({
                beforeSend: function () {
                    $("#loadingImg").fadeIn();
                },
                data: {modul: 'ketua', id_petani: value},
                success: function (respond) {
                    $("#ketua").val(respond);
                    $("#loadingImg").fadeOut();
                    console.log(respond);
                }
            })
        });

        $("#komoditi").change(function () {
            var value = $(this).val();
//            console.log(value);
$.ajax({
    beforeSend: function () {
        $("#loadingImg5").fadeIn();
    },
    data: {modul: 'sub_kegiatan', nama: value},
    success: function (respond) {
        $("#sub_kegiatan").html(respond);
        $('#sub_kegiatan').val('').trigger('change');
        $("#jenis_bantuan").html("");
        $('#jenis_bantuan').val('').trigger('change');
        $('tr.dd-item').remove();
        $("#loadingImg5").fadeOut();
                    //    console.log(respond);
                }
            })
});

        $("#sub_kegiatan").change(function () {
            var value = $(this).val();
//            console.log(value);
$.ajax({
    beforeSend: function () {
        $("#loadingImg5").fadeIn();
    },
    data: {modul: 'kegiatan', jenis_kegiatan: value},
    success: function (respond) {
        $("#jenis_bantuan").html(respond);
        $('#jenis_bantuan').val('').trigger('change');
        $('tr.dd-item').remove();
        $("#loadingImg5").fadeOut();
//                    console.log(respond);
}
})
});
    })

$('#addBantuan').click(function () {
    var idJenisBantuan = $('#jenis_bantuan :selected').val();
    var jenisBantuan = $('#jenis_bantuan :selected').text();

    if (typeof idJenisBantuan == 'undefined') {
        toastr.error('Mohon pilih Jenis Bantuan', 'Warning', {timeOut: 5000}, toastr.options = {
            "closeButton": true});
    } else {
        if ($('tr.dd-item').hasClass('dd-item-' + idJenisBantuan)) {
            toastr.error('Jenis Bantuan sudah di tambahkan', 'Warning', {timeOut: 5000}, toastr.options = {
                "closeButton": true});
        } else {
            $.ajax({
                method: "POST",
                url: "<?php echo site_url('Opd/Pengajuan_proposal/getDataJenisBantuan'); ?>",
                data: "idJenisBantuan=" + idJenisBantuan
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                $('tbody.outer').append('<tr id="tr_' + idJenisBantuan + '" role="row" class="odd dd-item dd-item-' + idJenisBantuan + '" data-id="' + idJenisBantuan + '"><td><input type="hidden" name="id_jenis_bantuan[]" id="id_jenis_bantuan_' + idJenisBantuan + '" value="' + idJenisBantuan + '"><input type="hidden" name="jenis_bantuan[]" id="jenis_bantuan_' + idJenisBantuan + '" value="' + jenisBantuan + '">' + jenisBantuan + '</td><td><input style="width: 50%;" type="text" class="number_decimal" name="volume[]" id="volume_' + idJenisBantuan + '"></td><td><input type="hidden" name="satuan[]" id="satuan_' + idJenisBantuan + '" value="' + result.satuan + '">' + result.satuan + '</td><td><input style="width: 85%;" type="text" name="keterangan[]" id="keterangan_' + idJenisBantuan + '"></td><td><span tooltip=" Hapus"><button type="button" onclick="deleteJenisBantuan(' + idJenisBantuan + ')" class="mini negative ui positive button"><i class="fa fa-trash - o"></i></button></span></td></tr>');
                $('tbody.outer-program').append('<tr id="tr_' + idJenisBantuan + '" role="row" class="odd dd-item dd-item-' + idJenisBantuan + '" data-id="' + idJenisBantuan + '"><td><input type="hidden" name="program[]" id="program_' + idJenisBantuan + '" value="' + result.program + '">' + result.program + '</td></tr>');
                $('tbody.outer-kegiatan').append('<tr id="tr_' + idJenisBantuan + '" role="row" class="odd dd-item dd-item-' + idJenisBantuan + '" data-id="' + idJenisBantuan + '"><td><input type="hidden" name="jenis_kegiatan[]" id="jenis_kegiatan_' + idJenisBantuan + '" value="' + result.jenis_kegiatan + '">' + result.jenis_kegiatan + '</td></tr>');
                $('#jenis_bantuan').val('').trigger('change');
            })
        }
    }
});

function deleteJenisBantuan(idJenisBantuan) {
    $('.dd-item-' + idJenisBantuan).remove();
}

$('#simpan').click(function () {
    var error = 0;
    var message = "";

    var data = $(this).serialize();

    var kabupaten = $("#kabupaten").val();
    if (error == 0) {
        if (kabupaten.length == 0) {
            error++;
            message = "Kabupaten wajib di isi.";
        }
    }

    var kelompok = $("#kelompok").val();
    if (error == 0) {
        if (kelompok.length == 0) {
            error++;
            message = " Kelompok Tani wajib di isi.";
        }
    }

    var ketua = $("#ketua").val();
    if (error == 0) {
        if (ketua.length == 0) {
            error++;
            message = " Ketua kelompok tani wajib di isi.";
        }
    }

    var komoditi = $("#komoditi").val();
    if (error == 0) {
        if (komoditi.length == 0) {
            error++;
            message = "Komoditi wajib di isi.";
        }
    }

    var sub_kegiatan = $("#sub_kegiatan").val();
    if (error == 0) {
        if (sub_kegiatan.length == 0) {
            error++;
            message = "Sub kegiatan wajib di isi.";
        }
    }

    if (error == 0) {
        var BreakException = {};
        try {
            var jenisBantuanListComponent = $('#dd_list tr');
            jenisBantuanListComponent.each(function () {
                var idJenisBantuan = $(this).attr('data-id');
                var idJenisBantuan = idJenisBantuan.trim();
                if (error == 0) {
                    if (idJenisBantuan.length == 0) {
                        error++;
                        message = "Jenis Bantuan wajib di isi.";
                    }
                }

                var volume = $("#volume_" + idJenisBantuan).val();
                var volume = volume.trim();
                if (error == 0) {
                    if (volume.length == 0) {
                        error++;
                        message = "Volume wajib di isi.";
                    }
                }

                    // var prioritas = $("#prioritas_" + idJenisBantuan).val();
                    // var prioritas = prioritas.trim();
                    // if (error == 0) {
                    //     if (prioritas.length == 0) {
                    //         error++;
                    //         message = "Prioritas wajib di isi.";
                    //     }
                    // }

                    if (error > 0) {
                        throw BreakException;
                    }
                });
        } catch (e) {
            if (e !== BreakException)
                throw e;
        }
    }

    if (error == 0) {
        swal({
            title: "Simpan Data?",
            text: "Apakah Anda yakin ingin mengajukan Proposal ini?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Simpan",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        },
        function () {
            $(".confirm").attr('disabled', 'disabled');
            var data = $("#form-tambah").serialize();
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $("#loadingImgSpecial").show();
                },
                url: '<?php echo base_url('Opd/Pengajuan_proposal/prosesAdd'); ?>',
                data: data,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == true) {
                    document.getElementById("form-tambah").reset();
                    $("#loadingImgSpecial").hide();
                    setTimeout("window.location='<?php echo site_url('data-proposal'); ?>'", 450);
                    swal("Success", result.pesan, "success");
                } else {
                    $("#loadingImgSpecial").hide();
                    swal("Warning", result.pesan, "warning");
                }
                console.log(result);
            })
        });
    } else {
        toastr.error(message, 'Warning', {timeOut: 5000}, toastr.options = {
            "closeButton": true});
    }
});
</script>