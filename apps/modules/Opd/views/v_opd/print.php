
<?php
$qKomoditi = "SELECT * FROM tbl_komoditi WHERE id_komoditi = '{$brand->id_komoditi}'";
$resKomoditi = $this->db->query($qKomoditi)->row();

$dataKelompok = $this->Mdl_petani->selectById($brand->id_petani);

$qSubKegiatan = "SELECT * FROM tbl_sub_kegiatan WHERE id = '{$brand->id_sub_kegiatan}'";
$resSubKegiatan = $this->db->query($qSubKegiatan)->row();

$sqlProposalDetail = "SELECT * FROM tbl_proposal_detail WHERE id_proposal = '{$brand->id_proposal}'";
$dataProposalDetail = $this->db->query($sqlProposalDetail)->result();

$jenis_bantuan = "<ul>";
foreach ($dataProposalDetail as $key => $value) {
    $qJenisBantuan = "SELECT * FROM tbl_jenis_bantuan WHERE id = '{$value->id_jenis_bantuan}'";
    $resJenisBantuan = $this->db->query($qJenisBantuan)->row();
    $jenis_bantuan .= "<li class='jarak'>" . $resJenisBantuan->jenis_bantuan . "</li>";
}
$jenis_bantuan .= "</ul>";

$satuan = "<ul>";
foreach ($dataProposalDetail as $key => $value) {
    $satuan .= "<li class='jarak'>" . $value->satuan . "</li>";
}
$satuan .= "</ul>";

$jumlah = "<ul>";
foreach ($dataProposalDetail as $key => $value) {
    $jumlah .= "<li class='jarak'>" . $value->jumlah . " " .$value->satuan. "</li>";
}
$jumlah .= "</ul>";

$prioritas = "<ul>";
foreach ($dataProposalDetail as $key => $value) {
    $prioritas .= "<li class='jarak'>" . $value->prioritas . "</li>";
}
$prioritas .= "</ul>";

$program = "<ul>";
foreach ($dataProposalDetail as $key => $value) {
    $program .= "<li class='jarak'>" . $value->program . "</li>";
}
$program .= "</ul>";

$kegiatan = "<ul>";
foreach ($dataProposalDetail as $key => $value) {
    $kegiatan .= "<li class='jarak'>" . $value->kegiatan . "</li>";
}
$kegiatan .= "</ul>";

$catatan .= "<ul>";
foreach ($dataProposalDetail as $key => $value) {
    $catatan .= "<li>" . $value->catatan . "</li>";
}
$catatan .= "</ul>";

if ($brand->status_dpa == '1') {
    $status_dpa = 'MASUK DPA';
} else {
    $status_dpa = 'BELUM MASUK DPA';
}

?>

<style type="text/css">
    .jarak{
        margin-left: -30px;
    }
</style>

<html>
    <table border='0' cellspacing='0' cellpading='1' class='tampil' style='font-family: Futura, "Trebuchet MS", Arial, sans-serif; font-size: 24px; font-style: normal;'>
        <tr>
            <td id='kepala' width='700' align='center'>
                <table width='100%' border='0'>
                    <tr>
                        <td width='50'><img src='<?php echo base_url() ?>assets/tambahan/gambar/logo-icon.png'width='60' height='80' align='left'></td>
                        <td align='center'>
                            <font size='4'><b>
                                Dinas Perkebunan Provinsi Jawa Timur</b></font><br>
                            <font size='2'>Jl. Gayung Kebonsari No.171, Gayungan, Kec. Gayungan, Kota SBY, Jawa Timur 60235 <br></font>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <hr style='border: double'>
            </td>
        </tr>
    </table>
    <body>
        <table border='0' cellpadding='3' cellspacing='1' width='700' style='font-family: "time news roman"; font-size: 16px; background: #ffffff;'>

            <tr>
                <td colspan='3' align='center' bgcolor="#0099CC">Rekap Form Pengajuan Proposal</td>
            </tr>
            <tr>
                <td width='20'><br></td>
                <td>
                    <table style='font-family: "time news roman"; font-size: 14px;' border='0' cellpadding='3' cellspacing='8' width='100%'>
                        <tr valign="top">
                            <td colspan='3'><b>Data Pengajuan :</b></td>
                            <td></td>
                        </tr>
                        <tr valign="top">
                            <td>Nama Kelompok</td><td width='7'>:</td><td><?php echo $dataKelompok->nama_kelompok; ?></td>
                            <td width='150' rowspan='10' valign='top'>
                                <table border='1' cellpadding='2' cellspacing='0'>
                                    <tr>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr valign="top">
                            <td>Nama Ketua</td><td width='7'>:</td><td><?php echo $dataKelompok->nama_ketua; ?></td>
                        </tr>
                        <tr valign="top">
                            <td>Desa</td><td width='7'>:</td><td><?php echo $brand->desa; ?></td>
                        </tr>
                        <tr valign="top">
                            <td>Kecamatan</td><td width='7'>:</td><td><?php echo $brand->kecamatan; ?></td>
                        </tr>
                        <tr valign="top">
                            <td>Kabupaten</td><td width='7'>:</td><td><?php echo $brand->kabupaten; ?></td>
                        </tr>
                        <tr valign="top">
                            <td>Komoditi</td><td width='7'>:</td><td><?php echo $resKomoditi->nama; ?></td>
                        </tr>
                        <tr valign="top">
                            <td>Sub Kegiatan</td><td width='7'>:</td><td><?php echo $resSubKegiatan->jenis_kegiatan; ?></td>
                        </tr>
                        <tr valign="top">
                            <td>Jenis Bantuan</td><td width='7'>:</td><td><?php echo $jenis_bantuan; ?></td>
                        </tr>
                        <tr valign="top">
                            <td>Volume</td><td width='7'>:</td><td><?php echo $jumlah; ?> </td>
                        </tr>


                        <tr valign="top">
                            <td>Kegiatan</td><td width='7'>:</td><td><?php echo $kegiatan; ?></td>
                        </tr>
                        <tr valign="top">
                            <td>Program</td><td width='7'>:</td><td><?php echo $program; ?></td>
                        </tr>
                        <tr valign="top">
                            <td>Status Proposal</td><td width='7'>:</td><td><b><?php echo $brand->status; ?></b></td>
                        </tr>
                        <tr>
                            <td colspan='4'>
                                <div align="">
                                    <h3><br><strong><?php echo $status_dpa; ?></strong></h3>
                                </div>
                            </td>
                        </tr>

                    </table>
                </td>
                <td width='20'></td>
            </tr>
        </table>   
    </body>

</html>
<script>
window.print();
</script>