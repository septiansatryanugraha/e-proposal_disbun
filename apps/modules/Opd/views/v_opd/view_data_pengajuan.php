<style>

#loadingImg{
    margin-top: -3px;
    position: absolute;
    margin-left: 120px;
}

 .field-icon {
  float: left;
  margin-left: 91%;
  margin-top: -27px;
  position: relative;
  z-index: 2;
}

</style>

<?php
$dataKelompok = $this->Mdl_petani->selectById($brand->id_petani);
$qKomoditi = "SELECT * FROM tbl_komoditi WHERE id_komoditi = '{$brand->id_komoditi}'";
$resKomoditi = $this->db->query($qKomoditi)->row();

?>


<div class="twelve wide column except">
<h3 class="ui dividing header"><?=$title?></h3>


<form class="ui large form" id="form-tambah" method="POST">
<div class="ui stacked segment">

<div class="two fields">
<div class="field">
<label>Nama Kelompok</label>
<div class="field">
<div class="ui info message">
<?php echo $dataKelompok->nama_kelompok; ?>
</div>
</div>
</div>

<div class="field">
<label>Ketua Kelompok</label>
<div class="field">
<div class="ui info message">
<?php echo $dataKelompok->nama_ketua; ?>
</div>
</div>
</div>
</div>

<div class="three fields">

<div class="field">
<label>Kabupaten</label>
<div class="field">
<div class="ui info message">
<?php echo $brand->kabupaten; ?>
</div>
</div>
</div>

<div class="field">
<label>Kecamatan</label>
<div class="field">
<div class="ui info message">
<?php echo $brand->kecamatan; ?>
</div>
</div>
</div>

<div class="field">
<label>Desa</label>
<div class="field">
<div class="ui info message">
<?php echo $brand->desa; ?>
</div>
</div>
</div>

</div>

<div class="field">
<label>Komoditi</label>
<div class="seven wide column field">
<div class="ui info message">
<?php echo $resKomoditi->nama; ?>
</div>
</div>
</div>

<div class="field">
<div class="kiri">
                                <table id="example" class="table" style="width: 90%;" role="grid" aria-describedby="example_info">
                                    <thead>
                                        <tr role="row">
                                            <th tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 30%;">Jenis Bantuan</th>
                                            <th tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 15%;">Volume</th>
                                            <th tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 10%;">Satuan</th>
                                            <th tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 35%;">Keterangan</th>
                                        </tr>   
                                    </thead>
                                    <tbody id="dd_list" class="dd-list outer">
                                        <?php foreach ($detail as $key => $value) { 
                                            $JenisBantuan = "SELECT * FROM tbl_jenis_bantuan WHERE id = '{$value->jenis_bantuan}'";
                                     $resJenisBantuan = $this->db->query($JenisBantuan)->row();

                                            ?>
                                            <tr role="row" class="odd dd-item">
                                                <td><?php echo $resJenisBantuan->jenis_bantuan; ?></td>
                                                <td><center><?php echo $value->jumlah; ?></center></td>
                                                <td><center><?php echo $value->satuan; ?></center></td>
                                                <td><center><?php echo $value->catatan; ?></center></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                            <div class="field">
                            <div class="kiri">
                                <table id="example" class="ui celled table dataTable no-footer" style="width: 80%;" role="grid" aria-describedby="example_info">
                                    <thead>
                                        <tr role="row">
                                            <th tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 100%;">Program</th>
                                        </tr>   
                                    </thead>
                                    <tbody id="dd_list_program" class="dd-list-program outer-program">
                                        <?php foreach ($detail as $key => $value) { ?>
                                            <tr role="row" class="odd dd-item">
                                                <td><?php echo $value->program; ?></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="field">
                            <div class="kiri">
                                <table id="example" class="ui celled table dataTable no-footer" style="width: 80%;" role="grid" aria-describedby="example_info">
                                    <thead>
                                        <tr role="row">
                                            <th tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 100%;">Kegiatan</th>
                                        </tr>   
                                    </thead>
                                    <tbody id="dd_list_kegiatan" class="dd-list-kegiatan outer-kegiatan">
                                        <?php foreach ($detail as $key => $value) { ?>
                                            <tr role="row" class="odd dd-item">
                                                <td><?php echo $value->kegiatan; ?></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>





</div>
</form>
</div>
</div>
</div>
</div>





