<style>

    #loadingImgSpecial{
        margin-top: -3px;
        position: absolute;
        margin-left: 120px;
    }

    .font-loading
    {
        font-family: Futura,Trebuchet MS,Arial,sans-serif; 
        color:red;
        font-size: 14px;
    }

</style>


<div class="twelve wide column except">
    <h3 class="ui dividing header"><?= $title ?></h3>


    <form class="ui large form" id="form-tambah" method="POST">
        <input type="hidden" name="id_user" value="<?= $sessid; ?>">
        <input type="hidden" value="<?= isset($_POST['folder']) ? $_POST['folder'] : time() ?>" name="folder">

        <div class="ui stacked segment">

            <div class="field">
                <div class="nine wide column field">
                    <label>Nama Kelompok</label>
                    <input type="text" name="nama_kelompok" id="nama_kelompok" placeholder="Nama kelompok" max-length="35" autocomplete="off">
                </div>  
            </div>

            <div class="field">
                <div class="nine wide column field">
                    <label>Nama Ketua Kelompok</label>
                    <input type="text" name="nama_ketua" id="nama_ketua" placeholder="Nama ketua kelompok" max-length="35" autocomplete="off">
                </div>  
            </div>


            <div class="field ">
                <div class="six wide column field">
                    <label>Kabupaten</label>
                    <select class="kab" id="kabupaten">
                        <option value=""></option>
                        <option value="<?php echo $this->session->userdata('kode_kabupaten'); ?>"><?php echo $this->session->userdata('kabupaten'); ?></option>
                    </select>
                </div>
                <div id="loadingImg">
                    <img src="<?php echo base_url() . 'assets/' ?>publik/img/images/loading-bubble.gif">
                    <p class="font-loading">Proccessing Data</font></p>
                </div>
            </div>

            <input type="hidden" class="form-control"  name="kabupaten" id="nama_kab" aria-describedby="sizing-addon2">

            <div class="field ">
                <div class="six wide column field">
                    <label>Kecamatan</label>
                    <select name="kecamatan" id="kecamatan" class="kec">

                    </select>
                </div>
                <div id="loadingImg2">
                    <img src="<?php echo base_url() . 'assets/' ?>publik/img/images/loading-bubble.gif">
                    <p class="font-loading">Proccessing Data</font></p>
                </div>
            </div>

            <div class="field ">
                <div class="six wide column field">
                    <label>Desa</label>
                    <select name="desa" id="desa" class="desa">
                    </select>
                </div>
            </div>

            <div class="ui info message">
                <div class="content">
                    <div class="header">Kelengkapan Data</div>
                    <p>Kelengkapan yang harus di lampirkan</p>
                </div><br>

                <div class="field">
                    <label>Domisili </label>
                    <input type="file" name="others[]" id="files" multiple onchange="return valid_file()"/>
                    <div class="ui red left pointing label">Max: 500 kb. Ekstensi .pdf</div>
                </div>

                <div class="field">
                    <label>Keterangan Pengesahan Bupati / BHI</label>
                    <input type="file" name="others[]" id="files2" multiple onchange="return valid_file2()"/>
                    <div class="ui red left pointing label">Max: 500 kb. Ekstensi .pdf</div>
                </div>

                <div class="field">
                    <label>Susunan Pengurus / Organisasi</label>
                    <input type="file" name="others[]" id="files3" multiple onchange="return valid_file3()"/>
                    <div class="ui red left pointing label">Max: 500 kb. Ekstensi .pdf</div>
                </div>
            </div>


            <div class="two fields">
                <div class="five wide column field">
                    <label>Latitude</label>
                    <div class="ui left icon input">
                        <i class="map marker alternate icon"></i>
                        <input type="text" name="latitude" placeholder="Latitude" id="latitude" autocomplete="off">
                    </div>
                </div>


                <div class="five wide column field">
                    <label>Longitude</label>
                    <div class="ui left icon input">
                        <i class="map marker alternate icon"></i>
                        <input type="text" name="longitude" placeholder="Longitude" id="longitude" autocomplete="off">
                    </div>
                </div>

            </div>
            <font class="text-peta" color="red"><a href="https://www.latlong.net/" target="blank">cek latitude & longitude</a></font>
            <br><br>

            <div class="field">
                <label>Foto Profil</label>
                <div id="slider">
                    <img class="img-thumbnail" src="<?php echo base_url(); ?>assets/tambahan/gambar/tidak-ada.png" alt="your image" width="150px" />
                </div>
                <br>
                <input type="file"  name="gambar" id="gambar" onchange="return fileValidation()"/>
            </div>


            <div class="ui info message">
                <div class="content">
                    <div class="header">Penting !! </div>
                    <p>Pastikan Permohonan Rekomendasi sudah di approve oleh Admin, Cek ulang data anda sebelum mengajukan Permohonan Sertifikasi </p>
                </div>
            </div>


            <div id="loadingImgSpecial">
                <img src="<?php echo base_url() . 'assets/' ?>tambahan/gambar/loader-muter.gif" width="45px">
            </div>

            <button name="simpan" type="submit" class="ui large teal submit button"><i class="fa fa-save"></i> Simpan</button>

        </div>
    </form>
</div>
</div>
</div>

<script type="text/javascript">
    //Proses Controller logic ajax

    $(document).ready(function () {
        $("#loadingImgSpecial").hide();
    });



    $('#form-tambah').submit(function (e) {

        var error = 0;
        var message = "";

        var data = $(this).serialize();

        var nama_kelompok = $("#nama_kelompok").val();
        var nama_kelompok = nama_kelompok.trim();

        if (error == 0) {
            if (nama_kelompok.length == 0) {
                error++;
                message = "Nama Kelompok wajib di isi.";
            }
        }

        var nama_ketua = $("#nama_ketua").val();
        var nama_ketua = nama_ketua.trim();

        if (error == 0) {
            if (nama_ketua.length == 0) {
                error++;
                message = "Nama Kelompok wajib di isi.";
            }
        }


        var nama_kab = $("#kabupaten").val();
        var nama_kab = nama_kab.trim();

        if (error == 0) {
            if (nama_kab.length == 0) {
                error++;
                message = "Kabupaten wajib di isi.";
            }
        }

        var files = $("#files").val();
        var files = files.trim();

        if (error == 0) {
            if (files.length == 0) {
                error++;
                message = "Surat domisili wajib di isi.";
            }
        }

        var files2 = $("#files2").val();
        var files2 = files2.trim();

        if (error == 0) {
            if (files2.length == 0) {
                error++;
                message = "Surat keterangan pengesahan Bupati / BHI wajib di isi.";
            }
        }


        var files3 = $("#files3").val();
        var files3 = files3.trim();

        if (error == 0) {
            if (files3.length == 0) {
                error++;
                message = "Surat susunan pengurus / organisasi .";
            }
        }



        if (error == 0) {
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $("#loadingImgSpecial").show();

                },
                url: '<?php echo base_url('Opd/Petani/prosesAdd'); ?>',
                type: "post",
                data: new FormData(this),
                processData: false,
                contentType: false,
                cache: false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == true) {
                    document.getElementById("form-tambah").reset();
                    $("#loadingImgSpecial").hide();
                    swal("Success", result.pesan, "success");
                } else {
                    $("#loadingImgSpecial").hide();
                    swal("Warning", result.pesan, "warning");
                }
            })
            e.preventDefault();
        } else {
            toastr.error(message, 'Warning', {timeOut: 5000}, toastr.options = {
                "closeButton": true});
            return false;

        }
    });

    $(document).ready(function () {
        $(".kab").select2({
            placeholder: " -- Pilih Kabupaten -- "
        });
    });

    $(document).ready(function () {
        $(".kec").select2({
            // placeholder: " -- Pilih Kabupaten -- "
        });
    });

    $(document).ready(function () {
        $(".desa").select2({
            // placeholder: " -- Pilih Kabupaten -- "
        });
    });

</script>


<script type="text/javascript">
    $(function () {
        $("#loadingImg").hide();
        $("#loadingImg2").hide();

        $.ajaxSetup({
            type: "POST",
            url: "<?php echo base_url('Opd/Petani/ambil_data') ?>",
            cache: false,
        });


        // custom untuk input

        $("#kabupaten").change(function () {
            var value = $(this).val();
            console.log(value);
            if (value > 0) {
                $.ajax({
                    beforeSend: function () {
                        $("#loadingImg").fadeIn();
                    },
                    data: {modul: 'kabupaten', kode: value},
                    success: function (respond) {
                        $("#nama_kab").val(respond);
                        $("#loadingImg").fadeOut();
                        console.log(respond);
                    }
                })
            }
        });

        // custom untuk input

        $("#kabupaten").change(function () {
            var value = $(this).val();
            console.log(value);
            if (value > 0) {
                $.ajax({
                    beforeSend: function () {
                        $("#loadingImg").fadeIn();
                    },
                    data: {modul: 'kecamatan', kode: value},
                    success: function (respond) {
                        $("#kecamatan").html(respond);
                        $("#loadingImg").fadeOut();
                        console.log(respond);
                    }
                })
            }
        });


        $("#kecamatan").change(function () {
            var value = $(this).val();
            console.log(value);
            $.ajax({
                beforeSend: function () {
                    $("#loadingImg2").fadeIn();
                },
                data: {modul: 'desa', kecamatan: value},
                success: function (respond) {
                    $("#desa").html(respond);
                    $("#loadingImg2").fadeOut();
                    console.log(respond);
                }
            })
        });


    })
</script>




