<style>

#loadingImgSpecial{
    margin-top: -3px;
    position: absolute;
    margin-left: 120px;
}

 .field-icon {
  float: left;
  margin-left: 91%;
  margin-top: -27px;
  position: relative;
  z-index: 2;
}

.font-loading
{
  font-family: Futura,Trebuchet MS,Arial,sans-serif; 
  color:red;
  font-size: 14px;
}

/* The container */
.container2 {
  display: block;
  position: relative;
  padding-left: 35px;
  margin-bottom: 12px;
  cursor: pointer;
  font-size: 22px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

/* Hide the browser's default checkbox */
.container2 input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
}

/* Create a custom checkbox */
.checkmark {
  position: absolute;
  top: 0;
  left: 0;
  height: 20px;
  width: 20px;
  border-radius: 3px;
  background-color: #d4d2d2;
}

/* On mouse-over, add a grey background color */
.container:hover input ~ .checkmark {
  background-color: #ccc;
}

/* When the checkbox is checked, add a blue background */
.container2 input:checked ~ .checkmark {
  background-color: #2196F3;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the checkmark when checked */
.container2 input:checked ~ .checkmark:after {
  display: block;
}

/* Style the checkmark/indicator */
.container2 .checkmark:after {
  left: 7px;
  top: 4px;
  width: 5px;
  height: 11px;
  border: solid white;
  border-width: 0 3px 3px 0;
  -webkit-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  transform: rotate(45deg);
}



</style>





<div class="twelve wide column except">
<h3 class="ui dividing header"><?=$title?></h3>


<form class="ui large form" id="form-tambah" method="POST" enctype="multipart/form-data">
<input type="hidden" name="id_user" value="<?= $sessid; ?>">
<input type="hidden" name="id_petani" value="<?php echo $petani->id_petani; ?>">
<input type="hidden" value="<?= isset($petani->folder) ? $petani->folder : time() ?>" name="folder">
<div class="ui stacked segment">


<div class="field">
<div class="nine wide column field">
<label>Nama Kelompok</label>
<input type="text" name="nama_kelompok" id="nama_kelompok" placeholder="Nama kelompok" value="<?php echo $petani->nama_kelompok; ?>">
</div>  
</div>

<div class="field">
<div class="nine wide column field">
<label>Nama Ketua Kelompok</label>
<input type="text" name="nama_ketua" id="nama_ketua" placeholder="Nama ketua kelompok" value="<?php echo $petani->nama_ketua; ?>">
</div>  
</div>

<div class="field ">
<div class="six wide column field">
<label>Kabupaten</label>
<select class="kab" id="kabupaten">
<option></option>
<option value="<?php echo $this->session->userdata('kode_kabupaten'); ?>"><?php echo $this->session->userdata('kabupaten'); ?></option>
</select>
</div>
<div id="loadingImg">
<img src="<?php echo base_url().'assets/' ?>publik/img/images/loading-bubble.gif">
<p class="font-loading">Proccessing Data</font></p>
</div>
</div>

<input type="hidden" class="form-control"  name="kabupaten" id="nama_kab" value="<?php echo $petani->kabupaten; ?>">

     <div class="field ">
     <div class="six wide column field">
     <label>Kecamatan</label>
     <select name="kecamatan" id="kecamatan" class="kec">
      <?php foreach ($kecamatan as $data) { ?>
        <option value="<?php echo $data->kecamatan; ?>"<?php if($data->kecamatan == $petani->kecamatan){echo "selected='selected'";} ?>><?php echo $data->kecamatan; ?>
        </option>
        <?php } ?>
    
     </select>
     </div>
     <div id="loadingImg2">
     <img src="<?php echo base_url().'assets/' ?>publik/img/images/loading-bubble.gif">
     <p class="font-loading">Proccessing Data</font></p>
     </div>
     </div>

     <div class="field ">
     <div class="six wide column field">
     <label>Desa</label>
     <select name="desa" id="desa" class="desa">
      <?php foreach ($desa as $data) { ?>
     <option value="<?php echo $data->desa; ?>"<?php if($data->desa == $petani->desa){echo "selected='selected'";} ?>><?php echo $data->desa; ?>
     </option>
     <?php } ?>
     </select>
     </div>
     </div>


     <div class="two fields">
        <div class="five wide column field">
        <label>Latitude</label>
        <div class="ui left icon input">
        <i class="map marker alternate icon"></i>
        <input type="text" name="latitude" placeholder="Latitude" id="latitude" value="<?php echo $petani->latitude; ?>">
        </div>
        </div>

     
        <div class="five wide column field">
        <label>Longitude</label>
        <div class="ui left icon input">
        <i class="map marker alternate icon"></i>
        <input type="text" name="longitude" placeholder="Longitude" id="longitude" value="<?php echo $petani->longitude; ?>">
        </div>
        </div>

        </div>
        <font class="text-peta" color="red"><a href="https://www.latlong.net/" target="blank">cek latitude & longitude</a></font>
        <br><br>

        <div class="field">
        <label>Foto Profil</label>
        <div id="slider">
        <?php if($petani->gambar != NULL) {  ?>
        <img class="img-thumbnail" src='../<?php echo $petani->gambar; ?>'>
        <?php } else { ?>
        <img class="img-thumbnail" src="<?php echo base_url();?>assets/tambahan/gambar/tidak-ada.png" alt="your image" width="150px" />
        <?php }  ?>
        </div><br>
        <input type="file"  name="gambar" id="gambar" onchange="return fileValidation()"/>
        </div>

        <div class="field ">
        <div class="six wide column field">
        <label>Status</label>
       <select name="status" id="status" class="ui dropdown">
       <?php foreach ($status as $data) { ?>
        <option value="<?php echo $data->nama; ?>"<?php if($data->nama == $petani->status){echo "selected='selected'";} ?>><?php echo $data->nama; ?>
        </option>
        <?php } ?>
    
       </select>
       </div>
       </div>


<div id="loadingImgSpecial">
<img src="<?php echo base_url().'assets/' ?>tambahan/gambar/loader-muter.gif" width="45px">
</div>

<button name="simpan" type="submit" class="ui large teal submit button"><i class="fa fa-save"></i> Update</button>
</div>
</form>
</div>
</div>
</div>
</div>


<script type="text/javascript">
    //Proses Controller logic ajax

 $(document).ready(function(){
  $("#loadingImgSpecial").hide();
  });


  $('#form-tambah').submit(function(e) {
      
        var error = 0;
        var message = "";

        var data = $(this).serialize();

        var nama_kelompok = $("#nama_kelompok").val();
        var nama_kelompok = nama_kelompok.trim();

          if (error == 0) {
            if (nama_kelompok.length == 0) {
                error++;
                message = "Nama Kelompok wajib di isi.";
            }
        }

        var nama_ketua = $("#nama_ketua").val();
        var nama_ketua = nama_ketua.trim();

          if (error == 0) {
            if (nama_ketua.length == 0) {
                error++;
                message = "Nama Kelompok wajib di isi.";
            }
        }

      
    if (error == 0) {
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                $("#loadingImgSpecial").show();
              
                },
                url: '<?php echo base_url('Opd/Petani/prosesUpdate'); ?>',
                type:"post",
                data:new FormData(this),
                processData:false,
                contentType:false,
                cache:false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == true) {
                    $("#loadingImgSpecial").hide();
                    swal("Success", result.pesan, "success");
                    setTimeout("window.location='<?php echo site_url('data-kelompok-petani'); ?>'", 450);
                } else {
                    $("#loadingImgSpecial").hide();
                    swal("Warning", result.pesan, "warning");
                }
            })
            e.preventDefault();
        } else {
            toastr.error(message,'Warning', {timeOut: 5000},toastr.options = {
             "closeButton": true}); 
            return false;

        }
    });

  $(document).ready(function() {
     $(".kab").select2({
        placeholder: " -- Pilih Kabupaten -- "
        });
  });

 $(document).ready(function() {
     $(".kec").select2({
        // placeholder: " -- Pilih Kabupaten -- "
        });
  });

 $(document).ready(function() {
     $(".desa").select2({
        // placeholder: " -- Pilih Kabupaten -- "
        });
  });

  </script>


<script type="text/javascript">
    $(function(){
    $("#loadingImg").hide();
    $("#loadingImg2").hide();

    $.ajaxSetup({
    type:"POST",
    url: "<?php echo base_url('Opd/Petani/ambil_data') ?>",
    cache: false,
    });

    // custom untuk input
    $("#kabupaten").change(function(){
    var value=$(this).val();
    console.log(value);
    if(value>0){
    $.ajax({
    beforeSend: function (){
    $("#loadingImg").fadeIn();
        },
    data:{modul:'kabupaten',kode:value},
    success: function(respond){
    $("#nama_kab").val(respond);
    $("#loadingImg").fadeOut();  
    console.log(respond);
    }
    })
    }
    });

    // custom untuk input
    $("#kabupaten").change(function(){
    var value=$(this).val();
    console.log(value);
    if(value>0){
    $.ajax({
    beforeSend: function (){
    $("#loadingImg").fadeIn();
        },
    data:{modul:'kecamatan',kode:value},
    success: function(respond){
    $("#kecamatan").html(respond);
    $("#loadingImg").fadeOut();  
    console.log(respond);
    }
    })
    }
    });
    
    
    $("#kecamatan").change(function(){
    var value=$(this).val();
    console.log(value);
    $.ajax({
    beforeSend: function (){
    $("#loadingImg2").fadeIn();
        },
    data:{modul:'desa',kecamatan:value},
    success: function(respond){
    $("#desa").html(respond);
    $("#loadingImg2").fadeOut();
    console.log(respond);
    }
    })
    });
    
    
    })
  </script>


<script type="text/javascript">
    
 $(function() {
    $('.pencet').hide(); 
    $("#check-all").click(function(){
        if($(this).is(":checked")) {
            $(".check-item").prop("checked", true);
            $('.pencet').fadeIn("slow");
        } else {
            $('.pencet').fadeOut("slow");
            $(".check-item").prop("checked", false);
        } 
    });
});
</script>


<script type="text/javascript">   
 $(function() {
    $('.pencet').hide(); 
    $(".check-item").click(function(){
        if($(this).is(":checked")) {
            $('.pencet').fadeIn("slow");
        } else {
            $('.pencet').fadeOut("slow");
        } 
    });
});
</script>