
<div class="twelve wide column except">
	<h3 class="ui dividing header"><?=$title?></h3>


		<div class="ui info message">
		<div class="content">
		<div class="header">Penting !! </div>
		<p><li>Silahkan pilih button <b>Pengajuan Proposal</b> ( untuk membuat pengajuan proposal baru ).</li>
		</p>
		</div>
		</div>


<div class="ui form">
  <div class="fields">
    <div class="field">
    	<?php if ($status_button->status=='Enabled') { ?>
      <a href="<?php echo site_url('add-pengajuan-proposal'); ?>"><button class="ui primary button" ><i class="fa fa-plus" aria-hidden="true"></i> Pengajuan Proposal</button></a><br><br>
  <?php } else { ?>
  <?php } ?>
    </div>
  </div>
</div>

 <table id="example" class="ui celled table" style="width:100%">
 <thead>
 <tr>
 <th>No</th>
 <th>Nama Kelompok</th>
 <th>Nama Ketua</th>
 <th>Kecamatan</th>
 <th>Desa</th>
 <th>Komoditi</th>
 <th width="160px">Status</th>
 <th width="110px">Action</th>
 
 </tr>	
 </thead>
 <tbody>
		
 </tbody>
 </table>
 </div>
 </div>
 </div>
 </div>




<script type="text/javascript">

 //untuk load data table ajax	
	
var save_method; //for save method string
var table;

$(document).ready(function() {

    //datatables
    table = $('#example').DataTable({ 
    "aLengthMenu": [[25, 50, 75, 100, -1], [25, 50, 75, 100, "All"]],
            "pageLength": 25,
            "processing": true, //Feature control the processing indicator.
            "order": [], //Initial no order.
             oLanguage: {
                "sProcessing": "<img src='<?php base_url(); ?>assets/tambahan/gambar/loading.gif' width='25px'>",
                "sLengthMenu": "_MENU_ &nbsp;&nbsp;Data Per Halaman",
                "sInfo": "Menampilkan _START_ s/d _END_ dari <b>_TOTAL_ data</b>",
                "sInfoFiltered": "(difilter dari _MAX_ total data)",
                "sEmptyTable": "Data tidak ada di server",
                "sInfoPostFix": "",
                "sSearch": "<i class='fa fa-search fa-fw'></i> Pencarian : ",
                "sPaginationType": "simple_numbers",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Pertama",
                    "sPrevious": "Sebelumnya",
                    "sNext": "Selanjutnya",
                    "sLast": "Terakhir"
                }
            },

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('Opd/Pengajuan_proposal/ajax_list')?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],

    });

});


</script>