<div class="ui black inverted vertical footer segment">
    <div class="ui center aligned container">
        <div class="ui inverted section divider"></div>
        <div class="ui horizontal inverted small divided link list">
            <a class="item" href="" target="_blank">&copy; Dinas Perkebunan Provinsi Jawa Timur <?php echo date("Y"); ?></a>
        </div>
    </div>
</div>


<script src="<?php echo base_url(); ?>assets/js/ajax.js"></script>
<script src="<?php echo base_url(); ?>assets/publik/js/jquery-3.3.1.js"></script>
<script src="<?php echo base_url(); ?>assets/publik/sm/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/publik/sm/semantic.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo base_url(); ?>assets/publik/sm/main.js"></script>
<script src="<?php echo base_url(); ?>assets/publik/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/publik/js/dataTables.semanticui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/toastr/toastr.js"></script>
<script type='text/javascript' src='<?php echo base_url(); ?>assets/publik/js/select2.js'></script>
<script src="<?php echo base_url(); ?>assets/publik/jquery-ui-1.12.1/jquery-ui.js"></script>
<script src="<?php echo base_url(); ?>assets/publik/jquery-ui-1.12.1/jquery-ui.min.js"></script>
<script type="text/javascript">

    //----- Number Format -----//
    $(document).on('keypress', '.number_only', function (event) {
        if ((event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
    $('.number_only').on('focusin', function () {
        var x = $(this).val();
        if (x == 0 || x.length == 0) {
            $(this).val("");
        }
    });
    $('.number_only').on('focusout', function () {
        var x = $(this).val();
        if (x == 0 || x.length == 0) {
            $(this).val(0);
        }
    });

    $(document).on('keypress', '.number_decimal', function (event) {
        if ((event.which != 44 && event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
    $('.number_decimal').on('focusin', function () {
        var x = $(this).val();
        if (x == 0 || x.length == 0) {
            $(this).val("");
        }
    });
    $('.number_decimal').on('focusout', function () {
        var x = $(this).val();
        var x_comma = x.split(',').length;
        if (x == 0 || x.length == 0 || x_comma > 2) {
            $(this).val(0);
        }
    });

    $("#filter_tahun").change(function () {
        $.ajax({
            url: '<?php echo site_url('Dashboard/Dashboard/changeYear'); ?>',
            method: 'POST',
            data: {tahun: $(this).val()},
            success: function (data) {
                var result = jQuery.parseJSON(data);
                setTimeout(location.reload.bind(location), 450);
            },
        });
    });
    $(function () {
        $(".selek-tahun").select2({
            placeholder: " -- Pilih Tahun -- "
        });
    });
</script>

</body>
</html>
