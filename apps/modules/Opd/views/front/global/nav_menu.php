<div class="ui fixed stackable menu">
    <div href="#" class="header item">
        <img class="image" width="400px" src="<?php echo base_url(); ?>assets/publik/img/images/logo-dinas.png">
    </div>
    <div class="right menu">
        <?php if ($sessid == ""): ?>
            <div class="item">

                <div class="content">
                    <a href="<?php echo base_url(); ?>" class="button">Login</a>
                </div>
            </div>
        <?php else: ?>
            <div style="padding-top: 25px;">
                <span>Filter Tahun :</span>
                <select name="filter_tahun" id="filter_tahun" class="selek-tahun" style="width: 100px;">
                    <?php for ($i = 2019; $i < 2031; $i++) { ?>
                        <option value="<?php echo $i ?>" <?php echo isset($_SESSION['tahun']) ? ($_SESSION['tahun'] == $i ? 'selected' : '') : ($i == date('Y') ? 'selected' : '' ) ?> ><?php echo $i ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="ui inline dropdown item" tabindex="0">
                <div class="text">
                    <img class="ui avatar image" width="400px" src="<?php echo base_url(); ?>assets/publik/img/web/no-image.jpg">
                    <?php echo $user->nama_lengkap; ?>
                </div>
                <i class="dropdown icon"></i>
                <div class="menu" tabindex="-1">
                  <!-- <div class="item"><?php echo anchor(site_url() . 'beranda.html', '<i class="fa fa-tachometer" aria-hidden="true"></i>&nbsp;&nbsp; Dashboard'); ?></div>
                  <div class="item"><?php echo anchor(base_url() . 'data-rekomendasi.html', ' <i class="fa fa-check-square-o" aria-hidden="true"></i>&nbsp;&nbsp; Rekomendasi'); ?></div> 
                  <div class="item"><?php echo anchor(base_url() . 'data-sertifikasi.html', '<i class="fa fa-file-text-o" aria-hidden="true"></i>&nbsp;&nbsp; Sertifikasi'); ?></div> 
                  <div class="item"><?php echo anchor(base_url() . 'data-opd.html', '<i class="fa fa-undo aria-hidden="true"></i>&nbsp;&nbsp; Proses'); ?></div> -->
                    <div class="item"><?php echo anchor(base_url() . 'profil-user.html', '<i class="fa fa-user aria-hidden="true"></i>&nbsp;&nbsp; Profil'); ?></div> 
                    <div class="item"><?php echo anchor(base_url() . 'Opd/Login_opd/logout', '<i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp;&nbsp; Logout'); ?></div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>