<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	<title><?php echo $title. " - Dinas Perkebunan";?></title>
		<link rel="icon" href="<?php echo base_url()?>/assets/publik/img/images/logo-icon.png">
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="../bower_components/html5shiv/dist/html5shiv.js"></script>
	<script src="../bower_components/respond/dest/respond.min.js"></script>
	<![endif]-->
 
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/publik/sm/matrix-style.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/eksternal/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/sweetalert/sweetalert.css">
  <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/publik/sm/apaja.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/publik/sm/dataTables.semanticui.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/publik/sm/semantic.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/publik/css/select2.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/toastr/toastr.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/publik/jquery-ui-1.12.1/jquery-ui.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/publik/jquery-ui-1.12.1/jquery-ui.theme.css">
  

  <style>

.dataTables_processing {
                position: absolute;
                top: 50%;
                left: 50%;
                width: 50%;
                height: 60px;
                margin-left: -50%;
                margin-top: -25px;
                padding-top: 0px;
                text-align: center;
                font-size: 1.2em;
                
            }

/* START TOOLTIP STYLES */
[tooltip] {
  position: relative; /* opinion 1 */
}

/* Applies to all tooltips */
[tooltip]::before,
[tooltip]::after {
  text-transform: none; /* opinion 2 */
  font-size: .9em; /* opinion 3 */
  line-height: 1;
  user-select: none;
  pointer-events: none;
  position: absolute;
  display: none;
  opacity: 0;
}
[tooltip]::before {
  content: '';
  border: 5px solid transparent; /* opinion 4 */
  z-index: 1001; /* absurdity 1 */
}
[tooltip]::after {
  content: attr(tooltip); /* magic! */
  
  /* most of the rest of this is opinion */
  font-family: Helvetica, sans-serif;
  text-align: center;
  
  /* 
    Let the content set the size of the tooltips 
    but this will also keep them from being obnoxious
    */
  min-width: 3em;
  max-width: 21em;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  padding: 1ch 1.5ch;
  border-radius: .3ch;
  box-shadow: 0 1em 2em -.5em rgba(0, 0, 0, 0.35);
  background: #333;
  color: #fff;
  z-index: 1000; /* absurdity 2 */
}

/* Make the tooltips respond to hover */
[tooltip]:hover::before,
[tooltip]:hover::after {
  display: block;
}

/* don't show empty tooltips */
[tooltip='']::before,
[tooltip='']::after {
  display: none !important;
}

/* FLOW: UP */
[tooltip]:not([flow])::before,
[tooltip][flow^="up"]::before {
  bottom: 150%;
  border-bottom-width: 0;
  border-top-color: #333;
}
[tooltip]:not([flow])::after,
[tooltip][flow^="up"]::after {
  bottom: calc(150% + 5px);
}
[tooltip]:not([flow])::before,
[tooltip]:not([flow])::after,
[tooltip][flow^="up"]::before,
[tooltip][flow^="up"]::after {
  left: 50%;
  transform: translate(-50%, -.5em);
}

/* KEYFRAMES */
@keyframes tooltips-vert {
  to {
    opacity: .9;
    transform: translate(-50%, 0);
  }
}

@keyframes tooltips-horz {
  to {
    opacity: .9;
    transform: translate(0, -50%);
  }
}

/* FX All The Things */ 
[tooltip]:not([flow]):hover::before,
[tooltip]:not([flow]):hover::after,
[tooltip][flow^="up"]:hover::before,
[tooltip][flow^="up"]:hover::after,
[tooltip][flow^="down"]:hover::before,
[tooltip][flow^="down"]:hover::after {
  animation: tooltips-vert 300ms ease-out forwards;
}

[tooltip][flow^="left"]:hover::before,
[tooltip][flow^="left"]:hover::after,
[tooltip][flow^="right"]:hover::before,
[tooltip][flow^="right"]:hover::after {
  animation: tooltips-horz 300ms ease-out forwards;
}

#LoadingDulu {
  position: fixed;
  top:0px;
  width: 100%;
  z-index: 99999;
}

#LoadingContent {
  height: 30px;
  margin-top: 20px;
  margin-left: 600px;
  width: 180px;
  background: #ff005e;
  text-align: center;
  line-height: 29px;
  font-weight: bold;
  color: #fff;
}


  </style>


<?php echo $script_captcha; // javascript recaptcha ?>
  <?php if(isset($style)) echo $style; ?>
</head>
<div id='LoadingDulu'></div>
<body>