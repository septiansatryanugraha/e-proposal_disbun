<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekap_proposal extends MX_Controller
{
    const __tableName = 'tbl_proposal';
    const __tableId = 'id_proposal';
    const __model = 'Mdl_rekap_proposal';

    function __construct()
    {
        parent::__construct();
        $this->load->model(self::__model);
        $this->load->model('Mdl_registrasi');
        $this->load->model('Mdl_petani');
    }

    public function index($param = '')
    {
        header('Location: ' . base_url() . '');
    }

    function home()
    {
        if ($this->lib->login() == "") {
            $this->session->set_flashdata('not_login', '<div class="ui success message"><i class="close icon"></i><div class="header">Silahkan login terlebih dahulu.</div></div>');
            redirect('login-pengajuan');
        } else {
            $nama_kabupaten = $this->session->userdata('kabupaten');

            $data = array(
                'title' => 'Rekap Data Pengajuan Proposal Kab ' . $nama_kabupaten . '',
                'sessid' => $this->session->userdata('id'),
                'style' => '<style type="text/css">.main.container, .ui.vertical.segment {margin-top: 7em;}.ui.menu .item img.logo {margin-right: 1em;}</style>'
            );
            $data['kab'] = $this->session->userdata('kabupaten');
            $data['status'] = $this->Mdl_rekap_proposal->selectStatus();

            $id_user = $this->session->userdata('id');
            $data['user'] = $this->Mdl_registrasi->select_by_id($id_user);

            $this->load->view('front/global/header', $data);
            $this->load->view('front/global/nav_menu', $data);
            $this->load->view('back/user/sidebar_dashboard');
            $this->load->view('v_rekap/v_rekap-data', $data);
            $this->load->view('front/global/footer', $data);
        }
    }

    function filter()
    {
        if ($this->lib->login() == "") {
            $this->session->set_flashdata('not_login', '<div class="ui success message"><i class="close icon"></i><div class="header">Silahkan login terlebih dahulu.</div></div>');
            redirect('login-pengajuan');
        } else {
            $nama_kabupaten = $this->session->userdata('kabupaten');

            $data = array(
                'title' => 'Rekap Data Pengajuan Proposal Kab ' . $nama_kabupaten . '',
                'sessid' => $this->session->userdata('id'),
                'style' => '<style type="text/css">.main.container, .ui.vertical.segment {margin-top: 7em;}.ui.menu .item img.logo {margin-right: 1em;}</style>'
            );
            $data['kab'] = $this->session->userdata('kabupaten');
            $data['status'] = $this->Mdl_rekap_proposal->selectStatus();

            $id_user = $this->session->userdata('id');
            $data['user'] = $this->Mdl_registrasi->select_by_id($id_user);

            $tanggal_awal = $this->input->post('tanggal_awal');
            $tanggal_akhir = $this->input->post('tanggal_akhir');
            $kab = $this->input->post('kab');
            $status = $this->input->post('status');

            $data['filter'] = $this->Mdl_rekap_proposal->export_data($tanggal_awal, $tanggal_akhir, $kab, $status);
            $data['tanggal_awal'] = $tanggal_awal;
            $data['tanggal_akhir'] = $tanggal_akhir;
            $data['data_kab'] = $kab;
            $data['data_status'] = $status;

            $id_user = $this->session->userdata('id');
            $data['user'] = $this->Mdl_registrasi->select_by_id($id_user);

            $this->load->view('front/global/header', $data);
            $this->load->view('front/global/nav_menu', $data);
            $this->load->view('back/user/sidebar_dashboard');
            $this->load->view('v_rekap/filter', $data);
            $this->load->view('front/global/footer', $data);
        }
    }

    public function export_excel()
    {

        $waktu = date("Y-m-d h:i");
        $nama_kabupaten = $this->session->userdata('kabupaten');

        $data['title'] = "Report Data Pengajuan Proposal Dinas Perkebunan Kab " . $nama_kabupaten . " " . $waktu . "";

        $tanggal_awal = $this->input->post('tanggal_awal');
        $tanggal_akhir = $this->input->post('tanggal_akhir');
        $kab = $this->input->post('kab');
        $status = $this->input->post('status');

        $data['excel'] = $this->Mdl_rekap_proposal->export_data($tanggal_awal, $tanggal_akhir, $kab, $status);
        $data['tanggal_awal'] = $tanggal_awal;
        $data['tanggal_akhir'] = $tanggal_akhir;
        $data['data_kab'] = $kab;

        $this->load->view('v_rekap/v_laporan_excel', $data);
    }
}
