<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MX_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Mdl_web');
        $this->load->model('Mdl_registrasi');
        $this->load->model('Mdl_rekap_proposal');
    }

    public function index()
    {

        $data = array(
            'bantuan' => $this->Mdl_web->bantuan(),
            'sessid' => $this->session->userdata('id'),
            'nama' => $this->session->userdata('nama_lengkap'),
            'title' => "Dashboard",
            'style' => '<style type="text/css">.main.container, .ui.vertical.segment {margin-top: 7em;}.ui.menu .item img.logo {margin-right: 1em;}</style>'
        );

        $id_user = $this->session->userdata('id');
        $data['user'] = $this->Mdl_registrasi->select_by_id($id_user);
        $data['log'] = $this->Mdl_rekap_proposal->log($id_user);
        
        $this->load->view('front/global/header', $data);
        $this->load->view('front/global/nav_menu', $data);
        $this->load->view('back/user/sidebar_dashboard');
        $this->load->view('dashboard', $data);
        $this->load->view('front/global/footer', $data);
    }
}
