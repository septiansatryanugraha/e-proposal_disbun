<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengajuan_proposal extends MX_Controller
{
    const __tableName = 'tbl_form_proposal';
    const __tableId = 'id_proposal';

    function __construct()
    {
        parent::__construct();
        $this->load->model('Mdl_proposal');
        $this->load->model('Mdl_registrasi');
        $this->load->model('Mdl_petani');
        $this->load->model('Mdl_petani');
    }

    public function index($param = '')
    {
        header('Location: ' . base_url() . '');
    }

    function ambil_data()
    {
        $respon = '';

        $modul = $this->input->post('modul');

        if ($modul == "kabupaten") {
            $id = $this->input->post('kode');
            if (strlen($id) > 0) {
                $respon = $this->Mdl_proposal->select_kabupaten($id);
            }
        }
        if ($modul == "kecamatan") {
            $id = $this->input->post('kode');
            if (strlen($id) > 0) {
                $respon = $this->Mdl_proposal->select_kecamatan($id);
            }
        } else if ($modul == "desa") {
            $id = $this->input->post('kecamatan');
            if (strlen($id) > 0) {
                $respon = $this->Mdl_proposal->select_desa($id);
            }
        } else if ($modul == "kelompok") {
            $id = $this->input->post('desa');
            if (strlen($id) > 0) {
                $respon = $this->Mdl_proposal->select_kelompok($id);
            }
        } else if ($modul == "ketua") {
            $id = $this->input->post('id_petani');
            if (strlen($id) > 0) {
                $respon = $this->Mdl_proposal->select_ketua($id);
            }
        } else if ($modul == "sub_kegiatan") {
            $id = $this->input->post('nama');
            if (strlen($id) > 0) {
                $respon = $this->Mdl_proposal->select_sub_kegiatan($id);
            }
        } else if ($modul == "kegiatan") {
            $id = $this->input->post('jenis_kegiatan');
            if (strlen($id) > 0) {
                $respon = $this->Mdl_proposal->select_jenis_bantuan($id);
            }
        }

        echo $respon;
        // else if($modul=="satuan"){
        // $id=$this->input->post('jenis_bantuan');
        // echo $this->Mdl_proposal->select_satuan($id);
        // }
        // else if($modul=="program"){
        // $id=$this->input->post('jenis_kegiatan');
        // echo $this->Mdl_proposal->select_jenis_program($id);
        // }
    }

    public function add_dokumen()
    {
        if ($this->lib->login() != "") {
            $this->load->module('template');
//            $this->load->model('Mdl_category', 'mdl_category');
            $nama_kabupaten = $this->session->userdata('kabupaten');
            $data = array(
                'title' => 'Pengajuan Proposal Kabupaten ' . $nama_kabupaten . '',
//                'category' => $this->mdl_category->get('category', 'ASC'),
                'kat_usaha' => $this->Mdl_proposal->selekKategori2(),
                'komoditi' => $this->Mdl_proposal->selekKomoditi(),
                'sub_kegiatan' => $this->Mdl_proposal->selekSubKegiatan(),
                'sessid' => $this->session->userdata('id'),
                'style' => '<style type="text/css">.main.container, .ui.vertical.segment {margin-top: 7em;}.ui.menu .item img.logo {margin-right: 1em;}</style>'
            );

            $id_user = $this->session->userdata('id');
            $data['user'] = $this->Mdl_registrasi->select_by_id($id_user);

            $this->load->view('front/global/header', $data);
            $this->load->view('front/global/nav_menu', $data);
            $this->load->view('back/user/sidebar_dashboard');
            $this->load->view('v_opd/tambah', $data);
            $this->load->view('front/global/footer', $data);
        } else {
            $this->session->set_flashdata('not_login', '<p style="color: red;">ilahkan login untuk upload dokumen pengajuan.</p>');
            redirect('login-pengajuan');
        }
    }

    public function ajax_list()
    {
        $id_user = $this->session->userdata('kabupaten');
        $list = $this->Mdl_proposal->get_where_custom('kabupaten', $id_user);

        $data = array();
        $no = $_POST['start'];
        foreach ($list->result() as $brand) {

            $idKelompok = $brand->id_petani;
            $dataKelompok = $this->Mdl_petani->selectById($idKelompok);

            $status = '<a class="ui blue label">Usulan Baru</a>';
            if ($brand->status == 'Rekomendasi') {
                $status = '<a class="ui red label">Telah Direkomendasi</a>';
            }

            $qKomoditi = "SELECT * FROM tbl_komoditi WHERE id_komoditi = '{$brand->id_komoditi}'";
            $resKomoditi = $this->db->query($qKomoditi)->row();

            if ($brand->status == 'Usulan Baru') {
                $Status = '<a class="ui red label">Belum Rekomendasi</a>';
            } else if ($brand->status == 'Verifikasi') {
                $Status = '<a class="ui yellow label">Verifikasi</a>';
            } else if ($brand->status == 'Rekom Bidang') {
                $Status = '<a class="ui blue label">Rekom Bidang</a>';
            } else if ($brand->status == 'Tidak Rekom') {
                $Status = '<a class="ui yellow label">Tidak Rekom</a>';
            } else if ($brand->status == 'Penetapan') {
                $Status = '<a class="ui green label">Penetapan</a>';
            }

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $dataKelompok->nama_kelompok;
            $row[] = $dataKelompok->nama_ketua;
            $row[] = $brand->kecamatan;
            $row[] = $brand->desa;
            $row[] = $resKomoditi->nama;

            $sqlProposalDetail = "SELECT * FROM tbl_proposal_detail WHERE id_proposal = '{$brand->id_proposal}'";
            $dataProposalDetail = $this->db->query($sqlProposalDetail)->result();

            $html = "<ul>";
            foreach ($dataProposalDetail as $key => $value) {
                $qJenisBantuan = "SELECT * FROM tbl_jenis_bantuan WHERE id = '{$value->id_jenis_bantuan}'";
                $resJenisBantuan = $this->db->query($qJenisBantuan)->row();

                $html .= "<li>";
                $html .= $resJenisBantuan->jenis_bantuan;
                $html .= "</li>";
            }
            $html .= "</ul>";

             if ($brand->status_dpa == 0) {
                $StatusDpa ='';
             } else {
                $StatusDpa ='<a class="ui green label">DPA '. date('Y', strtotime($brand->updated_date)) .'</a>';
             }


              $row[] = '' . $Status . '<br><br>' . $StatusDpa . '<br> Pengajuan proposal pada : <br><b>' . date('d-m-Y', strtotime($brand->created_date)) . '</b>';

            $buttonPrint = '';
            $buttonView = '';
            if ($brand->status_dpa == 0) {
                $buttonView = "<a href='" . site_url('view-data-proposal/' . $brand->id_proposal) . "' class='tiny ui primary button' target='__blank'><span tooltip='Lihat Data'><i class='fa fa-eye'></i></a></span>";
            } else {
                $buttonView = "<a href='" . site_url('view-data-proposal/' . $brand->id_proposal) . "' class='tiny ui primary button' target='__blank'><span tooltip='Lihat Data'><i class='fa fa-eye'></i></a></span>";
                $buttonPrint = "<a href='" . site_url('cetak-proposal/' . $brand->id_proposal) . "' class='tiny positive ui button' target='__blank'><span tooltip='Print Data'><i class='fa fa-print'></i></a></span>";
            }

            $row[] = ''.$buttonView.''.$buttonPrint;
            $data[] = $row;
        }


        $output = array(
            "draw" => $_POST['draw'],
            "data" => $data,
        );
        echo json_encode($output);
    }

    public function Edit($id) {

        if ($this->lib->login() != "") {
            $this->load->module('template');
            $this->load->model('Mdl_category', 'mdl_category');
            $nama_kabupaten = $this->session->userdata('kabupaten');

            $data = array(
                'title' => 'Pengajuan Proposal ' . $nama_kabupaten . '',
                'sessid' => $this->session->userdata('id'),
                'style' => '<style type="text/css">.main.container, .ui.vertical.segment {margin-top: 7em;}.ui.menu .item img.logo {margin-right: 1em;}</style>'
            );

            $id_user = $this->session->userdata('id');
            $data['user'] = $this->Mdl_registrasi->select_by_id($id_user);


            $data['brand'] = $this->Mdl_proposal->selectById($id);
            $data['detail'] = $this->Mdl_proposal->getDataDetail($id);

            $this->load->view('front/global/header', $data);
            $this->load->view('front/global/nav_menu', $data);
            $this->load->view('back/user/sidebar_dashboard');
            $this->load->view('v_opd/view_data_pengajuan', $data);
            $this->load->view('front/global/footer', $data);
        } else {
            $this->session->set_flashdata('not_login', '<p style="color: red;">Silahkan login untuk upload dokumen pengajuan.</p>');
            redirect('login-pengajuan');
        }
    }

    public function cetak($id)
    {

        $where = array(self::__tableId => $id);
        $data['brand'] = $this->Mdl_proposal->selectById($id);
        $this->load->view('v_opd/print', $data);
    }

    public function prosesAdd()
    {
        $errCode = 0;
        $errMessage = "";

        $date = date('Y-m-d H:i:s');
        $date2 = date('Y-m-d');
        // $kode = $this->Mdl_proposal->kode();
        $user = $this->session->userdata('nama_lengkap');

        $kabupaten = $this->input->post("kabupaten");
        $kecamatan = $this->input->post("kecamatan");
        $desa = $this->input->post("desa");
        $kelompok = $this->input->post("kelompok");
        $ketua = $this->input->post("ketua");
        $komoditi = $this->input->post("komoditi");
        $subKegiatan = $this->input->post("sub_kegiatan");

        $idJenisBantuan = $this->input->post("id_jenis_bantuan");
        $jenisBantuan = $this->input->post("jenis_bantuan");
        $volume = $this->input->post("volume");
        $satuan = $this->input->post("satuan");
        // $prioritas = $this->input->post("prioritas");
        $keterangan = $this->input->post("keterangan");
        $program = $this->input->post("program");
        $jenisKegiatan = $this->input->post("jenis_kegiatan");
        // $listJenisBantuan = parent::rotate(array('id_jenis_bantuan' => $idJenisBantuan, 'jenis_bantuan' => $jenisBantuan, 'volume' => $volume, 'satuan' => $satuan, 'prioritas' => $prioritas, 'keterangan' => $keterangan, 'program' => $program, 'jenis_kegiatan' => $jenisKegiatan));

        $listJenisBantuan = parent::rotate(array('id_jenis_bantuan' => $idJenisBantuan, 'jenis_bantuan' => $jenisBantuan, 'volume' => $volume, 'satuan' => $satuan, 
            'keterangan' => $keterangan, 'program' => $program, 'jenis_kegiatan' => $jenisKegiatan));

        $this->db->trans_begin();
        if ($errCode == 0) {
            if (strlen($kabupaten) == 0) {
                $errCode++;
                $errMessage = "Kabupaten wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($kecamatan) == 0) {
                $errCode++;
                $errMessage = "Kecamatan wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($desa) == 0) {
                $errCode++;
                $errMessage = "Desa wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($kelompok) == 0) {
                $errCode++;
                $errMessage = "Kelompok Tani wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($ketua) == 0) {
                $errCode++;
                $errMessage = "Ketua wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($komoditi) == 0) {
                $errCode++;
                $errMessage = "Jenis Komoditi wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (strlen($subKegiatan) == 0) {
                $errCode++;
                $errMessage = "Sub Kegiatan wajib di isi.";
            }
        }
        if ($errCode == 0) {
            if (count($listJenisBantuan) == 0) {
                $errCode++;
                $errMessage = "List Bantuan wajib di isi.";
            }
        }
        if ($errCode == 0) {
            foreach ($listJenisBantuan as $key => $value) {
                if (strlen($value['volume']) == 0) {
                    $errCode++;
                    $errMessage = "Volume wajib di isi.";
                    break;
                }
                // if (strlen($value['prioritas']) == 0) {
                //     $errCode++;
                //     $errMessage = "Prioritas wajib di isi.";
                //     break;
                // }
            }
        }
        if ($errCode == 0) {
            $getKelompokDpa = $this->getKelompokDpa($kelompok);
            if ($getKelompokDpa != null) {
                if (date('Y', strtotime($date2)) < date('Y', strtotime($getKelompokDpa->date))) {
                    $errCode++;
                    $errMessage = "Gagal mengajukan proposal karena data kelompok telah mengajukan proposal pada tahun " . date('Y', strtotime($getKelompokDpa->created_date)) . " .";
                }
            }
        }
        if ($errCode == 0) {
            try {
                $data = array(
                    'id_user' => $this->input->post('id_user'),
                    'id_komoditi' => $this->input->post('komoditi'),
                    'id_sub_kegiatan' => $this->input->post('sub_kegiatan'),
                    'id_petani' => $this->input->post('kelompok'),
                    'kabupaten' => $this->input->post('kabupaten'),
                    'kecamatan' => $this->input->post('kecamatan'),
                    'desa' => $this->input->post('desa'),
                    'tipe' => 'pengajuan permohonan rekomendasi',
                    'status' => 'Usulan Baru',
                    'created_date' => $date,
                    'created_by' => $user,
                    'updated_date' => $date,
                    'tanggal' => $date2,
                );
                $this->db->insert('tbl_proposal', $data);
                $idProposal = $this->db->insert_id();
                if (strlen($idProposal) > 0) {
                    foreach ($listJenisBantuan as $key => $value) {
                        $dataDetail = array(
                            'id_proposal' => $idProposal,
                            'id_jenis_bantuan' => $value['id_jenis_bantuan'],
                            'jenis_bantuan' => $value['id_jenis_bantuan'],
                            'satuan' => $value['satuan'],
                            'jumlah' => $value['volume'],
                            // 'prioritas' => $value['prioritas'],
                            'catatan' => $value['keterangan'],
                            'program' => $value['program'],
                            'kegiatan' => $value['jenis_kegiatan'],
                            'created_date' => $date,
                            'created_by' => $user,
                            'updated_date' => $date,
                        );
                        $this->db->insert('tbl_proposal_detail', $dataDetail);
                    }
                }

                $data2 = array(
                    // 'kode_rekomendasi'  => $this->input->post('kode_rekomendasi'),
                    'kode' => $idProposal,
                    'id_user' => $this->input->post('id_user'),
                    'status' => 'Usulan Baru',
                    'keterangan' => 'user <b>' . $user . '</b> melakukan proses pengajuan proposal ke Dinas Perkebunan Jawa Timur',
                    'created_by' => 'System',
                    'created_date' => $date,
                );
                $result = $this->db->insert('tbl_log', $data2);
            } catch (Exception $ex) {
                $errCode++;
                $errMessage = $ex->getMessage();
            }
        }
        if ($errCode == 0) {
            if ($this->db->trans_status() === FALSE) {
                $errCode++;
                $errMessage = "Error saving databse.";
            }
        }
        if ($errCode == 0) {
            $this->db->trans_commit();
            $out['status'] = true;
            $out['pesan'] = 'Pengajuan proposal berhasil di kirim';
        } else {
            $this->db->trans_rollback();
            $out['status'] = false;
            $out['pesan'] = $errMessage;
        }

        echo json_encode($out);
    }

    function home()
    {
        if ($this->lib->login() == "") {
            $this->session->set_flashdata('not_login', '<div class="ui success message"><i class="close icon"></i><div class="header">Silahkan login terlebih dahulu.</div></div>');
            redirect('login-pengajuan');
        } else {
            $nama_kabupaten = $this->session->userdata('kabupaten');
            $data = array(
                'title' => 'Pengajuan Proposal Kabupaten ' . $nama_kabupaten . '',
                'sessid' => $this->session->userdata('id'),
                'style' => '<style type="text/css">.main.container, .ui.vertical.segment {margin-top: 7em;}.ui.menu .item img.logo {margin-right: 1em;}</style>'
            );


            $data['status_button'] = $this->Mdl_proposal->selekButton();
            $id_user = $this->session->userdata('id');
            $data['user'] = $this->Mdl_registrasi->select_by_id($id_user);

            $this->load->view('front/global/header', $data);
            $this->load->view('front/global/nav_menu', $data);
            $this->load->view('back/user/sidebar_dashboard');
            $this->load->view('v_opd/home', $data);
            $this->load->view('front/global/footer', $data);
        }
    }

    public function getDataJenisBantuan()
    {
        $idJenisBantuan = $this->input->post("idJenisBantuan");

        $sqlJenisBantuan = "SELECT * FROM tbl_jenis_bantuan WHERE id = '{$idJenisBantuan}'";
        $dataJenisBantuan = $this->db->query($sqlJenisBantuan)->row();

        $sqlKegiatan = "SELECT * FROM tbl_kegiatan WHERE id_jenis_bantuan = '{$dataJenisBantuan->id}'";
        $dataKegiatan = $this->db->query($sqlKegiatan)->row();

        $data = array(
            'satuan' => $dataJenisBantuan->satuan,
            'jenis_kegiatan' => $dataKegiatan->jenis_kegiatan,
            'program' => $dataKegiatan->program,
        );

        echo json_encode($data);
    }

    public function getKelompokDpa($kelompok = '')
    {
        $sql = "SELECT * FROM tbl_history_pengajuan WHERE id_kelompok = \"{$kelompok}\" ORDER BY id_proposal DESC LIMIT 1";
        $data = $this->db->query($sql);
        return $data->row();
    }
}
