<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Petani extends MX_Controller {

    const __tableName = 'tbl_petani';
    const __tableId = 'id_petani';

    function __construct() {
        parent::__construct();
        $this->load->model('Mdl_registrasi');
        $this->load->model('Mdl_petani');
    }

    public function index($param = '') {
        header('Location: ' . base_url() . '');
    }

    function ambil_data() {

        $modul = $this->input->post('modul');

        if ($modul == "kabupaten") {
            $id = $this->input->post('kode');
            echo $this->Mdl_petani->select_kabupaten($id);
        }

        if ($modul == "kecamatan") {
            $id = $this->input->post('kode');
            echo $this->Mdl_petani->select_kecamatan($id);
        } else if ($modul == "desa") {
            $id = $this->input->post('kecamatan');
            echo $this->Mdl_petani->select_desa($id);
        }
    }

    function home() {
        if ($this->lib->login() == "") {
            $this->session->set_flashdata('not_login', '<div class="ui success message"><i class="close icon"></i><div class="header">Silahkan login terlebih dahulu.</div></div>');
            redirect('login-pengajuan');
        } else {
            $nama_kabupaten = $this->session->userdata('kabupaten');

            $data = array(
                'title' => 'Data Kelompok Tani Kab ' . $nama_kabupaten . '',
                'sessid' => $this->session->userdata('id'),
                'style' => '<style type="text/css">.main.container, .ui.vertical.segment {margin-top: 7em;}.ui.menu .item img.logo {margin-right: 1em;}</style>'
            );

            $data['status_button'] = $this->Mdl_petani->selekButton();
            $data['file_form'] = $this->Mdl_petani->selekFormat();
            $id_user = $this->session->userdata('id');
            $data['user'] = $this->Mdl_registrasi->select_by_id($id_user);

            $this->load->view('front/global/header', $data);
            $this->load->view('front/global/nav_menu', $data);
            $this->load->view('back/user/sidebar_dashboard');
            $this->load->view('v_opd/home_petani', $data);
            $this->load->view('front/global/footer', $data);
        }
    }

    public function ajax_list() {
        $id_user = $this->session->userdata('kabupaten');
        $list = $this->Mdl_petani->get_where_custom('kabupaten', $id_user);

        $data = array();
        $no = $_POST['start'];
        foreach ($list->result() as $brand) {

            $status = '<a class="tiny ui red label">Non Aktif</a>';
            if ($brand->status == 'Aktif') {
                $status = '<a class="tiny ui blue label">Aktif</a>';
            }

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $brand->nama_kelompok;
            $row[] = $brand->nama_ketua;
            $row[] = $brand->kecamatan;
            $row[] = $brand->desa;
            //add html for action

            $buttonEdit = anchor('view-kelompok-petani/' . $brand->id_petani, ' <span tooltip="Edit Data"><span class="fa fa-edit" ></span>', ' class="tiny positive ui button"');

            $buttonDel = '<span tooltip=" Hapus"><button class=" tiny negative ui positive button hapus-petani" data-id=' . "'" . $brand->id_petani . "'" . '><i class="fa fa-trash"></button></span>';

            $row[] = $buttonEdit . '  ' . $buttonDel;
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function add_dokumen() {
        if ($this->lib->login() != "") {
            $this->load->module('template');
            $this->load->model('Mdl_category', 'mdl_category');
            $nama_kabupaten = $this->session->userdata('kabupaten');

            $data = array(
                'title' => 'Tambah Kelompok Tani Kab ' . $nama_kabupaten . '',
                'kabupaten' => $this->Mdl_petani->selekKab(),
                'sessid' => $this->session->userdata('id'),
                'style' => '<style type="text/css">.main.container, .ui.vertical.segment {margin-top: 7em;}.ui.menu .item img.logo {margin-right: 1em;}</style>'
            );

            $id_user = $this->session->userdata('id');
            $data['user'] = $this->Mdl_registrasi->select_by_id($id_user);


            $this->load->view('front/global/header', $data);
            $this->load->view('front/global/nav_menu', $data);
            $this->load->view('back/user/sidebar_dashboard');
            $this->load->view('v_opd/tambah_petani', $data);
            $this->load->view('front/global/footer', $data);
        } else {
            $this->session->set_flashdata('not_login', '<p style="color: red;">ilahkan login untuk upload dokumen pengajuan.</p>');
            redirect('login-pengajuan');
        }
    }

    public function prosesAdd() {

        $username = $this->session->userdata('nama_lengkap');
        $date = date('Y-m-d H:i:s');


        $config['upload_path'] = "./upload/petani/";
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '2048'; //maksimum besar file 2M
        $config['overwrite'] = TRUE;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload("gambar")) {
            $image_data = $this->upload->data();
            $path['link'] = "upload/petani/";

            $data = array(
                'nama_kelompok' => $this->input->post('nama_kelompok'),
                'nama_ketua' => $this->input->post('nama_ketua'),
                'kabupaten' => $this->input->post('kabupaten'),
                'kecamatan' => $this->input->post('kecamatan'),
                'folder' => $this->input->post('folder'),
                'desa' => $this->input->post('desa'),
                'longitude' => $this->input->post('longitude'),
                'latitude' => $this->input->post('latitude'),
                'gambar' => $path['link'] . '' . $image_data['file_name'],
                'tipe_petani' => 'Baru',
                'status' => 'Non aktif',
                'created_by' => $username,
                'created_date' => $date
            );

            $result = $this->db->insert(self::__tableName, $data);
            $this->do_upload_others_images();

            if ($this->db->trans_status() === FALSE) {
                $out = array('status' => false, 'pesan' => 'Maaf data gagal di simpan !');
            }

            if ($result > 0) {
                $this->db->trans_commit();
                $out = array('status' => true, 'pesan' => ' Data berhasil di simpan');
            } else {
                $this->db->trans_rollback();
                $out = array('status' => false, 'pesan' => 'Maaf data gagal di simpan !');
            }
        } else {
            $data = array(
                'nama_kelompok' => $this->input->post('nama_kelompok'),
                'nama_ketua' => $this->input->post('nama_ketua'),
                'kabupaten' => $this->input->post('kabupaten'),
                'kecamatan' => $this->input->post('kecamatan'),
                'desa' => $this->input->post('desa'),
                'folder' => $this->input->post('folder'),
                'longitude' => $this->input->post('longitude'),
                'latitude' => $this->input->post('latitude'),
                'tipe_petani' => 'Baru',
                'status' => 'Non aktif',
                'created_by' => $username,
                'created_date' => $date
            );
            $result = $this->db->insert(self::__tableName, $data);
            $this->do_upload_others_images();
            if ($result > 0) {
                $this->db->trans_commit();
                $out = array('status' => true, 'pesan' => ' Data berhasil di simpan');
            } else {
                $this->db->trans_rollback();
                $out = array('status' => false, 'pesan' => 'Maaf data gagal di simpan !');
            }
        }
        echo json_encode($out);
    }

    public function Edit($id) {

        if ($this->lib->login() != "") {
            $this->load->module('template');
            $this->load->model('Mdl_category', 'mdl_category');
            $nama_kabupaten = $this->session->userdata('kabupaten');

            $data = array(
                'title' => 'Edit Kelompok Petani Kab ' . $nama_kabupaten . '',
                'sessid' => $this->session->userdata('id'),
                'kabupaten' => $this->Mdl_petani->selekKab(),
                'status' => $this->Mdl_petani->selekStatus(),
                'style' => '<style type="text/css">.main.container, .ui.vertical.segment {margin-top: 7em;}.ui.menu .item img.logo {margin-right: 1em;}</style>'
            );

            $id_user = $this->session->userdata('id');
            $data['user'] = $this->Mdl_registrasi->select_by_id($id_user);

            $path = $this->Mdl_petani->selectById($id);
            $path->folder;
            $res = $path->folder;

            $directory = 'upload/petani/' . $res;
            $data["images"] = glob($directory . "/*");

            $data['petani'] = $this->Mdl_petani->selectById($id);
            $data['kecamatan'] = $this->Mdl_petani->selekKec();
            $data['desa'] = $this->Mdl_petani->selekDes();

            $this->load->view('front/global/header', $data);
            $this->load->view('front/global/nav_menu', $data);
            $this->load->view('back/user/sidebar_dashboard');
            $this->load->view('v_opd/view_data_petani', $data);
            $this->load->view('front/global/footer', $data);
        } else {
            $this->session->set_flashdata('not_login', '<p style="color: red;">Silahkan login untuk upload dokumen pengajuan.</p>');
            redirect('login-pengajuan');
        }
    }

    public function download() {
        if ($this->input->post('images')) {
            $images = $this->input->post('images');
            foreach ($images as $image) {
                $this->zip->read_file($image);
            }
            $this->zip->download('' . time() . '.zip');
        }
    }

    public function prosesUpdate() {

        $username = $this->userdata->nama;
        $date = date('Y-m-d H:i:s');
        $date2 = date('Y-m-d');

        $where = trim($this->input->post(self::__tableId));

        $config['upload_path'] = "./upload/petani/";
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '2048'; //maksimum besar file 2M
        $config['overwrite'] = TRUE;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload("gambar")) {
            $image_data = $this->upload->data();
            $path['link'] = "upload/petani/";

            $data = array(
                'nama_kelompok' => $this->input->post('nama_kelompok'),
                'nama_ketua' => $this->input->post('nama_ketua'),
                'kabupaten' => $this->input->post('kabupaten'),
                'kecamatan' => $this->input->post('kecamatan'),
                'desa' => $this->input->post('desa'),
                'folder' => $this->input->post('folder'),
                'longitude' => $this->input->post('longitude'),
                'latitude' => $this->input->post('latitude'),
                'gambar' => $path['link'] . '' . $image_data['file_name'],
                'status' => $this->input->post('status'),
                'created_by' => $username,
                'created_date' => $date
            );

            $result = $this->db->update(self::__tableName, $data, array(self::__tableId => $where));
            $this->do_upload_others_images();

            if ($this->db->trans_status() === FALSE) {
                $out = array('status' => false, 'pesan' => 'Maaf data gagal di update !');
            }

            if ($result > 0) {
                $this->db->trans_commit();
                $out = array('status' => true, 'pesan' => ' Data berhasil di update');
            } else {
                $this->db->trans_rollback();
                $out = array('status' => false, 'pesan' => 'Maaf data gagal di update !');
            }
        } else {
            $data = array(
                'nama_kelompok' => $this->input->post('nama_kelompok'),
                'nama_ketua' => $this->input->post('nama_ketua'),
                'kabupaten' => $this->input->post('kabupaten'),
                'kecamatan' => $this->input->post('kecamatan'),
                'desa' => $this->input->post('desa'),
                'folder' => $this->input->post('folder'),
                'longitude' => $this->input->post('longitude'),
                'latitude' => $this->input->post('latitude'),
                'status' => $this->input->post('status'),
                'created_by' => $username,
                'created_date' => $date
            );
            $result = $this->db->update(self::__tableName, $data, array(self::__tableId => $where));
            $this->do_upload_others_images();

            if ($this->db->trans_status() === FALSE) {
                $out = array('status' => false, 'pesan' => 'Maaf data gagal di update !');
            }

            if ($result > 0) {
                $this->db->trans_commit();
                $out = array('status' => true, 'pesan' => ' Data berhasil di update');
            } else {
                $this->db->trans_rollback();
                $out = array('status' => false, 'pesan' => 'Maaf data gagal di update !');
            }
        }
        echo json_encode($out);
    }

    private $allowed_img_types = 'gif|jpg|png|jpeg|JPG|PNG|JPEG|pdf';

    private function do_upload_others_images() {
        $upath = './upload/petani/' . $_POST['folder'] . '/';
        if (!file_exists($upath)) {
            mkdir($upath, 0777);
        }

        $this->load->library('upload');

        $files = $_FILES;
        $cpt = count($_FILES['others']['name']);
        for ($i = 0; $i < $cpt; $i++) {
            unset($_FILES);
            $_FILES['others']['name'] = $files['others']['name'][$i];
            $_FILES['others']['type'] = $files['others']['type'][$i];
            $_FILES['others']['tmp_name'] = $files['others']['tmp_name'][$i];
            $_FILES['others']['error'] = $files['others']['error'][$i];
            $_FILES['others']['size'] = $files['others']['size'][$i];

            $this->upload->initialize(array(
                'upload_path' => $upath,
                'allowed_types' => $this->allowed_img_types
            ));
            $this->upload->do_upload('others');
        }
    }

    public function import() {

        $error = false;

        $this->form_validation->set_rules('excel', 'File', 'trim|required');
        if ($_FILES['excel']['name'] == '') {
            
        } else {
            $config['upload_path'] = './assets/excel/';
            $config['allowed_types'] = 'xls|xlsx';

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('excel')) {
                $error = array('error' => $this->upload->display_errors());
            } else {
                $dataUpload = $this->upload->data();

                error_reporting(E_ALL);
                date_default_timezone_set('Asia/Jakarta');

                include './assets/phpexcel/Classes/PHPExcel/IOFactory.php';

                $inputFileName = './assets/excel/' . $dataUpload['file_name'];
                $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
                $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);

                $data = array();
                $index = 0;
                
                if (!$error) {
                foreach($sheetData as $row){
                $checkFiles = $this->Mdl_petani->checkFiles($row['D']);
                if ($checkFiles == false) {
                array_push($data, array(
                                    'kabupaten'      => $row['A'],
                                    'kecamatan'      => $row['B'],
                                    'desa'           => $row['C'],
                                    'nama_kelompok'  => $row['D'],
                                    'nama_ketua'     => $row['E'],
                                    'tipe_petani'    => $row['F'],
                                ));
                
                $index++;
            }
        }
        }
        unlink('./assets/excel/' . $dataUpload['file_name']);

            if (!$error) {
                if (empty($data)) {
                    $out = array('status' => 'awas', 'pesan' => 'Maaf, Data sudah ada dalam sistem silahkan cek kembali data import terakhir !');
                    $error = true;
                }
            }

            if (!$error) {
            $result=$this->db->insert_batch('tbl_petani', $data);

            if ($result > 0) {
                $out = array('status' => true, 'pesan' => ' Data berhasil di import');
            } else {
                $out = array('status' => false, 'pesan' => 'Maaf data gagal di import !');
            }
        }
            
        }

            echo json_encode($out);
        }
    }

    // public function import2() {

    //     $this->form_validation->set_rules('excel', 'File', 'trim|required');
    //     if ($_FILES['excel']['name'] == '') {
            
    //     } else {
    //         $config['upload_path'] = './assets/excel/';
    //         $config['allowed_types'] = 'xls|xlsx';

    //         $this->load->library('upload', $config);

    //         if (!$this->upload->do_upload('excel')) {
    //             $error = array('error' => $this->upload->display_errors());
    //         } else {
    //             $data = $this->upload->data();

    //             error_reporting(E_ALL);
    //             date_default_timezone_set('Asia/Jakarta');

    //             include './assets/phpexcel/Classes/PHPExcel/IOFactory.php';

    //             $inputFileName = './assets/excel/' . $data['file_name'];
    //             $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
    //             $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);

    //             $index = 0;
    //             foreach ($sheetData as $key => $value) {
    //                 if ($key != 1) {
    //                     $check = $this->Mdl_petani->checkFiles($value['A']);
    //                     if ($check != 1) {
    //                     $resultData[$index]['kabupaten'] = $value['A'];
    //                     $resultData[$index]['kecamatan'] = $value['B'];
    //                     $resultData[$index]['desa'] = $value['C'];
    //                     $resultData[$index]['nama_kelompok'] = $value['D'];
    //                     $resultData[$index]['nama_ketua'] = $value['E'];
    //                     $resultData[$index]['tipe_petani'] = $value['F'];
    //                     }
    //                 }
    //                 $index++;
    //             }
    //             unlink('./assets/excel/' . $data['file_name']);

    //             if (count($resultData) != 0) {
    //                 $result = $this->Mdl_petani->simpan_import($resultData);
    //                 if ($result > 0) {
    //                     $out = array('status' => true, 'pesan' => ' Data berhasil di import');
    //                 } else {
    //                     $out = array('status' => false, 'pesan' => 'Maaf data gagal di import !');
    //                 }
    //             } else {
    //                 $out = array('status' => false, 'pesan' => 'Maaf data gagal di import !');
    //             }
            
    //     }

    //         echo json_encode($out);
    //     }
    // }

    public function hapus() {

        $this->load->helper("file");
        $id = $_POST[self::__tableId];
        $path = $this->Mdl_petani->selectById($id);
        $path->folder;
        $res = $path->folder;
        $path = 'upload/petani/' . $res;
        delete_files($path, true, false, 1);

        $result = $this->Mdl_petani->hapus($id);

        if ($result > 0) {
            $out = array('status' => true, 'pesan' => ' Data berhasil di hapus');
        } else {
            $out = array('status' => false, 'pesan' => 'Maaf data gagal di hapus !');
        }
        echo json_encode($out);
    }

}
