<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_proposal extends AUTH_Controller
{
    const __tableName = 'tbl_proposal';
    const __tableId = 'id_proposal';
    const __folder = 'v_report_proposal/';
    const __kode_menu = 'report-proposal';
    const __title = 'Report Proposal ';
    const __model = 'M_rekap_proposal';

    public function __construct()
    {
        parent::__construct();
        $this->load->model(self::__model);
        $this->load->model('M_sidebar');
    }

    public function loadkonten($page, $data)
    {

        $data['userdata'] = $this->userdata;
        $ajax = ($this->input->post('status_link') == "ajax" ? true : false);
        if (!$ajax) {
            $this->load->view('Dashboard/layouts/header', $data);
        }
        $this->load->view($page, $data);
        if (!$ajax)
            $this->load->view('Dashboard/layouts/footer', $data);
    }

    public function index()
    {
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = self::__title;
        $data['status'] = $this->M_rekap_proposal->selectStatus();
        $this->loadkonten('' . self::__folder . 'v_rekap-data', $data);
    }

    public function filter()
    {
        $groupId = $this->session->userdata('grup_id');
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = self::__title;
        $data['status'] = $this->M_rekap_proposal->selectStatus();

        $tanggal_awal = $this->input->post('tanggal_awal');
        $tanggal_akhir = $this->input->post('tanggal_akhir');
        $status = $this->input->post('status');

        $data['filter'] = $this->M_rekap_proposal->export_data($tanggal_awal, $tanggal_akhir, $status, $groupId);
        $data['tanggal_awal'] = $tanggal_awal;
        $data['tanggal_akhir'] = $tanggal_akhir;
        $data['data_status'] = $status;

        $this->loadkonten('' . self::__folder . 'filter', $data);
    }

    public function export_excel()
    {
        $groupId = $this->session->userdata('grup_id');
        $waktu = date("Y-m-d h:i");

        $data['title'] = "Report Data Pengajuan Proposal Dinas Perkebunan Prov Jatim " . $waktu . "";

        $tanggal_awal = $this->input->post('tanggal_awal');
        $tanggal_akhir = $this->input->post('tanggal_akhir');
        $status = $this->input->post('status');

        $data['excel'] = $this->M_rekap_proposal->export_data($tanggal_awal, $tanggal_akhir, $status, $groupId);
        $data['tanggal_awal'] = $tanggal_awal;
        $data['tanggal_akhir'] = $tanggal_akhir;
        $this->load->view('' . self::__folder . 'v_laporan_excel', $data);
    }
}
