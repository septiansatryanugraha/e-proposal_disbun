<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_rekap_history_pengajuan extends CI_Model
{

    public function export_data($tanggalAwal, $tanggalAkhir, $status, $groupId)
    {
        if (strlen($tanggalAwal) > 0) {
            $tanggalAwal = date('Y-m-d', strtotime($tanggalAwal));
        }
        if (strlen($tanggalAkhir) > 0) {
            $tanggalAkhir = date('Y-m-d', strtotime($tanggalAkhir));
        }

        $sql = "SELECT tbl_log.*,
                IFNULL(tbl_petani.nama_kelompok, '') AS nama_kelompok,
                IFNULL(tbl_petani.nama_ketua, '') AS nama_ketua
                FROM tbl_log AS tbl_log
                LEFT JOIN tbl_proposal ON tbl_log.kode = tbl_proposal.id_proposal
                LEFT JOIN tbl_komoditi ON tbl_komoditi.id_komoditi = tbl_proposal.id_komoditi
                LEFT JOIN tbl_petani ON tbl_petani.id_petani = tbl_proposal.id_petani
                WHERE tbl_log.id_admin IS NULL 
                AND tbl_log.created_date >= '{$tanggalAwal}'
                AND tbl_log.created_date <= '{$tanggalAkhir}'";
        if ($groupId > 2) {
            $sql .= " AND tbl_komoditi.grup_id = '{$groupId}'";
        }

        $data = $this->db->query($sql);
        if ($data->num_rows() > 0) {
            return $data->result();
        }
    }
}
