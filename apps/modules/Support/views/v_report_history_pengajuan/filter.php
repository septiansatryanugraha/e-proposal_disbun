<?php $this->load->view('_heading/_headerContent') ?>

<section class="content">
    <div class="box">

        <div class="box-header with-border">
            <i class="fa fa-file-excel-o" aria-hidden="true"></i>
            <!-- pesan customer -->
            <h3 class="box-title">Export laporan dalam format Excel</h3>
        </div>

        <!-- /.box-header -->
        <div class="box-body">

            <form method="post" id="myform" action="<?php echo site_url('filter-history-pengajuan'); ?>">

                <div class="box-header">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Tanggal Awal:</label>
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" name="tanggal_awal" class="form-control" id="from" value="<?php echo date('01-m-Y'); ?>">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Tanggal Akhir:</label>
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" name="tanggal_akhir" class="form-control" id="to" value="<?php echo date('t-m-Y'); ?>">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-1">
                        <div class="form-group">
                            <label></label>
                            <div class="input-group date">
                                <button name="simpan" type="submit" class="btn btn-sm btn-primary batas-export klik"><i class="fa fa-refresh"></i> Filter</button>
                            </div>
                        </div>
                    </div>
            </form>


            <form method="post" id="myform" action="<?php echo site_url('export-history-pengajuan'); ?>">
                <div class="col-md-1 jarak-kiri">
                    <div class="form-group">
                        <label></label>
                        <div class="input-group date">
                            <input type="hidden" name="tanggal_awal"  value="<?php echo $tanggal_awal; ?>">
                            <input type="hidden" name="tanggal_akhir"  value="<?php echo $tanggal_akhir; ?>">
                            <button name="simpan" type="submit" class="btn btn-sm btn-primary batas-export"><i class="fa fa-download"></i> Export Excel</button>
                        </div>
                    </div>
                </div>
            </form><br><br><br><br>

            <table id="tableku" class=" table table-bordered table-striped">
                <thead>
                    <tr>
                        <th style="text-align: center; width: 15%">Tanggal Pengajuan</th>
                        <th style="text-align: center; width: 15%">Kelompok</th>
                        <th style="text-align: center; width: 15%">Ketua</th>
                        <th style="text-align: center; width: 55%">Keterangan</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($filter as $data) { ?>
                        <tr>
                            <td style="text-align: left;"><?php echo date('d-m-Y', strtotime($data->created_date)) ?></td>
                            <td style="text-align: left;"><?php echo $data->nama_kelompok ?></td>
                            <td style="text-align: left;"><?php echo $data->nama_ketua ?></td>
                            <td style="text-align: left;"><?php echo $data->keterangan ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>

        </div>
    </div>
</div>
</section>

<script>
    // untuk datetime from
    $(function ()
    {
        $("#from").datepicker({
            orientation: "left",
            autoclose: !0,
            format: 'dd-mm-yyyy'
        })
    });

    // untuk datetime to
    $(function ()
    {
        $("#to").datepicker({
            orientation: "left",
            autoclose: !0,
            format: 'dd-mm-yyyy'
        })
    });


    $(document).ready(function () {
        $('#tableku').DataTable();
    });

</script>