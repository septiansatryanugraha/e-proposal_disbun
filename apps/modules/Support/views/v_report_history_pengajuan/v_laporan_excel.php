<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$title.xls");
header("Pragma: no-cache");
header("Expires: 0");

?>

<center><h2>Report History Pengajuan Proposal Dinas Perkebunan Prov Jatim</h2></center>

<br>
<table>
    <tr><td></td><td><b>Periode Report &nbsp;&nbsp;&nbsp; :</b></td><td><b>Periode awal &nbsp;&nbsp;&nbsp; : <?php echo date('d-m-Y', strtotime($tanggal_awal)) ?></b></td></tr>
</table>
<table>
    <tr><td></td><td></td><td><b>Periode akhir &nbsp;&nbsp;&nbsp; : <?php echo date('d-m-Y', strtotime($tanggal_akhir)) ?></td></tr></table>
<br><br>

<table border="1" width="80%">
    <thead>
        <tr>
            <th style="text-align: center; width: 15%">Tanggal Pengajuan</th>
            <th style="text-align: center; width: 15%">Kelompok</th>
            <th style="text-align: center; width: 15%">Ketua</th>
            <th style="text-align: center; width: 55%">Keterangan</th>
        </tr>
    </thead>

    <tbody>
        <?php
        if (!empty($excel)) {
            $no = 1;
            foreach ($excel as $data) {

                ?>
                <tr>
                    <td style="text-align: left;"><?php echo date('d-m-Y', strtotime($data->created_date)) ?></td>
                    <td style="text-align: left;"><?php echo $data->nama_kelompok ?></td>
                    <td style="text-align: left;"><?php echo $data->nama_ketua ?></td>
                    <td style="text-align: left;"><?php echo $data->keterangan ?></td>
                </tr><?php
                $no++;
            }

            ?>
        <?php } else { ?>
        <center><?php echo "Belum Ada history pengajuan" ?></center>
    <?php } ?>
</tbody>
</table><br>



<br><br>





