<?php $this->load->view('_heading/_headerContent') ?>

<section class="content">
    <div class="box">

        <div class="box-header with-border">
            <i class="fa fa-file-excel-o" aria-hidden="true"></i>
            <!-- pesan customer -->
            <h3 class="box-title">Export laporan dalam format Excel</h3>
        </div>

        <!-- /.box-header -->
        <div class="box-body">

            <form method="post" id="myform" action="<?php echo site_url('filter-proposal'); ?>">

                <div class="box-header">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Tanggal Awal:</label>
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" name="tanggal_awal" class="form-control" id="from" value="<?php echo date('01-m-Y'); ?>">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Tanggal Akhir:</label>
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" name="tanggal_akhir" class="form-control" id="to" value="<?php echo date('t-m-Y'); ?>">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Status Proposal </label>
                            <div class="input-group col-sm-9">
                                <select name="status" class="form-control selek-tipe">
                                    <option></option>
                                    <?php foreach ($status as $data) { ?>
                                        <option value="<?php echo $data->nama; ?>">
                                            <?php echo $data->nama; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-1">
                        <div class="form-group">
                            <label></label>
                            <div class="input-group date">
                                <button name="simpan" type="submit" class="btn btn-sm btn-primary batas-export klik"><i class="fa fa-refresh"></i> Filter</button>
                            </div>
                        </div>
                    </div>
            </form>


            <form method="post" id="myform" action="<?php echo site_url('export-proposal'); ?>">
                <div class="col-md-1 jarak-kiri">
                    <div class="form-group">
                        <label></label>
                        <div class="input-group date">
                            <input type="hidden" name="tanggal_awal"  value="<?php echo $tanggal_awal; ?>">
                            <input type="hidden" name="tanggal_akhir"  value="<?php echo $tanggal_akhir; ?>">
                            <input type="hidden" name="status"  value="<?php echo $data_status; ?>">
                            <button name="simpan" type="submit" class="btn btn-sm btn-primary batas-export"><i class="fa fa-download"></i> Export Excel</button>
                        </div>
                    </div>
                </div>
            </form><br><br><br><br>


            <table id="tableku" class=" table table-bordered table-striped">
                <thead>
                    <tr>
                        <th align="center">No</th>
                        <th align="center">Kabupaten</th>
                        <th align="center">Kecamatan</th>
                        <th align="center">Desa</th>
                        <th align="center">Kelompok</th>
                        <th align="center">Ketua</th>
                        <th align="center">Status</th>
                        <th align="center">Tanggal Pengajuan</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    if (!empty($filter)) {
                        $no = 1;
                        foreach ($filter as $data) {

                            ?>
                            <tr>
                                <td><?php echo $no ?></td>
                                <td><?php echo $data->kabupaten ?></td>
                                <td><?php echo $data->kecamatan ?></td>
                                <td><?php echo $data->desa ?></td>
                                <td><?php echo $data->nama_kelompok ?></td>
                                <td><?php echo $data->nama_ketua ?></td>
                                <td><?php echo $data->status ?></td>
                                <td><?php echo date('d-m-Y', strtotime($data->tanggal)); ?></td>
                            </tr>
                            <?php
                            $no++;
                        }

                        ?>
                    <?php } else { ?>
                    <?php } ?>
                </tbody>	
            </table>

        </div>
    </div>
</div>
</section>

<script>
    // untuk datetime from
    $(function ()
    {
        $("#from").datepicker({
            orientation: "left",
            autoclose: !0,
            format: 'dd-mm-yyyy'
        })
    });

    // untuk datetime to
    $(function ()
    {
        $("#to").datepicker({
            orientation: "left",
            autoclose: !0,
            format: 'dd-mm-yyyy'
        })
    });


    $(document).ready(function () {
        $('#tableku').DataTable();
    });

    $(function ()
    {
        $(".selek-tipe").select2({
            placeholder: " -- Pilih Status -- ",
            allowClear: true
        });
    });

</script>