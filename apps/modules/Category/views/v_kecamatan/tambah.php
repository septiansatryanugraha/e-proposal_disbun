<?php $this->load->view('_heading/_headerContent') ?>

<style>
#osas {
color:red;
font-weight:bold;
margin-left:0px;
}

  .field-icon {
  float: left;
  margin-left: 93%;
  margin-top: -25px;
  position: relative;
  z-index: 2;
 }


</style>
		
<section class="content">
    
    
    <!-- style loading -->
    <div class="loading2"></div>
    <!-- -->
    
    <div class="box">
        <div class="row">
        <div class="col-md-8">
        <!-- form start -->
        <form class="form-horizontal" id="form-tambah" method="POST">
        <input type="hidden" name="created_by" value="<?php echo $userdata->nama; ?>">
      
        <div class="box-body">
               
       <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Kecamatan</label>
        <div class="col-sm-6">
        <input type="text" class="form-control" name="kecamatan" placeholder="Kecamatan" id="kecamatan" aria-describedby="sizing-addon2">
        </div>
        </div>

        <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Kode Kecamatan</label>
        <div class="col-sm-6">
        <input type="text" class="form-control" name="kode_kecamatan" placeholder="kode kecamatan " id="kode_kecamatan" onkeypress="return hanyaAngka(event)" aria-describedby="sizing-addon2">
        </div>
        </div>

        <div class="form-group">
       <label class="col-sm-2 control-label">Kabupaten</label>
       <div class="col-sm-4">
       <select name="kode_kabupaten" class="form-control selek-komoditi" id="kabupaten">
                <option></option>
                <?php
                foreach ($kab as $data) {
                ?>
                <option value="<?php echo $data->kode; ?>">
                <?php echo $data->kabupaten; ?>
                </option>
                <?php
                }
                ?>
                </select>
       </div>
       </div>
        </div>
        <div class="box-footer">
        <button name="submit" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Simpan</button>
        <a class="klik ajaxify" href="<?php echo site_url('kecamatan'); ?>"><button class="btn btn-primary btn-flat" ><i class="fa fa-arrow-left"></i> Kembali</button></a>
        </div>
        </form>
      
        </div>
        <!-- /.box -->
       
        </div>
          <!-- /.row -->
    </div>

    </section>



    <script type="text/javascript">
    //Proses Controller logic ajax

    function cekemail(a) {
    re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(a);
    }

    function hanyaAngka(evt) {
      var charCode = (evt.which) ? evt.which : event.keyCode
       if (charCode > 31 && (charCode < 48 || charCode > 57))
 
        return false;
      return true;
    } 


    function cekpassword(a) {
    re2 = /^\S{3,}$/;
    return re2.test(a);
    }

    $('#form-tambah').submit(function(e) {
      
        var error = 0;
        var message = "";

        var data = $(this).serialize();

        var kecamatan = $("#kecamatan").val();
        var kecamatan = kecamatan.trim();

          if (error == 0) {
            if (kecamatan.length == 0) {
                error++;
                message = "Nama Kecamatan wajib di isi.";
            }
        }

        var kode_kecamatan = $("#kode_kecamatan").val();
        var kode_kecamatan = kode_kecamatan.trim();

          if (error == 0) {
            if (kode_kecamatan.length == 0) {
                error++;
                message = "Kode kecamatan wajib di isi.";
            }
        }

        var kabupaten = $("#kabupaten").val();
        var kabupaten = kabupaten.trim();

          if (error == 0) {
            if (kabupaten.length == 0) {
                error++;
                message = "Kabupaten wajib di isi.";
            }
        }

      
            if (error == 0) {
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                $(".loading2").show();
                $(".loading2").modal('show');
                },
                url: '<?php echo base_url('Category/Kecamatan/prosesAdd'); ?>',
                type:"post",
                data:new FormData(this),
                processData:false,
                contentType:false,
                cache:false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == true) {
                    document.getElementById("form-tambah").reset();
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    swal("Success", result.pesan, "success");
                } else {
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    swal("Warning", result.pesan, "warning");
                }
            })
            e.preventDefault();
        } else {
            toastr.error(message,'Warning', {timeOut: 5000},toastr.options = {
             "closeButton": true}); 
            return false;

        }
    });

     // untuk select2 ajak pilih department
    $(function () 
    {
    $(".selek-komoditi").select2({
        placeholder: " -- Pilih kabupaten -- "
        });
    });


  </script>

		
		

		
		