 <?php $this->load->view('_heading/_headerContent') ?>
 
 <style>
  .field-icon {
  float: left;
  margin-left: 93%;
  margin-top: -25px;
  position: relative;
  z-index: 2;
}

 </style>


 <?php
  $password=$brand->password;  
  $dec_pass = base64_decode($password);   
 ?>
 
 
 <section class="content">

  <!-- style loading -->
    <div class="loading2"></div>
    <!-- -->
  
    <div class="box">
    <form class="form-horizontal" id="form-update" method="POST">
    <input type="hidden"  name="id" value="<?php echo $brand->id; ?>">
    <input type="hidden"  name="nama" value="<?php echo $brand->nama; ?>">
  
        <div class="row">
        <div class="col-md-10">
      
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body">

        <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Kecamatan</label>
        <div class="col-sm-5">
        <input type="text" class="form-control" name="kecamatan" placeholder="Kecamatan" id="kecamatan" aria-describedby="sizing-addon2" value="<?php echo $brand->kecamatan; ?>">
        </div>
        </div>

        <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Kode Kecamatan</label>
        <div class="col-sm-5">
        <input type="text" class="form-control" name="kode_kecamatan" placeholder="kode kecamatan " id="kode_kecamatan" onkeypress="return hanyaAngka(event)" aria-describedby="sizing-addon2" value="<?php echo $brand->kode_kecamatan; ?>">
        </div>
        </div>
    

        <div class="form-group">
        <label class="col-sm-2 control-label">Kabupaten</label>
        <div class="col-sm-3">
        <select name="kode_kabupaten" class="form-control selek-komoditi" id="kabupaten">
        <?php foreach ($kab as $data) { ?>
        <option value="<?php echo $data->kode; ?>"<?php if($data->kode == $brand->kode_kabupaten)
        {echo "selected='selected'";} ?>>
        <?php echo $data->kabupaten; ?>
        </option>
        <?php } ?>
        </select>
        </div>
        </div>
   
       
     </div>
        <!-- /.box-body -->
    
    <div class="box-footer">
        <button name="simpan" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Update</button>
        <a class="klik ajaxify" href="<?php echo site_url('kecamatan'); ?>"><button class="btn btn-primary btn-flat" ><i class="fa fa-arrow-left"></i> Kembali</button></a>
        </div>
        </form>
        </div>
        <!-- /.box -->
        </div>
        <!-- /.row -->
    </div>
  </section>  
    
    
     <script type="text/javascript">
    //Proses Controller logic ajax

    function cekemail(a) {
    re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(a);
    }

    function hanyaAngka(evt) {
      var charCode = (evt.which) ? evt.which : event.keyCode
       if (charCode > 31 && (charCode < 48 || charCode > 57))
 
        return false;
      return true;
    } 


    function cekpassword(a) {
    re2 = /^\S{3,}$/;
    return re2.test(a);
    }

    $('#form-update').submit(function(e) {
      
        var error = 0;
        var message = "";

        var data = $(this).serialize();

        var kecamatan = $("#kecamatan").val();
        var kecamatan = kecamatan.trim();

          if (error == 0) {
            if (kecamatan.length == 0) {
                error++;
                message = "Nama Kecamatan wajib di isi.";
            }
        }

        var kode_kecamatan = $("#kode_kecamatan").val();
        var kode_kecamatan = kode_kecamatan.trim();

          if (error == 0) {
            if (kode_kecamatan.length == 0) {
                error++;
                message = "Kode kecamatan wajib di isi.";
            }
        }

        var kabupaten = $("#kabupaten").val();
        var kabupaten = kabupaten.trim();

          if (error == 0) {
            if (kabupaten.length == 0) {
                error++;
                message = "Kabupaten wajib di isi.";
            }
        }

      
            if (error == 0) {
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                $(".loading2").show();
                $(".loading2").modal('show');
                },
                url: '<?php echo base_url('Category/Kecamatan/prosesUpdate'); ?>',
                type:"post",
                data:new FormData(this),
                processData:false,
                contentType:false,
                cache:false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == true) {
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    swal("Success", result.pesan, "success");
                } else {
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    swal("Warning", result.pesan, "warning");
                }
            })
            e.preventDefault();
        } else {
            toastr.error(message,'Warning', {timeOut: 5000},toastr.options = {
             "closeButton": true}); 
            return false;

        }
    });

     // untuk select2 ajak pilih department
    $(function () 
    {
    $(".selek-komoditi").select2({
        placeholder: " -- Pilih kabupaten -- "
        });
    });


  </script>