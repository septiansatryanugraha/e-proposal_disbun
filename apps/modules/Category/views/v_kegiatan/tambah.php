<?php $this->load->view('_heading/_headerContent') ?>

<style>
    #osas {
        color:red;
        font-weight:bold;
        margin-left:0px;
    }

    .field-icon {
        float: left;
        margin-left: 93%;
        margin-top: -25px;
        position: relative;
        z-index: 2;
    }


</style>

<section class="content">


    <!-- style loading -->
    <div class="loading2"></div>
    <!-- -->

    <div class="box">
        <div class="row">
            <div class="col-md-8">
                <!-- form start -->
                <form class="form-horizontal" id="form-tambah" method="POST">
                    <input type="hidden" name="created_by" value="<?php echo $userdata->nama; ?>">

                    <div class="box-body">

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Jenis Kegiatan</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" name="jenis_kegiatan" placeholder="jenis kegiatan" id="jenis" aria-describedby="sizing-addon2">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Program</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" name="program" placeholder="program" id="program" aria-describedby="sizing-addon2">
                            </div>
                        </div>



                        <div class="form-group">
                            <label class="col-sm-2 control-label">Jenis Bantuan</label>
                            <div class="col-sm-7">
                                <select name="jenis_bantuan" class="form-control selek-komoditi" id="jenis_bantuan">
                                    <option></option>
                                    <?php
                                    foreach ($jenis_bantuan as $data) {

                                        ?>
                                        <option value="<?php echo $data->id; ?>">
                                            <?php echo $data->jenis_bantuan; ?>
                                        </option>
                                        <?php
                                    }

                                    ?>
                                </select>
                            </div>
                        </div> 




                    </div>
                    <div class="box-footer">
                        <button name="submit" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Simpan</button>
                        <a class="klik ajaxify" href="<?php echo site_url('kegiatan'); ?>"><button class="btn btn-primary btn-flat" ><i class="fa fa-arrow-left"></i> Kembali</button></a>
                    </div>
                </form>

            </div>
            <!-- /.box -->

        </div>
        <!-- /.row -->
    </div>

</section>


<script type="text/javascript">
//Proses Controller logic ajax

    function cekemail(a) {
        re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        return re.test(a);
    }

    function cekpassword(a) {
        re2 = /^\S{3,}$/;
        return re2.test(a);
    }

    $('#form-tambah').submit(function (e) {

        var error = 0;
        var message = "";

        var data = $(this).serialize();

        var jenis = $("#jenis").val();
        var jenis = jenis.trim();

        if (error == 0) {
            if (jenis.length == 0) {
                error++;
                message = "Jenis Kegiatan wajib di isi.";
            }
        }

        var program = $("#program").val();
        var program = program.trim();

        if (error == 0) {
            if (program.length == 0) {
                error++;
                message = "Program kegiatan wajib di isi.";
            }
        }

        var jenis_bantuan = $("#jenis_bantuan").val();
        var jenis_bantuan = jenis_bantuan.trim();

        if (error == 0) {
            if (jenis_bantuan.length == 0) {
                error++;
                message = "Jenis Bantuan wajib di isi.";
            }
        }


        if (error == 0) {
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $(".loading2").show();
                    $(".loading2").modal('show');
                },
                url: '<?php echo base_url('Category/Kegiatan/prosesAdd'); ?>',
                type: "post",
                data: new FormData(this),
                processData: false,
                contentType: false,
                cache: false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == true) {
                    document.getElementById("form-tambah").reset();
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    swal("Success", result.pesan, "success");
                } else {
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    swal("Warning", result.pesan, "warning");
                }
            })
            e.preventDefault();
        } else {
            toastr.error(message, 'Warning', {timeOut: 5000}, toastr.options = {
                "closeButton": true});
            return false;

        }
    });


    // untuk select2 ajak pilih department
    $(function ()
    {
        $(".selek-komoditi").select2({
            placeholder: " -- Pilih jenis bantuan -- "
        });
    });



</script>





