<?php $this->load->view('_heading/_headerContent') ?>

<style>
#osas {
color:red;
font-weight:bold;
margin-left:0px;
}

  .field-icon {
  float: left;
  margin-left: 93%;
  margin-top: -25px;
  position: relative;
  z-index: 2;
 }


</style>
    
<section class="content">
    
    
    <!-- style loading -->
    <div class="loading2"></div>
    <!-- -->
    
    <div class="box">
        <div class="row">
        <div class="col-md-8" id="newContain">
        <!-- form start -->
        <form class="form-horizontal" id="form-tambah" method="POST">
       <input type="hidden" name="created_by" value="<?php echo $userdata->nama; ?>">
      
        <div class="box-body">
        <div class="form-group">      
        <label for="inputFoto" class="col-sm-2 control-label">File Form Pengajuan</label>
        <div class="col-sm-7">
        <input type="file" class="form-control" name="path" id="path">
        <p style='color: red; font-size: 14px;'> *Maksimal File 2 MB</p>
        </div>
        </div>



        </div>
        <div class="box-footer">
        <button name="submit" id="simpan" type="submit" class="btn btn-success btn-flat" onClick="form-tambah()";><i class="fa fa-save"></i> Simpan</button>
        <button type="reset" class="btn btn-warning btn-flat"><i class="fa fa-retweet"></i> Cancel</button>
        </div>
        </form>
      
        </div>
        <!-- /.box -->
       
        </div>
          <!-- /.row -->
    </div>

    </section>


  <script type="text/javascript">
    //Proses Controller logic ajax

    $('#form-tambah').submit(function(e) {
      
        var error = 0;
        var message = "";

         var data = $(this).serialize();

        var path = $("#path").val();
        var path = path.trim();

          if (error == 0) {
            if (path.length == 0) {
                error++;
                message = "File wajib di isi.";
            }

        }


        if (error == 0) {
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                $(".loading2").show();
                $(".loading2").modal('show');
                },
                url: '<?php echo base_url('Category/File_upload/prosesAdd'); ?>',
                type:"post",
                data:new FormData(this),
                processData:false,
                contentType:false,
                cache:false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == 'berhasil') {
                    document.getElementById("form-tambah").reset();
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    save_berhasil();
                    setTimeout(location.reload.bind(location), 500);

                }  else {
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    gagal();
                    
                }
            })
            e.preventDefault();
        } else {
            swal("Peringatan", message, "warning");
            return false;
        }
    });

  </script>


  
    
    

    
    