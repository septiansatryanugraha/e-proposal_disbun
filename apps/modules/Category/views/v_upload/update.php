	<?php $this->load->view('_heading/_headerContent') ?>
	
	
	<section class="content">
		<!-- style loading -->
		<div class="loading2"></div>
		<!-- -->
		
		<div class="box">
		<form id="form-update" class="form-horizontal" method="POST">
        <input type="hidden" name="id_file" value="<?php echo $brand->id_file; ?>">
		<input type="hidden" name="updated_by" value="<?php echo $userdata->nama; ?>">
        <div class="row">
        <div class="col-md-9">

        <div class="box-header with-border">
        <h3 class="box-title">Update Data</h3>
        </div>
			
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body">
	
        <div class="form-group">      
        <label for="inputFoto" class="col-sm-2 control-label">File Form Pengajuan</label>
        <div class="col-sm-7">
        <input type="file" class="form-control" name="path" id="path">
        <p style='color: red; font-size: 14px;'> *Maksimal File 2 MB</p>
          <a href="<?php echo base_url().$brand->path; ?>"><b><font face="raleway" size="2" color="blue"><i class="fa fa-file-word-o" aria-hidden="true"></i> <?php echo $brand->nama_file; ?></font></b></a>
        </div>
        </div>
					
        </div>
        <!-- /.box-body -->
		
		<div class="box-footer">
        <button name="simpan" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Update</button>
         <a class="klik ajaxify" href="<?php echo site_url('file-pengajuan'); ?>"><button class="btn btn-primary btn-flat" ><i class="fa fa-arrow-left"></i> Kembali</button></a>
        </div>
        </form>
        </div>
        <!-- /.box -->
        </div>
        <!-- /.row -->
		</div>
		
		</section>
		
		
 <script type="text/javascript">
    //Proses Controller logic ajax

    $('#form-update').submit(function(e) {
      
        var error = 0;
        var message = "";

         var data = $(this).serialize();

        var path = $("#path").val();
        var path = path.trim();

          if (error == 0) {
            if (path.length == 0) {
                error++;
                message = "File wajib di isi.";
            }

        }

        if (error == 0) {
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                $(".loading2").show();
                $(".loading2").modal('show');
                },
                url: '<?php echo base_url('Category/File_upload/prosesUpdate'); ?>',
                type:"post",
                data:new FormData(this),
                processData:false,
                contentType:false,
                cache:false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == 'berhasil') {
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    save_berhasil();

                }  else {
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    gagal();
                    
                }
            })
            e.preventDefault();
        } else {
            swal("Peringatan", message, "warning");
            return false;
        }
    });

  </script>
	
	
