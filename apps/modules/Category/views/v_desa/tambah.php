<?php $this->load->view('_heading/_headerContent') ?>

<style>
#osas {
color:red;
font-weight:bold;
margin-left:0px;
}

  .field-icon {
  float: left;
  margin-left: 93%;
  margin-top: -25px;
  position: relative;
  z-index: 2;
 }

 #loadingImg
 {
 margin-left: 17%;
 }

  .font-loading
{
  font-family: Futura,Trebuchet MS,Arial,sans-serif; 
  color:red;
  font-size: 14px;
}


</style>
		
<section class="content">
    
    
    <!-- style loading -->
    <div class="loading2"></div>
    <!-- -->
    
    <div class="box">
        <div class="row">
        <div class="col-md-8">
        <!-- form start -->
        <form class="form-horizontal" id="form-tambah" method="POST">
        <input type="hidden" name="created_by" value="<?php echo $userdata->nama; ?>">
      
        <div class="box-body">

    <div class="form-group">
     <label class="col-sm-2 control-label">Kabupaten</label>
     <div class="col-sm-4">
     <select name="" class="form-control selek-dinas" id="kabupaten">
     <option></option>
     <?php foreach ($kabupaten as $data) { ?>
     <option value="<?php echo $data->kode; ?>">
     <?php echo $data->kabupaten; ?>
     </option>
     <?php } ?>
     </select>
     </div>
     </div> 

     <div id="loadingImg">
     <img src="<?php echo base_url().'assets/' ?>tambahan/gambar/loading-bubble.gif">
     <p class="font-loading">Proccessing Data</font></p>
     </div>

     <input type="hidden" class="form-control"  name="kabupaten" id="nama_kab" aria-describedby="sizing-addon2">

     <div class="form-group">
     <label class="col-sm-2 control-label">Kecamatan</label>
     <div class="col-sm-4">
     <select select name="kecamatan" class="form-control selek-kecamatan" id="kecamatan">
     <option></option>
     </select>
     </div>
     </div> 

     <div class="form-group">
     <label for="inputEmail3" class="col-sm-2 control-label">Desa</label>
     <div class="col-sm-5">
     <input type="text" class="form-control" name="desa" placeholder="Nama Desa" id="desa" aria-describedby="sizing-addon2">
     </div>
     </div>

               
        </div>
        <div class="box-footer">
        <button name="submit" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Simpan</button>
        <a class="klik ajaxify" href="<?php echo site_url('master-desa'); ?>"><button class="btn btn-primary btn-flat" ><i class="fa fa-arrow-left"></i> Kembali</button></a>
        </div>
        </form>
      
        </div>
        <!-- /.box -->
       
        </div>
          <!-- /.row -->
    </div>

    </section>


    <script type="text/javascript">
    //Proses Controller logic ajax

    function cekemail(a) {
    re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(a);
    }

    function cekpassword(a) {
    re2 = /^\S{3,}$/;
    return re2.test(a);
    }

    $('#form-tambah').submit(function(e) {
      
        var error = 0;
        var message = "";

        var data = $(this).serialize();

        var kecamatan = $("#kecamatan").val();
        var kecamatan = kecamatan.trim();

          if (error == 0) {
            if (kecamatan.length == 0) {
                error++;
                message = "Kecamatan wajib di isi.";
            }
        }

        var desa = $("#desa").val();
        var desa = desa.trim();

          if (error == 0) {
            if (desa.length == 0) {
                error++;
                message = "Nama desa wajib di isi.";
            }
        }

      
    if (error == 0) {
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                $(".loading2").show();
                $(".loading2").modal('show');
                },
                url: '<?php echo base_url('Category/Desa/prosesAdd'); ?>',
                type:"post",
                data:new FormData(this),
                processData:false,
                contentType:false,
                cache:false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == true) {
                    document.getElementById("form-tambah").reset();
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    swal("Success", result.pesan, "success");
                    setTimeout(location.reload.bind(location), 500);
                } else {
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    swal("Warning", result.pesan, "warning");
                }
            })
            e.preventDefault();
        } else {
            toastr.error(message,'Warning', {timeOut: 5000},toastr.options = {
             "closeButton": true}); 
            return false;

        }
    });


      // untuk select2 ajak pilih department
    $(function () 
    {
    $(".selek-dinas").select2({
        placeholder: " -- Pilih Kabupaten -- "
        });
    });

      // untuk select2 ajak pilih department
    $(function () 
    {
    $(".selek-kecamatan").select2({
        placeholder: " -- Pilih Kecamatan -- "
        });
    });


  </script>


  <script type="text/javascript">
    $(function(){
    $("#loadingImg").hide();
    $("#loadingImg2").hide();

    $.ajaxSetup({
    type:"POST",
    url: "<?php echo base_url('Category/Desa/ambil_data') ?>",
    cache: false,
    });

    // custom untuk input

    $("#kabupaten").change(function(){
    var value=$(this).val();
    console.log(value);
    if(value>0){
    $.ajax({
    beforeSend: function (){
    $("#loadingImg").fadeIn();
        },
    data:{modul:'kecamatan',kode:value},
    success: function(respond){
    $("#kecamatan").html(respond);
    $("#loadingImg").fadeOut();  
    console.log(respond);
    }
    })
    }
    });
    
    
    })
  </script>

		
		

		
		