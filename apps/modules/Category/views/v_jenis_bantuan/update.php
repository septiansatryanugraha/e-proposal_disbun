<?php $this->load->view('_heading/_headerContent') ?>

<style>
    .field-icon {
        float: left;
        margin-left: 93%;
        margin-top: -25px;
        position: relative;
        z-index: 2;
    }

</style>


<?php
$password = $brand->password;
$dec_pass = base64_decode($password);

?>


<section class="content">

    <!-- style loading -->
    <div class="loading2"></div>
    <!-- -->

    <div class="box">
        <form class="form-horizontal" id="form-update" method="POST">
            <input type="hidden"  name="id" value="<?php echo $brand->id; ?>">
            <input type="hidden"  name="nama" value="<?php echo $brand->nama; ?>">

            <div class="row">
                <div class="col-md-10">

                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body">


                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Jenis Bantuan</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="jenis_bantuan" placeholder="Jenis Bantuan" id="jenis" value="<?php echo $brand->jenis_bantuan; ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Sub Kegiatan</label>
                            <div class="col-sm-6">
                                <select name="sub_kegiatan" class="form-control selek-kegiatan" id="sub_kegiatan">
                                    <?php foreach ($sub_kegiatan as $data) { ?>
                                        <option value="<?php echo $data->id; ?>"<?php
                                        if ($data->id == $brand->id_sub_kegiatan) {
                                            echo "selected='selected'";
                                        }

                                        ?>>
                                                    <?php echo $data->jenis_kegiatan; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Satuan</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="satuan" placeholder="satuan" id="satuan" value="<?php echo $brand->satuan; ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Komoditi</label>
                            <div class="col-sm-3">
                                <select name="komoditi" class="form-control selek-komoditi" id="komoditi" aria-describedby="sizing-addon2">
                                    <?php foreach ($komoditi as $data) { ?>
                                        <option value="<?php echo $data->id_komoditi; ?>"<?php
                                        if ($data->id_komoditi == $brand->id_komoditi) {
                                            echo "selected='selected'";
                                        }

                                        ?>>
                                                    <?php echo $data->nama; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>


                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button name="simpan" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Update</button>
                        <a class="klik ajaxify" href="<?php echo site_url('jenis-bantuan'); ?>"><button class="btn btn-primary btn-flat" ><i class="fa fa-arrow-left"></i> Kembali</button></a>
                    </div>
                    </form>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.row -->
    </div>
</section>  


<script type="text/javascript">
//Proses Controller logic ajax

    function cekemail(a) {
        re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        return re.test(a);
    }

    function cekpassword(a) {
        re2 = /^\S{3,}$/;
        return re2.test(a);
    }


    $('#form-update').submit(function (e) {

        var error = 0;
        var message = "";

        var data = $(this).serialize();

        var jenis = $("#jenis").val();
        var jenis = jenis.trim();

        if (error == 0) {
            if (jenis.length == 0) {
                error++;
                message = "Jenis Bantuan wajib di isi.";
            }
        }

        var sub_kegiatan = $("#sub_kegiatan").val();
        var sub_kegiatan = sub_kegiatan.trim();

        if (error == 0) {
            if (sub_kegiatan.length == 0) {
                error++;
                message = "Sub Kegiatan wajib di isi.";
            }
        }

        var satuan = $("#satuan").val();
        var satuan = satuan.trim();

        if (error == 0) {
            if (satuan.length == 0) {
                error++;
                message = "Satuan wajib di isi.";
            }
        }

        var komoditi = $("#komoditi").val();
        var komoditi = komoditi.trim();

        if (error == 0) {
            if (komoditi.length == 0) {
                error++;
                message = "Komoditi wajib di isi.";
            }
        }

        if (error == 0) {

            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $(".loading2").show();
                    $(".loading2").modal('show');
                },
                url: '<?php echo base_url('Category/Jenis_bantuan/prosesUpdate'); ?>',
                type: "post",
                data: new FormData(this),
                processData: false,
                contentType: false,
                cache: false,
            }).done(function (data) {

                var result = jQuery.parseJSON(data);
                if (result.status == true) {

                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    swal("Success", result.pesan, "success");
                    setTimeout(location.reload.bind(location), 500);

                } else {
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    swal("Warning", result.pesan, "warning");

                }

                console.log(result);
            })
            e.preventDefault();
        } else {
            toastr.error(message, 'Warning', {timeOut: 5000}, toastr.options = {
                "closeButton": true});
            return false;

        }
    });

// untuk select2 ajak pilih department
    $(function ()
    {
        $(".selek-komoditi").select2({
            placeholder: " -- Pilih komoditi -- "
        });
    });

// untuk select2 ajak pilih department
    $(function ()
    {
        $(".selek-kegiatan").select2({
            placeholder: " -- Pilih sub kegiatan -- "
        });
    });


</script>
