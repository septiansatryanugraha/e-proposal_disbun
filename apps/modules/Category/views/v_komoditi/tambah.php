<?php $this->load->view('_heading/_headerContent') ?>

<style>
    #osas {
        color:red;
        font-weight:bold;
        margin-left:0px;
    }

    .field-icon {
        float: left;
        margin-left: 93%;
        margin-top: -25px;
        position: relative;
        z-index: 2;
    }
</style>

<section class="content">
    <!-- style loading -->
    <div class="loading2"></div>
    <!-- -->
    <div class="box">
        <div class="row">
            <div class="col-md-8">
                <!-- form start -->
                <form class="form-horizontal" id="form-tambah" method="POST">
                    <input type="hidden" name="created_by" value="<?php echo $userdata->nama; ?>">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Hak Akses Admin</label>
                            <div class="col-sm-7">
                                <select name="grup_id" class="selek-akses" id="grup_id">
                                    <option disabled selected></option>
                                    <?php foreach ($group as $key => $value) { ?>
                                        <option value="<?php echo $value->grup_id ?>"><?php echo $value->nama_grup ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Komoditi</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" name="nama" placeholder="Nama Komoditi" id="nama_kom" aria-describedby="sizing-addon2">
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button name="submit" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Simpan</button>
                        <a class="klik ajaxify" href="<?php echo site_url('komoditi'); ?>"><button class="btn btn-primary btn-flat" ><i class="fa fa-arrow-left"></i> Kembali</button></a>
                    </div>
                </form>
            </div>
            <!-- /.box -->
        </div>
        <!-- /.row -->
    </div>
</section>

<script type="text/javascript">
//Proses Controller logic ajax

    function cekemail(a) {
        re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        return re.test(a);
    }

    function cekpassword(a) {
        re2 = /^\S{3,}$/;
        return re2.test(a);
    }

    $('#form-tambah').submit(function (e) {

        var error = 0;
        var message = "";

        var data = $(this).serialize();

        var nama_kom = $("#nama_kom").val();
        var nama_kom = nama_kom.trim();

        if (error == 0) {
            if (nama_kom.length == 0) {
                error++;
                message = "Nama Komoditi wajib di isi.";
            }
        }


        if (error == 0) {
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $(".loading2").show();
                    $(".loading2").modal('show');
                },
                url: '<?php echo base_url('Category/Komoditi/prosesAdd'); ?>',
                type: "post",
                data: new FormData(this),
                processData: false,
                contentType: false,
                cache: false,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == true) {
                    document.getElementById("form-tambah").reset();
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    swal("Success", result.pesan, "success");
                } else {
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    swal("Warning", result.pesan, "warning");
                }
            })
            e.preventDefault();
        } else {
            toastr.error(message, 'Warning', {timeOut: 5000}, toastr.options = {
                "closeButton": true});
            return false;
        }
    });

      // untuk select2 ajak pilih department
    $(function () 
    {
    $(".selek-akses").select2({
        placeholder: " -- Pilih akses -- "
        });
    });

</script>





