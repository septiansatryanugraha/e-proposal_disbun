<?php $this->load->view('_heading/_headerContent') ?>

<style>
    .field-icon {
        float: left;
        margin-left: 93%;
        margin-top: -25px;
        position: relative;
        z-index: 2;
    }

</style>

<?php
$password = $brand->password;
$dec_pass = base64_decode($password);

?>

<section class="content">
    <!-- style loading -->
    <div class="loading2"></div>
    <!-- -->
    <div class="box">
        <form class="form-horizontal" id="form-update" method="POST">
            <input type="hidden"  name="id_komoditi" value="<?php echo $brand->id_komoditi; ?>">
            <input type="hidden"  name="nama" value="<?php echo $brand->nama; ?>">
            <div class="row">
                <div class="col-md-10">
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Hak Akses Admin</label>
                            <div class="col-sm-7">
                                <select name="grup_id" class="selek-akses" id="grup_id">
                                    <?php foreach ($group as $key => $value) { ?>
                                        <option value="<?php echo $value->grup_id ?>" <?php echo ($value->grup_id == $brand->grup_id) ? 'selected' : '' ?>><?php echo $value->nama_grup ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Komoditi</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="nama" placeholder="Nama Komoditi" id="nama_kom" value="<?php echo $brand->nama; ?>">
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button name="simpan" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Update</button>
                        <a class="klik ajaxify" href="<?php echo site_url('komoditi'); ?>"><button class="btn btn-primary btn-flat" ><i class="fa fa-arrow-left"></i> Kembali</button></a>
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </form>
        <!-- /.row -->
    </div>
</section>  

<script type="text/javascript">
//Proses Controller logic ajax

    function cekemail(a) {
        re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        return re.test(a);
    }

    function cekpassword(a) {
        re2 = /^\S{3,}$/;
        return re2.test(a);
    }


    $('#form-update').submit(function (e) {

        var error = 0;
        var message = "";

        var data = $(this).serialize();

        var nama_kom = $("#nama_kom").val();
        var nama_kom = nama_kom.trim();

        if (error == 0) {
            if (nama_kom.length == 0) {
                error++;
                message = "Nama Komoditi wajib di isi.";
            }
        }

        if (error == 0) {

            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $(".loading2").show();
                    $(".loading2").modal('show');
                },
                url: '<?php echo base_url('Category/Komoditi/prosesUpdate'); ?>',
                type: "post",
                data: new FormData(this),
                processData: false,
                contentType: false,
                cache: false,
            }).done(function (data) {

                var result = jQuery.parseJSON(data);
                if (result.status == true) {

                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    swal("Success", result.pesan, "success");
                    setTimeout(location.reload.bind(location), 500);

                } else {
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    swal("Warning", result.pesan, "warning");

                }

                console.log(result);
            })
            e.preventDefault();
        } else {
            toastr.error(message, 'Warning', {timeOut: 5000}, toastr.options = {
                "closeButton": true});
            return false;

        }
    });

  // untuk select2 ajak pilih department
    $(function () 
    {
    $(".selek-akses").select2({
        placeholder: " -- Pilih akses -- "
        });
    });

</script>
