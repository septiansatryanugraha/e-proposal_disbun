	<?php $this->load->view('_heading/_headerContent') ?>
	
	
	<section class="content">
		<!-- style loading -->
		<div class="loading2"></div>
		<!-- -->
		
		<div class="box">
		<form id="form-update" class="form-horizontal" method="POST">
        <input type="hidden" name="id_bantuan" value="<?php echo $brand->id_bantuan; ?>">
		<input type="hidden" name="updated_by" value="<?php echo $userdata->nama; ?>">
        <div class="row">
        <div class="col-md-9">

        <div class="box-header with-border">
        <h3 class="box-title">Update Data</h3>
        </div>
			
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body">
	
         <div class="form-group">
         <label for="inputEmail3" class="col-sm-2 control-label">Judul Bantuan </label>
         <div class="col-sm-7">
         <input type="text" class="form-control" name="judul_bantuan" id="judul_bantuan" value="<?php echo $brand->judul_bantuan; ?>">
         </div>
         </div>

        <div class="form-group">
        <label class="col-sm-2 control-label">Konten</label>
        <div class="col-sm-7">
        <textarea id="summernote" name="konten" rows="10" cols="80"><?php echo $brand->konten;?>
        </textarea>
        </div>
        </div>
					
        </div>
        <!-- /.box-body -->
		
		<div class="box-footer">
        <button name="simpan" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Update</button>
         <a class="klik ajaxify" href="<?php echo site_url('bantuan'); ?>"><button class="btn btn-primary btn-flat" ><i class="fa fa-arrow-left"></i> Kembali</button></a>
        </div>
        </form>
        </div>
        <!-- /.box -->
        </div>
        <!-- /.row -->
		</div>
		
		</section>
		
		
	<script type="text/javascript">
    //Proses Controller logic ajax
    $('#form-update').submit(function(e) {
        var error = 0;
        var message = "";

        var data = $(this).serialize();

        var judul_bantuan = $("#judul_bantuan").val();
        var judul_bantuan = judul_bantuan.trim();
        
        if (error == 0) {
            if (judul_bantuan.length == 0) {
                error++;
                message = "Judul Bantuan wajib di isi.";
            }
        }

        var summernote = $("#summernote").val();
        var summernote = summernote.trim();
        
        if (error == 0) {
            if (summernote.length == 0) {
                error++;
                message = "Konten wajib di isi.";
            }
        }
      
        if (error == 0) {
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $(".loading2").show();
                    $(".loading2").modal('show');
                },
                url: '<?php echo site_url('Category/Bantuan/prosesUpdate'); ?>',
                data: data,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == true) {
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    swal("Success", result.pesan, "success");
                } else {
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    swal("Warning", result.pesan, "warning");
                }
                })
            e.preventDefault();
        } else {
            swal("Peringatan", message, "warning");
            return false;
        }
    });

      // text editor summernote
    
    $(document).ready(function() {
        $('#summernote').summernote({
            height: 200
        });
    });

</script>
	
	
