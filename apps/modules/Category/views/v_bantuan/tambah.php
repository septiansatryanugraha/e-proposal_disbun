 <?php $this->load->view('_heading/_headerContent') ?>

<style>
#osas {
color:red;
font-weight:bold;
margin-left:0px;
}

</style>
		
		<section class="content">
		<!-- style loading -->
		<div class="loading2"></div>
		<!-- -->
		
		<div class="box">
        <div class="row">
        <div class="col-md-10" id="newContain">
       
        <div class="box-header with-border">
        <h3 class="box-title">Tambah Data</h3>
         </div>
			
            <!-- /.box-header -->
            <!-- form start -->
			
			<form class="form-horizontal" id="form-tambah" method="POST">
			<input type="hidden" name="created_by" value="<?php echo $userdata->nama; ?>">
			
			<div class="box-body">
            
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Judul Bantuan </label>
                  <div class="col-sm-6">
                  <input type="text" class="form-control" name="judul_bantuan" id="judul_bantuan" aria-describedby="sizing-addon2">
                  </div>
                </div>

                <div class="form-group">
            <label class="col-sm-2 control-label">Konten</label>
            <div class="col-sm-6">
            <textarea id="summernote" name="konten"></textarea>
            </div>
            </div>
				
              </div>
              <div class="box-footer">
              <button name="simpan" id="simpan" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Simpan</button>
             
               <a class="klik ajaxify" href="<?php echo site_url('kategori-opd'); ?>"><button class="btn btn-primary btn-flat" ><i class="fa fa-arrow-left"></i> Kembali</button></a>
			  </div>
            </form>
			
          </div>
          <!-- /.box -->
       
          </div>
          <!-- /.row -->
		</div>

		</section>
		
		
	
<script type="text/javascript">
    //Proses Controller logic ajax
    $('#form-tambah').submit(function(e) {
        var error = 0;
        var message = "";

        var data = $(this).serialize();

        var judul_bantuan = $("#judul_bantuan").val();
        var judul_bantuan = judul_bantuan.trim();
        
        if (error == 0) {
            if (judul_bantuan.length == 0) {
                error++;
                message = "Judul Bantuan wajib di isi.";
            }
        }

        var summernote = $("#summernote").val();
        var summernote = summernote.trim();
        
        if (error == 0) {
            if (summernote.length == 0) {
                error++;
                message = "Konten wajib di isi.";
            }
        }
      
        if (error == 0) {
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $(".loading2").show();
                    $(".loading2").modal('show');
                },
                url: '<?php echo site_url('Category/Bantuan/prosesAdd'); ?>',
                data: data,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == true) {
                    document.getElementById("form-tambah").reset();
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    swal("Success", result.pesan, "success");
                } else {
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    swal("Warning", result.pesan, "warning");
                }
            })
            e.preventDefault();
        } else {
            toastr.error(message,'Warning', {timeOut: 5000},toastr.options = {
             "closeButton": true}); 
            return false;

        }
    });

    // text editor summernote
    
    $(document).ready(function() {
        $('#summernote').summernote({
            height: 200
        });
    });
     

</script>
	
		
		

		
		