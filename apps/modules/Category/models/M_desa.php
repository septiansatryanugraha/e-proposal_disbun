<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_desa extends CI_Model {

	const __tableName = 'tbl_desa';
	const __tableId   = 'id';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function get_data() {
        $this->db->from(self::__tableName);
        $this->db->order_by(self::__tableId,'desc');
        $data = $this->db->get();
        return $data->result();
    }
	
	public function selectById($id) {
        $sql = "SELECT * FROM " . self::__tableName . " WHERE " . self::__tableId . " = '{$id}'";
        $data = $this->db->query($sql);
        return $data->row();
    }

	
	public function update($data,$where) {
		$result= $this->db->update(self::__tableName,$data,$where);
	    return $result;
	}
	

	public function hapus($id) {
		$sql = "DELETE FROM " . self::__tableName . " WHERE  ". self::__tableId . " = '{$id}'";

		$this->db->query($sql);

		return $this->db->affected_rows();
	}

	public function selekKab() {
        $this->db->from('tbl_kabupaten');
        $data = $this->db->get();
        return $data->result();
    }

    public function selekKec() {
        $this->db->from('tbl_kecamatan');
        $data = $this->db->get();
        return $data->result();
    }

	public function select_kecamatan($kode){

    $res="<option value='0'>-- Pilih Kecamatan --</option>";
    $this->db->order_by('id','ASC');
    $kab= $this->db->get_where('tbl_kecamatan',array('kode_kabupaten'=>$kode));

    foreach ($kab->result_array() as $data ){
    $kabupaten.= "<option value='$data[kecamatan]'>$data[kecamatan]</option>";
    }
    return $kabupaten;
    }
    
	
	
}
