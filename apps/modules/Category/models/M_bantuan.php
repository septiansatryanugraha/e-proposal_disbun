<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_bantuan extends CI_Model {

	const __tableName = 'tbl_bantuan';
    const __tableId = 'id_bantuan';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function get_data() {
        $this->db->from(self::__tableName);
        $this->db->order_by(self::__tableId,'Desc');
        $data = $this->db->get();
        return $data->result();
    }
	
	public function selectById($id) {
        $sql = "SELECT * FROM " . self::__tableName . " WHERE " . self::__tableId . " = '{$id}'";
        $data = $this->db->query($sql);
        return $data->row();
    }

	
}
