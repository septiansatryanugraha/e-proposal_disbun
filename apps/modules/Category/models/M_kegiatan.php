<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_kegiatan extends CI_Model
{
    const __tableName = 'tbl_kegiatan';
    const __tableId = 'id';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function get_data()
    {
        $groupId = $this->session->userdata('grup_id');
        $tahunSession = isset($_SESSION['tahun']) ? $_SESSION['tahun'] : date('Y');
        $sql = "SELECT " . self::__tableName . ".*, tbl_jenis_bantuan.jenis_bantuan as jenis_bantuan";
        $sql .= " FROM " . self::__tableName . " as " . self::__tableName;
        $sql .= " LEFT JOIN tbl_jenis_bantuan ON tbl_jenis_bantuan.id = " . self::__tableName . ".id_jenis_bantuan";
        $sql .= " WHERE YEAR(" . self::__tableName . ".created_date) = '{$tahunSession}'";
        $sql .= " ORDER BY id DESC";
        $data = $this->db->query($sql);
        return $data->result();
    }

    public function selectById($id)
    {
        $sql = "SELECT * FROM " . self::__tableName . " WHERE " . self::__tableId . " = '{$id}'";
        $data = $this->db->query($sql);
        return $data->row();
    }

    public function update($data, $where)
    {
        $result = $this->db->update(self::__tableName, $data, $where);
        return $result;
    }

    public function hapus($id)
    {
        $sql = "DELETE FROM " . self::__tableName . " WHERE  " . self::__tableId . " = '{$id}'";

        $this->db->query($sql);

        return $this->db->affected_rows();
    }

    public function selekBantuan()
    {
        $this->db->from('tbl_jenis_bantuan');
        $data = $this->db->get();
        return $data->result();
    }
}
