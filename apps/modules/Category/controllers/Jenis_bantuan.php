<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis_bantuan extends AUTH_Controller
{
    const __tableName = 'tbl_jenis_bantuan';
    const __tableId = 'id';
    const __folder = 'v_jenis_bantuan/';
    const __kode_menu = 'jenis-bantuan';
    const __title = 'Jenis Bantuan';
    const __model = 'M_jenis_bantuan';

    public function __construct()
    {
        parent::__construct();
        $this->load->model(self::__model);
        $this->load->model('M_sidebar');
    }

    public function loadkonten($page, $data)
    {

        $data['userdata'] = $this->userdata;
        $ajax = ($this->input->post('status_link') == "ajax" ? true : false);
        if (!$ajax) {
            $this->load->view('Dashboard/layouts/header', $data);
        }
        $this->load->view($page, $data);
        if (!$ajax)
            $this->load->view('Dashboard/layouts/footer', $data);
    }

    public function index()
    {
        $accessAdd = $this->M_sidebar->access('add', self::__kode_menu);
        $data['accessAdd'] = $accessAdd->menuview;
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = self::__title;

        $this->loadkonten('' . self::__folder . 'home', $data);
    }

    public function ajax_list()
    {
        $tanggalAwal = $this->input->post('tanggal_awal');
        $tanggalAkhir = $this->input->post('tanggal_akhir');

        $filter = array(
            'tanggal_awal' => $tanggalAwal,
            'tanggal_akhir' => $tanggalAkhir,
        );

        $accessEdit = $this->M_sidebar->access('edit', self::__kode_menu);
        $accessDel = $this->M_sidebar->access('del', self::__kode_menu);
        $list = $this->M_jenis_bantuan->get_data();

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $brand) {

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $brand->sub_kegiatan;
            $row[] = $brand->jenis_bantuan;
            $row[] = $brand->satuan;
            $row[] = $brand->komoditi;
            //add html for action
            $buttonEdit = '';
            if ($accessEdit->menuview > 0) {
                $buttonEdit = anchor('edit-jenis-bantuan/' . $brand->id, ' <span tooltip="Edit Data"><span class="fa fa-edit" ></span>', ' class="btn btn-sm btn-primary klik ajaxify" ');
            }
            $buttonDel = '';
            if ($accessDel->menuview > 0) {
                $buttonDel = '<button class="btn btn-sm btn-danger hapus-bantuan" data-id=' . "'" . $brand->id . "'" . '><span tooltip="Hapus Data"><i class="glyphicon glyphicon-trash"></i></button>';
            }

            $row[] = $buttonEdit . '  ' . $buttonDel;
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function Add()
    {

        /* ini harus ada boss */
        $data['userdata'] = $this->session->userdata('nama');
        $access = $this->M_sidebar->access('add', self::__kode_menu);
        if ($access->menuview == 0) {
            $data['page'] = self::__title;
            $data['judul'] = self::__title;
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        }

        /* ini harus ada boss */ else {
            $data['page'] = self::__title;
            $data['judul'] = self::__title;
            $data['komoditi'] = $this->M_jenis_bantuan->selekKomoditi();
            $data['sub_kegiatan'] = $this->M_jenis_bantuan->selekSub();

            $this->loadkonten('' . self::__folder . 'tambah', $data);
        }
    }

    public function prosesAdd()
    {

        $username = $this->session->userdata('nama');
        $date = date('Y-m-d H:i:s');

        $this->db->trans_begin();

        $access = $this->M_sidebar->access('add', self::__kode_menu);
        if ($access->menuview == 0) {
            $out = array('status' => false, 'pesan' => 'You dont have access.');
        } else {

            $data = array(
                'id_sub_kegiatan' => $this->input->post('sub_kegiatan'),
                'id_komoditi' => $this->input->post('komoditi'),
                'jenis_bantuan' => $this->input->post('jenis_bantuan'),
                'satuan' => $this->input->post('satuan'),
                'created_by' => $username,
                'created_date' => $date,
            );

            $result = $this->db->insert(self::__tableName, $data);

            if ($this->db->trans_status() === FALSE) {
                $out = array('status' => false, 'pesan' => 'Maaf data gagal di simpan !');
            }

            if ($result > 0) {
                $this->db->trans_commit();
                $out = array('status' => true, 'pesan' => ' Data berhasil di simpan');
            } else {
                $this->db->trans_rollback();
                $out = array('status' => false, 'pesan' => 'Maaf data gagal di simpan !');
            }
        }

        echo json_encode($out);
    }

    public function Edit($id)
    {

        /* ini harus ada boss */
        $data['userdata'] = $this->session->userdata('nama');
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        if ($access->menuview == 0) {
            $data['page'] = self::__title;
            $data['judul'] = self::__title;
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        }
        /* ini harus ada boss */ else {

            $where = array(self::__tableId => $id);
            $data['brand'] = $this->M_jenis_bantuan->selectById($id);
            $data['komoditi'] = $this->M_jenis_bantuan->selekKomoditi();
            $data['sub_kegiatan'] = $this->M_jenis_bantuan->selekSub();

            $data['page'] = self::__title;
            $data['judul'] = self::__title;
            $this->loadkonten('' . self::__folder . 'update', $data);
        }
    }

    public function prosesUpdate()
    {

        $username = $this->session->userdata('nama');
        $date = date('Y-m-d H:i:s');

        $where = trim($this->input->post(self::__tableId));

        $this->db->trans_begin();

        $access = $this->M_sidebar->access('add', self::__kode_menu);
        if ($access->menuview == 0) {
            $out = array('status' => false, 'pesan' => 'You dont have access.');
        } else {

            $data = array(
                'id_sub_kegiatan' => $this->input->post('sub_kegiatan'),
                'id_komoditi' => $this->input->post('komoditi'),
                'jenis_bantuan' => $this->input->post('jenis_bantuan'),
                'satuan' => $this->input->post('satuan'),
                'created_by' => $username,
                'created_date' => $date,
            );

            $result = $this->db->update(self::__tableName, $data, array(self::__tableId => $where));

            if ($this->db->trans_status() === FALSE) {
                $out = array('status' => false, 'pesan' => 'Maaf data gagal di update !');
            }

            if ($result > 0) {
                $this->db->trans_commit();
                $out = array('status' => true, 'pesan' => ' Data berhasil di update');
            } else {
                $this->db->trans_rollback();
                $out = array('status' => false, 'pesan' => 'Maaf data gagal di update !');
            }
        }


        echo json_encode($out);
    }

    public function import() {

        $access = $this->M_sidebar->access('add', self::__kode_menu);
        if ($access->menuview == 0) {
            $out = array('status' => false, 'pesan' => 'You dont have access.');
        } else {

        $this->form_validation->set_rules('excel', 'File', 'trim|required');
        if ($_FILES['excel']['name'] == '') {
                
        } else {
                $config['upload_path'] = './assets/excel/';
                $config['allowed_types'] = 'xls|xlsx';

                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('excel')) {
                    $error = array('error' => $this->upload->display_errors());
                } else {
                    $data = $this->upload->data();

                    error_reporting(E_ALL);
                    date_default_timezone_set('Asia/Jakarta');

                    include './assets/phpexcel/Classes/PHPExcel/IOFactory.php';

                    $inputFileName = './assets/excel/' . $data['file_name'];
                    $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
                    $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);

                    $index = 0;
                    foreach ($sheetData as $key => $value) {
                        if ($key != 1) {
                            // if ($check != 1) {
                            $resultData[$index]['id_sub_kegiatan'] = $value['A'];
                            $resultData[$index]['id_komoditi'] = $value['B'];
                            $resultData[$index]['jenis_bantuan'] = $value['C'];
                            $resultData[$index]['satuan'] = $value['D'];
                            $resultData[$index]['created_by'] = $value['E'];
                            $resultData[$index]['created_date'] = date('Y-m-d H:i:s', strtotime($value['F']));
                            // }
                        }
                        $index++;
                    }

                    unlink('./assets/excel/' . $data['file_name']);

                    if (count($resultData) != 0) {
                        $result=$this->db->insert_batch('tbl_jenis_bantuan', $resultData);
                        if ($result > 0) {
                        $out = array('status' => true, 'pesan' => ' Data berhasil di import');
                        } else {
                        $out = array('status' => false, 'pesan' => 'Maaf data gagal di import !');
                        }
                    } else {
                    $out = array('status' => false, 'pesan' => 'Maaf data gagal di import !');
             }
           }
        }
    }
    echo json_encode($out);
    }

    public function hapus()
    {

        $id = $_POST[self::__tableId];
        $result = $this->M_jenis_bantuan->hapus($id);
        if ($result > 0) {
            $out = array('status' => true, 'pesan' => ' Data berhasil di hapus');
        } else {
            $out = array('status' => false, 'pesan' => 'Maaf data gagal di hapus !');
        }
        echo json_encode($out);
    }
}
