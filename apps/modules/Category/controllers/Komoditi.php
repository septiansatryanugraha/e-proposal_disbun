<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Komoditi extends AUTH_Controller
{
    const __tableName = 'tbl_komoditi';
    const __tableId = 'id_komoditi';
    const __folder = 'v_komoditi/';
    const __kode_menu = 'komoditi';
    const __title = 'Komoditi';
    const __model = 'M_komoditi';

    public function __construct()
    {
        parent::__construct();
        $this->load->model(self::__model);
        $this->load->model('M_sidebar');
    }

    public function loadkonten($page, $data)
    {

        $data['userdata'] = $this->userdata;
        $ajax = ($this->input->post('status_link') == "ajax" ? true : false);
        if (!$ajax) {
            $this->load->view('Dashboard/layouts/header', $data);
        }
        $this->load->view($page, $data);
        if (!$ajax)
            $this->load->view('Dashboard/layouts/footer', $data);
    }

    public function index()
    {
        $accessAdd = $this->M_sidebar->access('add', self::__kode_menu);
        $data['accessAdd'] = $accessAdd->menuview;
        $data['userdata'] = $this->userdata;
        $data['page'] = self::__title;
        $data['judul'] = self::__title;

        $this->loadkonten('' . self::__folder . 'home', $data);
    }

    public function ajax_list()
    {
        $tanggalAwal = $this->input->post('tanggal_awal');
        $tanggalAkhir = $this->input->post('tanggal_akhir');

        $filter = array(
            'tanggal_awal' => $tanggalAwal,
            'tanggal_akhir' => $tanggalAkhir,
        );

        $accessEdit = $this->M_sidebar->access('edit', self::__kode_menu);
        $accessDel = $this->M_sidebar->access('del', self::__kode_menu);
        $list = $this->M_komoditi->get_data();

        $data = array();
        $no = $_POST['start'];
        foreach ($list as $brand) {
            $no++;
            $grup = $this->M_komoditi->select_komoditi_by_id($brand->grup_id);
            $row = array();
            $row[] = $no;
            $row[] = $grup->nama_grup;
            $row[] = $brand->nama;
            //add html for action
            $buttonEdit = '';
            if ($accessEdit->menuview > 0) {
                $buttonEdit = anchor('edit-komoditi/' . $brand->id_komoditi, ' <span tooltip="Edit Data"><span class="fa fa-edit" ></span>', ' class="btn btn-sm btn-primary klik ajaxify" ');
            }
            $buttonDel = '';
            if ($accessDel->menuview > 0) {
                $buttonDel = '<button class="btn btn-sm btn-danger hapus-komoditi" data-id=' . "'" . $brand->id_komoditi . "'" . '><span tooltip="Hapus Data"><i class="glyphicon glyphicon-trash"></i></button>';
            }

            $row[] = $buttonEdit . '  ' . $buttonDel;
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function Add()
    {
        /* ini harus ada boss */
        $data['userdata'] = $this->session->userdata('nama');
        $access = $this->M_sidebar->access('add', self::__kode_menu);
        if ($access->menuview == 0) {
            $data['page'] = self::__title;
            $data['judul'] = self::__title;
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        }

        /* ini harus ada boss */ else {
            $data['page'] = self::__title;
            $data['judul'] = self::__title;
            $data['group'] = $this->M_komoditi->select_group();
            // $data['dinas'] 	    = $this->Mdl_opd->selekDinas();

            $this->loadkonten('' . self::__folder . 'tambah', $data);
        }
    }

    public function prosesAdd()
    {

        $username = $this->session->userdata('nama');
        $date = date('Y-m-d H:i:s');

        $this->db->trans_begin();

        $access = $this->M_sidebar->access('add', self::__kode_menu);
        if ($access->menuview == 0) {
            $out = array('status' => false, 'pesan' => 'You dont have access.');
        } else {

            $data = array(
                'grup_id' => $this->input->post('grup_id'),
                'nama' => $this->input->post('nama'),
            );

            $result = $this->db->insert(self::__tableName, $data);

            if ($this->db->trans_status() === FALSE) {
                $out = array('status' => false, 'pesan' => 'Maaf data gagal di simpan !');
            }

            if ($result > 0) {
                $this->db->trans_commit();
                $out = array('status' => true, 'pesan' => ' Data berhasil di simpan');
            } else {
                $this->db->trans_rollback();
                $out = array('status' => false, 'pesan' => 'Maaf data gagal di simpan !');
            }
        }

        echo json_encode($out);
    }

    public function Edit($id)
    {

        /* ini harus ada boss */
        $data['userdata'] = $this->session->userdata('nama');
        $access = $this->M_sidebar->access('edit', self::__kode_menu);
        if ($access->menuview == 0) {
            $data['page'] = self::__title;
            $data['judul'] = self::__title;
            $this->loadkonten('Dashboard/layouts/no_akses', $data);
        }
        /* ini harus ada boss */ else {

            $where = array(self::__tableId => $id);
            $data['brand'] = $this->M_komoditi->selectById($id);
            $data['group'] = $this->M_komoditi->select_group();

            $data['page'] = self::__title;
            $data['judul'] = self::__title;
            $this->loadkonten('' . self::__folder . 'update', $data);
        }
    }

    public function prosesUpdate()
    {

        $username = $this->session->userdata('nama');
        $date = date('Y-m-d H:i:s');

        $where = trim($this->input->post(self::__tableId));
        $where2 = trim($this->input->post('nama'));

        $this->db->trans_begin();

        $access = $this->M_sidebar->access('add', self::__kode_menu);
        if ($access->menuview == 0) {
            $out = array('status' => false, 'pesan' => 'You dont have access.');
        } else {

            $data = array(
                'grup_id' => $this->input->post('grup_id'),
                'nama' => $this->input->post('nama'),
            );


            $result = $this->db->update(self::__tableName, $data, array(self::__tableId => $where));

            if ($this->db->trans_status() === FALSE) {
                $out = array('status' => false, 'pesan' => 'Maaf data gagal di update !');
            }

            if ($result > 0) {
                $this->db->trans_commit();
                $out = array('status' => true, 'pesan' => ' Data berhasil di update');
            } else {
                $this->db->trans_rollback();
                $out = array('status' => false, 'pesan' => 'Maaf data gagal di update !');
            }
        }


        echo json_encode($out);
    }

    public function hapus()
    {

        $id = $_POST[self::__tableId];
        $result = $this->M_komoditi->hapus($id);
        if ($result > 0) {
            $out = array('status' => true, 'pesan' => ' Data berhasil di hapus');
        } else {
            $out = array('status' => false, 'pesan' => 'Maaf data gagal di hapus !');
        }
        echo json_encode($out);
    }
}
