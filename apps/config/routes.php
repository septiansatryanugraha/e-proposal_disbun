<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	http://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There area two reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router what URI segments to use if those provided
  | in the URL cannot be matched to a valid route.
  |
 */

$route['default_controller'] = "Opd/Login_opd";
$route['404_override'] = 'Default/Not_found';
// $route['404_override'] = '';
$route['login'] = 'Default/Auth';
$route['logout'] = 'Auth/logout';
$route['login-pengajuan'] = 'Opd/Login_opd';
$route['login-admin'] = 'Default/Auth';

$route['beranda'] = 'Opd/Dashboard';
$route['daftar-akun'] = 'Opd/Register';
$route['proses-daftar'] = 'Opd/Register/submit';

$route['add-pengajuan-proposal'] = 'Opd/Pengajuan_proposal/add_dokumen';
$route['data-proposal'] = 'Opd/Pengajuan_proposal/home';
$route['view-data-proposal/(:any)'] = 'Opd/Pengajuan_proposal/edit/$1';
$route['data-history'] = 'Opd/Products/history';

$route['upload-iup/(:any)'] = 'Opd/Products/edit/$1';


$route['profil-user'] = 'Opd/Profil_opd/profil';

$route['data-kelompok-petani'] = 'Opd/Petani/home';
$route['add-kelompok-petani'] = 'Opd/Petani/add_dokumen';
$route['view-kelompok-petani/(:any)'] = 'Opd/Petani/edit/$1';

/*   route modul master dokumen  */
$route['cetak-proposal/(:any)'] = 'Opd/Pengajuan_proposal/cetak/$1';


/*   route modul profile */
$route['profile'] = 'Setting/Profile/index';


/*   route modul dashboard  */
$route['dashboard'] = 'Support/Dashboard/index';


/*   route modul menu  */
$route['menu'] = 'Setting/Menu/index';
$route['icon'] = 'Setting/Menu/icon';
$route['add-menu'] = 'Setting/Menu/add';
$route['edit-menu/(:any)'] = 'Setting/Menu/edit/$1';


/*   route modul user  */
$route['user'] = 'Setting/User/index';
$route['add-user'] = 'Setting/User/add';
$route['edit-user/(:any)'] = 'Setting/User/edit/$1';


/*   route modul grup  */
$route['user-grup'] = 'Setting/Grup/index';
$route['add-grup'] = 'Setting/Grup/add';
$route['edit-grup/(:any)'] = 'Setting/Grup/edit/$1';

/*   route modul grup  */
$route['button'] = 'Setting/Button/index';
$route['edit-button/(:any)'] = 'Setting/Button/edit/$1';


/*    route modul Akses  */
$route['hak-akses/(:any)'] = 'Setting/Akses/hak_akses/$1';

/*   route modul master opd  */
$route['bantuan'] = 'Category/Bantuan/index';
$route['add-bantuan'] = 'Category/Bantuan/add';
$route['edit-bantuan/(:any)'] = 'Category/Bantuan/edit/$1';

/*   route modul category komoditi  */
$route['komoditi'] = 'Category/Komoditi/index';
$route['add-komoditi'] = 'Category/Komoditi/add';
$route['edit-komoditi/(:any)'] = 'Category/Komoditi/edit/$1';

/*   route modul category jenis bantuan  */
$route['jenis-bantuan'] = 'Category/Jenis_bantuan/index';
$route['add-jenis-bantuan'] = 'Category/Jenis_bantuan/add';
$route['edit-jenis-bantuan/(:any)'] = 'Category/Jenis_bantuan/edit/$1';

/*   route modul category sub kegiatan  */
$route['sub-kegiatan'] = 'Category/Sub_kegiatan/index';
$route['add-sub-kegiatan'] = 'Category/Sub_kegiatan/add';
$route['edit-sub-kegiatan/(:any)'] = 'Category/Sub_kegiatan/edit/$1';

/*   route modul category jenis bantuan  */
$route['kegiatan'] = 'Category/Kegiatan/index';
$route['add-kegiatan'] = 'Category/Kegiatan/add';
$route['edit-kegiatan/(:any)'] = 'Category/Kegiatan/edit/$1';

/*   route modul beban  */
$route['file-pengajuan'] = 'Category/File_upload/index';
$route['add-file-pengajuan'] = 'Category/File_upload/add';
$route['edit-file-pengajuan/(:any)'] = 'Category/File_upload/edit/$1';

/*   route modul beban  */
$route['kategori-status'] = 'Category/Status/index';
$route['add-status'] = 'Category/Status/add';
$route['edit-status/(:any)'] = 'Category/Status/edit/$1';

/*   route modul beban  */
$route['master-desa'] = 'Category/Desa/index';
$route['add-desa'] = 'Category/Desa/add';
$route['edit-desa/(:any)'] = 'Category/Desa/edit/$1';


/*   route modul master petani */
$route['master-petani'] = 'Master/Petani/index';
$route['add-petani'] = 'Master/Petani/add';
$route['edit-petani/(:any)'] = 'Master/Petani/edit/$1';


/*   route modul master user  */
$route['master-user-kab'] = 'Master/User_Kab/index';
$route['add-user-kab'] = 'Master/User_Kab/add';
$route['edit-user-kab/(:any)'] = 'Master/User_Kab/edit/$1';

/*   route modul master dokumen  */
$route['master-proposal'] = 'Master/Master_proposal/index';
$route['print-proposal/(:any)'] = 'Master/Master_proposal/cetak/$1';
$route['edit-proposal/(:any)'] = 'Master/Master_proposal/edit/$1';

/*   route modul master dokumen  */
$route['master-proposal-rekom'] = 'Master/Master_proposal_rekom/index';
$route['edit-proposal-rekom/(:any)'] = 'Master/Master_proposal_rekom/edit/$1';

/*   route modul master dokumen  */
$route['master-proposal-penetapan'] = 'Master/Master_proposal_penetapan/index';
$route['edit-proposal-penetapan/(:any)'] = 'Master/Master_proposal_penetapan/edit/$1';

/*   route modul master dokumen  */
$route['master-sertifikasi'] = 'Master/Master_sertifikasi/index';
$route['edit-sertifikasi/(:any)'] = 'Master/Master_sertifikasi/edit/$1';

/*   route modul master user  */
$route['kecamatan'] = 'Category/Kecamatan/index';
$route['add-kecamatan'] = 'Category/Kecamatan/add';
$route['edit-kecamatan/(:any)'] = 'Category/Kecamatan/edit/$1';


/*   route modul master dokumen  */
$route['master-sts'] = 'Master/Master_sts/index';
$route['edit-sts/(:any)'] = 'Master/Master_sts/edit/$1';


/*   route report modul laba rugi  */
$route['report-proposal'] = 'Support/Report_proposal/index';
$route['filter-proposal'] = 'Support/Report_proposal/filter';
$route['export-proposal'] = 'Support/Report_proposal/export_excel';

/*   route report modul laba rugi  */
$route['report-history-pengajuan'] = 'Support/Report_history_pengajuan/index';
$route['filter-history-pengajuan'] = 'Support/Report_history_pengajuan/filter';
$route['export-history-pengajuan'] = 'Support/Report_history_pengajuan/export_excel';

/*   route report modul laba rugi  */
$route['rekap-proposal'] = 'Opd/Rekap_proposal/home';
$route['filter-rekap-proposal'] = 'Opd/Rekap_proposal/filter';
$route['export-rekap-proposal'] = 'Opd/Rekap_proposal/export_excel';


/* End of file routes.php */
/* Location: ./application/config/routes.php */